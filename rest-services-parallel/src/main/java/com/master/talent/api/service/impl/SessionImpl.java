package com.master.talent.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.master.talent.api.dao.SessionDao;
import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.service.SessionService;

@Service
public class SessionImpl implements SessionService {
	
	private final Logger logger = LoggerFactory.getLogger(SessionImpl.class);
	
	@Autowired
	private SessionDao dao;
	
	public void setDao(SessionDao dao) {
		this.dao = dao;
	}

	@Override
	public SessionEntity createSession(String username) {
		try{
			SessionEntity sessionObject  = dao.createSession(username);
			return sessionObject;
		}catch(Exception e){
			logger.debug("Error ::" + e);
			return null;
		}
	}

	@Override
	public SessionEntity validateSession(String sessionId) {
		try{
			SessionEntity sessionObject  = dao.validateSession(sessionId);
			if (sessionObject != null){
				return sessionObject;
			} else {
				return null;
			}
		}catch(Exception e){
			logger.debug("Error ::" + e);
			return null;
		}
	}

	@Override
	public void deleteSession(String sessionId) {
		try{
			dao.deleteSession(sessionId);
		}catch(Exception e){
			logger.debug("Error ::" + e);
		}
		
	}	
}
