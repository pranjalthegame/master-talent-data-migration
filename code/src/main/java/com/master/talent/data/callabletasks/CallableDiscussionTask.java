/**
 * 
 */
package com.master.talent.data.callabletasks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.master.talent.data.model.DiscussionModel;
import com.master.talent.data.model.ResponseObject;
import com.master.talent.data.model.excel.DiscussionExcelModel;
import com.master.talent.data.service.RestService;
import com.master.talent.data.util.DiscussionConverterUtils;
import com.master.talent.data.util.JsonUtils;

/**
 * @author pranjal.sinha
 *
 */
public class CallableDiscussionTask implements Callable<Map<String,String>>{

	private RestService restService;
	private List<DiscussionExcelModel> discussionBatch;
	
	public CallableDiscussionTask(RestService restService,List<DiscussionExcelModel> discussionBatch) {
		this.restService = restService;
		this.discussionBatch = discussionBatch;
	}
	
	@Override
	public Map<String, String> call() throws Exception {
		Map<String,String> response = new HashMap<>();
		Thread.currentThread().setName(getThreadName());
		for(DiscussionExcelModel inputData: discussionBatch){
			DiscussionModel discussionModel = DiscussionConverterUtils.convertDiscussion(inputData);
			ResponseObject res = restService.processDiscussionOverHttp(discussionModel);
			String itemId = parseResponseForId(res);
			if(itemId!=null){
				response.put(itemId, discussionModel.getOwner());
			}else{
				response.put("Error-"+discussionModel.getOwner(), JsonUtils.toJson(res));
			}
		}return response;
	}
	
	private String getThreadName(){
		String threadName = "master-talent-discussion-data-migration-thread";
		Long threadId = Thread.currentThread().getId();
		threadName = threadName + "-" + threadId;
		return threadName;
	}
	
	private String parseResponseForId(ResponseObject res){
		try{
			String[] splitArr = res.getResponseBody().split(",");
			String value = splitArr[0];
			String[] idArr = value.split(":");
			return (idArr[1].substring(1, idArr[1].length()-1));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	
	}

}
