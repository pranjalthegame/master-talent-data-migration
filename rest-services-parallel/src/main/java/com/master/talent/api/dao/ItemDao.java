package com.master.talent.api.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.master.talent.api.entity.ApplyEntity;
import com.master.talent.api.entity.CommentEntity;
import com.master.talent.api.entity.ItemEntity;
import com.master.talent.api.entity.LikeEntity;
import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.entity.ShortListEntity;
import com.master.talent.api.entity.UserFollowersEntity;
import com.master.talent.api.entity.UserFriendsEntity;
import com.master.talent.api.entity.mapper.ItemDaoMapper;
import com.master.talent.api.entity.repo.ApplyEntityRepo;
import com.master.talent.api.entity.repo.CommentEntityRepo;
import com.master.talent.api.entity.repo.ItemEntityRepo;
import com.master.talent.api.entity.repo.LikeEntityRepo;
import com.master.talent.api.entity.repo.ShortlistEntityRepo;
import com.master.talent.api.entity.repo.UserFollowersEntityRepo;
import com.master.talent.api.entity.repo.UserFriendsEntityRepo;
import com.master.talent.api.entity.repo.UserPreSignUpEntityRepo;
import com.master.talent.api.exception.CouchbaseWriteFailureException;
import com.master.talent.api.exception.ResourceNotFoundException;
import com.master.talent.api.exception.SearchException;
import com.master.talent.api.exception.UnAuthorisedException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.Followers;
import com.master.talent.api.models.Friends;
import com.master.talent.api.models.ItemShare;
import com.master.talent.api.models.User;
import com.master.talent.api.utils.SpringPropertiesUtil;

@Component
public class ItemDao {
	
	private final Logger logger = LoggerFactory.getLogger(ItemDao.class);
	
	@Autowired
	private ItemEntityRepo itemEntityRepo;
	@Autowired
	private CommentEntityRepo commentEntityRepo;
	@Autowired
	private ItemDaoMapper itemDaoMapper;
	@Autowired
	private UserFriendsEntityRepo userFriendsEntityRepo;
	@Autowired
	private UserFollowersEntityRepo userFollowersEntityRepo;
	@Autowired
	private UserPreSignUpEntityRepo userPreSignUpEntityRepo;
	@Autowired
	private LikeEntityRepo likeEntityRepo;
	@Autowired
	private ApplyEntityRepo applyEntityRepo;	
	@Autowired
	private ShortlistEntityRepo shortlistEntityRepo;	
	@Autowired
	private SearchDao searchDao;
	
	SolrClient solrClient = new HttpSolrClient.Builder(SpringPropertiesUtil.getProperty("solr.server.url")).build();	
		
	private HttpClient getHttpClient() {
	    CredentialsProvider provider = new BasicCredentialsProvider();
	    UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(
	    		SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
	    provider.setCredentials(AuthScope.ANY, credentials);
	    return HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
	}
	
	public void setUserPreSignUpEntityRepo(UserPreSignUpEntityRepo userPreSignUpEntityRepo) {
		this.userPreSignUpEntityRepo = userPreSignUpEntityRepo;
	}
	public void setUserFriendsEntityRepo(UserFriendsEntityRepo userFriendsEntityRepo) {
		this.userFriendsEntityRepo = userFriendsEntityRepo;
	}
	public void setUserFollowersEntityRepo(
			UserFollowersEntityRepo userFollowersEntityRepo) {
		this.userFollowersEntityRepo = userFollowersEntityRepo;
	}
	public void setItemEntityRepo(ItemEntityRepo itemEntityRepo) {
		this.itemEntityRepo = itemEntityRepo;
	}
	public void setCommentEntityRepo(CommentEntityRepo commentEntityRepo) {
		this.commentEntityRepo = commentEntityRepo;
	}	
	public void setItemDaoMapper(ItemDaoMapper itemDaoMapper) {
		this.itemDaoMapper = itemDaoMapper;
	}
	
	public ItemEntity createItem(ItemEntity obj, boolean jobListing) {
		try{
			switch(obj.getItemType().toLowerCase()){ 
			    case "article":
			    	if(obj.getArticle()==null){
			    		throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_INVALID_TYPE,"Invalid Type::"+obj.getOwner());
			    	}
			    	obj.setDiscussion(null);
			    	obj.setJob(null);
			    	obj.setMedia(null);
			    	obj.setTutorial(null);
			    	break;
			    case "discussion":
			    	if(obj.getDiscussion()==null){
			    		throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_INVALID_TYPE,"Invalid Type::"+obj.getOwner());
			    	}
			    	obj.setArticle(null);
			    	obj.setJob(null);
			    	obj.setMedia(null);
			    	obj.setTutorial(null);
			    	break; 
			    case "media":
			    	if(obj.getMedia()==null){
			    		throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_INVALID_TYPE,"Invalid Type::"+obj.getOwner());
			    	}
			    	obj.setArticle(null);
			    	obj.setDiscussion(null);
			    	obj.setJob(null);
			    	obj.setTutorial(null);
			    	break; 
			    case "job":
			    	if(obj.getJob()==null){
			    		throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_INVALID_TYPE,"Invalid Type::"+obj.getOwner());
			    	}
			    	obj.setArticle(null);
			    	obj.setDiscussion(null);
			    	obj.setMedia(null);
			    	obj.setTutorial(null);
			    	break; 
			    case "tutorial":
			    	if(obj.getTutorial()==null){
			    		throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_INVALID_TYPE,"Invalid Type::"+obj.getOwner());
			    	}
			    	obj.setArticle(null);
			    	obj.setDiscussion(null);
			    	obj.setMedia(null);
			    	break; 
			    default:
			    	logger.debug("Wrong article::", obj.getArticle());
			    	throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_INVALID_TYPE,"Invalid Type::"+obj.getOwner()); 
			    } 
				obj.setCreated(DateTime.now());
				obj.setUpdated(DateTime.now());
				obj.setItemId(UUID.randomUUID().toString());
				ItemEntity itemEntiy = itemEntityRepo.save(obj);
				indexItemToSolr(itemEntiy, "CREATE", jobListing);
				return itemEntiy;
			}catch(Exception e){
			logger.debug("createItem", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_ITEM_CREATE_FAIL,e.getMessage(),e);
		}
	}
	
	private void indexItemToSolr(ItemEntity obj, String type, boolean jobListing) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			if(type.equalsIgnoreCase("DELETE")){
				
				UpdateRequest req = new UpdateRequest(); 
			    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
				req.deleteById(obj.getItemId());
				req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
				req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
				
			}
			
			if(type.equalsIgnoreCase("CREATE")){
				doc.addField("id", obj.getItemId());
				doc.addField("username", obj.getOwner());
			    doc.addField("created", new Date());
			    doc.addField("updated", new Date());
			    doc.addField("isShared", false);
			    if(obj.getPageId()!=null)
			    	doc.addField("pageId", obj.getPageId());
			    List<String> viewers = new ArrayList<String>();
			    viewers.add(obj.getOwner());
			    
			    //get and add all user friends 
			    UserFriendsEntity userFriendsEntity =userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+obj.getOwner());
			    if(userFriendsEntity!=null){
			    	if(userFriendsEntity.getFriends()!=null){
			    		if(!userFriendsEntity.getFriends().isEmpty()){
				    		for (Friends friend : userFriendsEntity.getFriends()) {
				    			viewers.add(friend.getFriendName());
							}
				    	}
			    	}			    	
			    }
			    //get and add all user followers
			    UserFollowersEntity userFollowersEntity = userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+obj.getOwner());
			    if(userFollowersEntity!=null){
			    	if(userFollowersEntity.getFollowers()!=null){
			    		if(!userFollowersEntity.getFollowers().isEmpty()){
				    		for (Followers  followers : userFollowersEntity.getFollowers()) {
				    			viewers.add(followers.getFollowerName());
							}
				    	}
			    	}
			    }
			    doc.addField("viewers", viewers);
			    
			    switch(obj.getItemType().toLowerCase()){ 
			    case "article":
			    	doc.addField("itemTitle", obj.getArticle().getTitle());
			    	doc.addField("itemDescription", obj.getArticle().getDesc().toLowerCase());
			    	doc.addField("itemType",obj.getItemType().toLowerCase());
			    	break;
			    case "discussion":
			    	doc.addField("itemTitle", obj.getDiscussion().getTitle());
			    	doc.addField("itemDescription", obj.getDiscussion().getDesc().toLowerCase());
			    	doc.addField("itemType",obj.getItemType().toLowerCase());
			    	break; 
			    case "media":
			    	doc.addField("itemTitle", "Media");
			    	doc.addField("itemDescription", obj.getMedia().getDesc().toLowerCase());
			    	doc.addField("itemType",obj.getItemType().toLowerCase());
			    	break; 
			    case "job":
			    	doc.addField("itemTitle", obj.getJob().getTitle());
			    	doc.addField("itemDescription", obj.getJob().getDesc().toLowerCase() +" " + obj.getJob().getJobType() + " " + obj.getJob().getIndustry());
			    	doc.addField("itemType",obj.getItemType().toLowerCase());
				    if(jobListing)
				    	doc.addField("proJob", true);
			    	break;
			    case "tutorial":
			    	doc.addField("itemTitle", obj.getTutorial().getTitle());
			    	doc.addField("itemDescription", obj.getTutorial().getDesc().toLowerCase());
			    	doc.addField("itemType",obj.getItemType().toLowerCase());
					doc.addField("itemCategory", obj.getTutorial().getCategory().toLowerCase());
					doc.addField("itemSubCategory", obj.getTutorial().getSubCategory().toLowerCase());
			    	break;
			    default:
			    	logger.debug("Invalid Payload::", obj);
				}
			    
			    UpdateRequest req = new UpdateRequest(); 
			    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
				req.add(doc); 
				req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
				req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			    
			}
		 }catch(Exception e){
			logger.debug("solr error -- also deleting object from couchbase", e);
			if(type.equalsIgnoreCase("CREATE")){
				itemEntityRepo.delete(obj);
			}
			throw new SearchException(ExceptionCodes.SECodes.SE_ITEM_INDEX_FAIL,e.getMessage(),e);
		}		
	}	
	
	public ItemEntity updateItem(String username, ItemEntity obj) {
		try{
			ItemEntity itemEntity =itemDaoMapper.mapper(itemEntityRepo.findOne(obj.getItemId()), obj);
			if(itemEntity.getOwner().equals(username)){
				indexItemToSolr(itemEntity, "UPDATE", false);
				return itemEntityRepo.save(itemEntity);
			} else {
				throw new UnAuthorisedException(ExceptionCodes.UECodes.UE_NOT_ALLOWED,SpringPropertiesUtil.getProperty("unauthorised.exception.message"));
			}
			}catch(Exception e){
			logger.debug("updateItem", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_ITEM_UPDATE_FAIL,e.getMessage(),e);
		}
	}
	
	public ItemEntity getItem(String itemId) {
		try{
			ItemEntity itemEntity = itemEntityRepo.findOne(itemId);
			if(itemEntity.getShare() !=null){
				ItemEntity localEntity  = itemEntityRepo.findOne(itemEntity.getShare().getParentItemId());
				localEntity.setItemId(itemEntity.getItemId());
				localEntity.setOwner(itemEntity.getOwner());
				localEntity.setDisplayName(getDisplayName(itemEntity.getOwner()));
				ItemShare share = itemEntity.getShare();
				share.setShareOwnerDisplayName(getDisplayName(share.getShareOwner()));
				localEntity.setShare(itemEntity.getShare());
				return localEntity;
			} else {				
				return itemEntity;
			}
		}catch(Exception e){
			logger.debug("itemget Entity", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_ITEM_NOT_FOUND,e.getMessage(),e);
		}
	}	
	
	public String getDisplayName(String username) throws SolrServerException, IOException{
		SolrQuery userQuery = new SolrQuery();
		userQuery.setQuery("id:"+username);
		
		QueryRequest req = new QueryRequest(userQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
				
		if(rsp.getResults()!=null){
			return rsp.getResults().get(0).getFieldValue("fullname").toString();
		} else {
			logger.debug("username not found ::",username);
			return "";
		}
    }
	
	public ItemEntity deleteItem(String itemId) {
		try{
			ItemEntity itemEntity = itemEntityRepo.findOne(itemId);
			itemEntity.setDelete(true);
			indexItemToSolr(itemEntity, "DELETE", false);
			return itemEntityRepo.save(itemEntity);
		}catch(Exception e){
			logger.debug("delete", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_ITEM_DELETE_FAIL,e.getMessage(),e);
		}
	}
	
	public CommentEntity createComment(CommentEntity obj) {
		try{
			obj.setCommentId(UUID.randomUUID().toString());
			obj.setCreated(DateTime.now());
			obj.setUpdated(DateTime.now());
			
			//update item attribute with latest friends list and followers and commentuser list
			updateItemSolrCommentList(obj.getItemId(), obj.getUsername());
			updateItemSolrIndexAfterComment(obj);
			updateCommentIndex(obj);
			
			return commentEntityRepo.save(obj);
		}catch(Exception e){
			logger.debug("createComment", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_COMMENT_CREATE_FAIL,e.getMessage(),e);
		}
	}
	
	private void updateItemSolrCommentList(String itemId, String username) throws SolrServerException, IOException {
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", itemId);
		Map<String, String> partialUpdate = new HashMap<String, String>();
		partialUpdate.put("add", username);
		doc.addField("commentList", partialUpdate);
		
		UpdateRequest req = new UpdateRequest(); 
	    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		req.add(doc); 
		req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
		req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
		
		
	}
	void updateCommentIndex(CommentEntity obj) throws SolrServerException, IOException{
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", obj.getCommentId());
		doc.addField("itemId", obj.getItemId());
		doc.addField("updated", new Date());
		
		UpdateRequest req = new UpdateRequest(); 
	    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		req.add(doc); 
		req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
		req.process(solrClient, SpringPropertiesUtil.getProperty("solr.comment.collection")); 
		
	}
	
	void updateItemSolrIndexAfterComment(CommentEntity obj) throws SolrServerException, IOException{
		UserFriendsEntity userFriendsEntity = userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+obj.getUsername());
		UserFollowersEntity userFollowersEntity = userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+obj.getUsername());
		SolrInputDocument doc = new SolrInputDocument();
		Map<String, String> partialUpdate = new HashMap<String, String>();
		boolean flag = false;
		if(userFriendsEntity !=null){
			if(userFriendsEntity.getFriends() != null){
				if(!userFriendsEntity.getFriends().isEmpty()){
					flag = true;
					List<Friends> listFriends = userFriendsEntity.getFriends();		
					for (Friends friends : listFriends) {
						partialUpdate.put("add", friends.getFriendName());
					}
				}
			}
		}
		if(userFollowersEntity !=null){
			if(userFollowersEntity.getFollowers() !=null ){
				if(!userFollowersEntity.getFollowers().isEmpty()){
					flag = true;
					List<Followers> listFollowers = userFollowersEntity.getFollowers();		
					for (Followers followers : listFollowers) {
						partialUpdate.put("add", followers.getFollowerName());
					}	
				}
			}
		}
		if(flag){
			doc.addField("id", obj.getItemId());	
			doc.addField("viewers", partialUpdate);
			
			UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
		}
	}
	
	public CommentEntity updateComment(String username, CommentEntity obj) {
		try{
			CommentEntity commentEntity = commentEntityRepo.findOne(obj.getCommentId());
			if(commentEntity != null){
				if(commentEntity.getUsername().equals(username)){
					commentEntity.setDesc(obj.getDesc());
					commentEntity.setUpdated(DateTime.now());
					return commentEntityRepo.save(commentEntity);
				} else{
					throw new UnAuthorisedException(ExceptionCodes.UECodes.UE_NOT_ALLOWED,SpringPropertiesUtil.getProperty("unauthorised.exception.message"));
				}				
			} else {
				throw new CouchbaseWriteFailureException("Comment Exists::"+obj.getItemId());
			}
			}catch(Exception e){
				logger.debug("createComment", e);
				throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_COMMENT_UPDATE_FAIL,e.getMessage(),e);
		}
	}
	
	public List<CommentEntity> getCommentByItem(String itemId, int from,  int to) {
		try{
			SolrQuery commentQuery = new SolrQuery();
			commentQuery.setQuery("itemId:"+itemId);
			commentQuery.addSort("updated", ORDER.desc);
			if(from ==0 && to ==0 ){
				commentQuery.setStart(0);
				commentQuery.setRows(1);
			} else {
				commentQuery.setStart(from);
				commentQuery.setRows(to);
			}
			List<CommentEntity> commentList = new ArrayList<CommentEntity>();
			
			QueryRequest req = new QueryRequest(commentQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.comment.collection")); 
			
			if (!rsp.getResults().isEmpty()){
				for (SolrDocument doc : rsp.getResults()) {
					CommentEntity local = commentEntityRepo.findOne(doc.getFieldValue("id").toString());
					local.setDisplayName(searchDao.getDisplayName(local.getUsername()));
					commentList.add(local);
				}
			}
		return commentList;
		}catch(Exception e){
			logger.debug("getCommentByItem", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_COMMENT_NOT_FOUND,e.getMessage(),e);
		}
	}	
	
	public CommentEntity deleteComment(String commentId) {
		try{
			CommentEntity commentEntity = commentEntityRepo.findOne(commentId);
			commentEntity.setDelete(true);
			
			UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.deleteById(commentId);
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.comment.collection")); 
			
			return commentEntityRepo.save(commentEntity);
		}catch(Exception e){
			logger.debug("delete", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_COMMENT_DELETE_FAIL,e.getMessage(),e);
		}
	}
	public ItemEntity createShareItem(ItemShare obj) {
		try{
			ItemEntity existingItem = itemEntityRepo.findOne(obj.getParentItemId());		
			ItemEntity newItem = new ItemEntity();
			if(existingItem!=null){
				newItem.setItemId(UUID.randomUUID().toString());
				newItem.setCreated(DateTime.now());
				newItem.setUpdated(DateTime.now());
				newItem.setOwner(obj.getShareOwner());
				
				ItemShare share = new ItemShare();
				share.setParentItemId(existingItem.getItemId());
				share.setShareOwner(existingItem.getOwner());
				share.setCreated(DateTime.now());
				share.setShareMssg(obj.getShareMssg());
				
				newItem.setShare(share);
			}			
			indexShareItemToSolr(existingItem, newItem, "CREATE");
			return itemEntityRepo.save(newItem);
		}catch(Exception e){
		logger.debug("createItem", e);
		throw new CouchbaseWriteFailureException("Item does notexist::"+obj.getParentItemId());
		}
	}
	
	private void indexShareItemToSolr(ItemEntity originalItem, ItemEntity sharedItem, String type) {
		try{
			SolrInputDocument doc = new SolrInputDocument();			
			if(type.equalsIgnoreCase("CREATE")){
				doc.addField("id", sharedItem.getItemId());
				doc.addField("username", sharedItem.getOwner());
			    doc.addField("created", new Date());
			    doc.addField("updated", new Date());
			    doc.addField("isShared", true);
			    List<String> viewers = new ArrayList<String>();
			    viewers.add(sharedItem.getOwner());
			    viewers.add(sharedItem.getShare().getShareOwner());
			    
			    //get and add all user friends 
			    UserFriendsEntity userFriendsEntity =userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+sharedItem.getShare().getShareOwner());
			    if(userFriendsEntity!=null){
			    	if(!userFriendsEntity.getFriends().isEmpty()){
			    		for (Friends friend : userFriendsEntity.getFriends()) {
			    			viewers.add(friend.getFriendName());
						}
			    	}
			    }
			    //get and add all user followers
			    UserFriendsEntity userFollowersEntity = userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+sharedItem.getShare().getShareOwner());
			    if(userFollowersEntity!=null){
			    	if(!userFollowersEntity.getFriends().isEmpty()){
			    		for (Friends friend : userFollowersEntity.getFriends()) {
			    			viewers.add(friend.getFriendName());
						}
			    	}
			    }
			    doc.addField("viewers", viewers);
			    
			    switch(originalItem.getItemType().toLowerCase()){ 
			    case "article":
			    	doc.addField("itemTitle", originalItem.getArticle().getTitle());
			    	doc.addField("itemDescription", originalItem.getArticle().getDesc());
			    	doc.addField("itemType",originalItem.getItemType().toLowerCase());
			    	break;
			    case "discussion":
			    	doc.addField("itemTitle", originalItem.getDiscussion().getTitle());
			    	doc.addField("itemDescription", originalItem.getDiscussion().getDesc());
			    	doc.addField("itemType",originalItem.getItemType().toLowerCase());
			    	break; 
			    case "media":
			    	doc.addField("itemTitle", "");
			    	doc.addField("itemDescription", originalItem.getMedia().getDesc());
			    	doc.addField("itemType",originalItem.getItemType().toLowerCase());
			    	break; 
			    case "job":
			    	doc.addField("itemTitle", originalItem.getJob().getTitle().toLowerCase());
			    	doc.addField("itemDescription", originalItem.getJob().getDesc().toLowerCase() +" " + originalItem.getJob().getJobType().toLowerCase() + " " + originalItem.getJob().getIndustry().toLowerCase()
			    			+ " " + originalItem.getJob().getFunctionalArea().toLowerCase() + " " + originalItem.getJob().getBudget().toLowerCase() + " " + originalItem.getJob().getLevel()
			    			+ " " + originalItem.getJob().getLocation().toLowerCase());
			    	doc.addField("itemType",originalItem.getItemType().toLowerCase());
			    	break; 
			    case "tutorial":
			    	doc.addField("itemTitle", originalItem.getTutorial().getTitle().toLowerCase());
			    	doc.addField("itemDescription", originalItem.getTutorial().getDesc().toLowerCase());
			    	doc.addField("itemType",originalItem.getItemType().toLowerCase());
			    	break;
			    default:
			    	logger.debug("Invalid Payload::", originalItem);
				}
			    
			    UpdateRequest req = new UpdateRequest(); 
			    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
				req.add(doc); 
				req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
				req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
				
			}
		 }catch(Exception e){
			logger.debug("solr error -- also deleting object from couchbase", e);
			throw new SearchException("search index failure::"+sharedItem.getItemId());
		}		
	}
	
	
	public boolean createLikeItem(String username, String itemId) {
		ItemEntity itemEntity = itemEntityRepo.findOne(itemId);
		if(itemEntity !=null){
			LikeEntity likeEntity = likeEntityRepo.findOne(LikeEntity.ENTITY_NAME+itemId);
			List<User> listUsers;
			if(likeEntity==null){
				listUsers = new ArrayList<User>();
				likeEntity = new LikeEntity();
				likeEntity.setItemId(itemId);
				User user = new User();
				user.setCreated(new Date());
				user.setUsername(username);
				listUsers.add(user);
				likeEntity.setUsers(listUsers);
			} else {
				listUsers = likeEntity.getUsers();
				User user = new User();
				user.setCreated(new Date());
				user.setUsername(username);
				likeEntity.getUsers().add(user);
			}
			updateEventsToItemSolr(itemId, username, "LIKE");
			likeEntityRepo.save(likeEntity);
			return true;
		}else{
			throw new ResourceNotFoundException("Invalid itemId");
		}
	}	
	
	public boolean unlikeItem(String username, String itemId) {
		try{
			LikeEntity likeEntity = likeEntityRepo.findOne(LikeEntity.ENTITY_NAME+itemId);
			List<User> userList = likeEntity.getUsers();
			Iterator<User> Iterator = userList.iterator();
			while (Iterator.hasNext()) {
				User user = Iterator.next();
			    if (user.getUsername().equals(username)) {
			    	Iterator.remove();
			    }
			}
			likeEntity.setUsers(userList);
			updateEventsToItemSolr(itemId, username, "LIKEDELETE");
			likeEntityRepo.save(likeEntity);
			return true;			
		} catch (Exception e){
			return false;
		}		
	}	
	
	public boolean createShortlistItem(String username, String itemId) {
		ItemEntity itemEntity = itemEntityRepo.findOne(itemId);
		if(itemEntity !=null){
			ShortListEntity shortListEntity = shortlistEntityRepo.findOne(ShortListEntity.ENTITY_NAME+itemId);
			List<User> listUsers;
			if(shortListEntity==null){
				listUsers = new ArrayList<User>();
				shortListEntity = new ShortListEntity();
				shortListEntity.setItemId(itemId);
				User user = new User();
				user.setCreated(new Date());
				user.setUsername(username);
				listUsers.add(user);
				shortListEntity.setUsers(listUsers);
			} else {
				listUsers = shortListEntity.getUsers();
				User user = new User();
				user.setCreated(new Date());
				user.setUsername(username);
				shortListEntity.getUsers().add(user);
			}
			updateEventsToItemSolr(itemId, username, "SHORTLIST");
			shortlistEntityRepo.save(shortListEntity);
			return true;
		}else{
			throw new ResourceNotFoundException("Invalid itemId");
		}
	}	
	
	public boolean createApplyItem(SessionEntity sessionEntity, String itemId) {
		ItemEntity itemEntity = itemEntityRepo.findOne(itemId);
		if(itemEntity !=null){
			ApplyEntity applyEntity = applyEntityRepo.findOne(ApplyEntity.ENTITY_NAME+itemId);
			List<User> listUsers;
			if(applyEntity==null){
				listUsers = new ArrayList<User>();
				applyEntity = new ApplyEntity();
				applyEntity.setItemId(itemId);
				User user = new User();
				user.setCreated(new Date());
				user.setUsername(sessionEntity.getUsername());
				listUsers.add(user);
				applyEntity.setUsers(listUsers);
			} else {
				listUsers = applyEntity.getUsers();
				User user = new User();
				user.setCreated(new Date());
				user.setUsername(sessionEntity.getUsername());
				applyEntity.getUsers().add(user);
			}
			if(sessionEntity.isPriorityApplication()){
				updateEventsToItemSolr(itemId, sessionEntity.getUsername(), "APPLY-PRIORITY");
			}else{
				updateEventsToItemSolr(itemId, sessionEntity.getUsername(), "APPLY");
			}
			applyEntityRepo.save(applyEntity);
			return true;
		}else{
			throw new ResourceNotFoundException("Invalid itemId");
		}
	}	
	
	private void updateEventsToItemSolr(String itemId, String username, String type) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", itemId);
			if(type.equalsIgnoreCase("LIKE")){
				Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("add", username);
		    	doc.addField("likeList", partialUpdate);
			}
			if(type.equalsIgnoreCase("LIKEDELETE")){
				Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("remove", username);
		    	doc.addField("likeList", partialUpdate);
			}
			if(type.equalsIgnoreCase("SHORTLIST")){
				Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("add", username);
		    	doc.addField("shorlistList", partialUpdate);
			}
			if(type.equalsIgnoreCase("SHORTLISTDELETE")){
				Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("remove", username);
		    	doc.addField("shorlistList", partialUpdate);
			}
		    if(type.equalsIgnoreCase("APPLY")){
		    	Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("add", username);
		    	doc.addField("applyList", partialUpdate);
		    }
		    if(type.equalsIgnoreCase("APPLY-PRIORITY")){
		    	Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("add", username);
		    	doc.addField("proApplyList", partialUpdate);
		    }
		    if(type.equalsIgnoreCase("APPLYDELETE")){
		    	Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("remove", username);
		    	doc.addField("applyList", partialUpdate);
		    }
		    
		    UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
		} catch(Exception e){
			throw new SearchException(e.getMessage(),e);
		}
	}
	public void itemViewedUpdate(String username, String itemId) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", itemId);
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("add", username);
			doc.addField("viewedList", partialUpdate);
			
			UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
	
		} catch(Exception e){
			throw new SearchException(ExceptionCodes.SECodes.SE_ITEM_INDEX_FAIL,e.getMessage(),e);
		}
		
	}
}
