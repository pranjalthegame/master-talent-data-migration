package com.master.talent.api.models;


public class Email {

	private String toEmail;
	private String toEmailsCC;
	private String toEmailsBC;

	private String subject;
	private String content;

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getToEmailsCC() {
		return toEmailsCC;
	}

	public void setToEmailsCC(String toEmailsCC) {
		this.toEmailsCC = toEmailsCC;
	}

	public String getToEmailsBC() {
		return toEmailsBC;
	}

	public void setToEmailsBC(String toEmailsBC) {
		this.toEmailsBC = toEmailsBC;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
