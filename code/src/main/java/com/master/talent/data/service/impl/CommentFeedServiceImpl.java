/**
 * 
 */
package com.master.talent.data.service.impl;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.master.talent.data.callabletasks.CallableCommentTask;
import com.master.talent.data.constants.AppConstants;
import com.master.talent.data.model.CommentModel;
import com.master.talent.data.model.excel.CommentExcelModel;
import com.master.talent.data.service.CommentFeedService;
import com.master.talent.data.service.ReaderService;
import com.master.talent.data.service.RestService;
import com.master.talent.data.util.CommentConverterUtils;
import com.master.talent.data.util.CommonUtils;

/**
 * @author pranjal.sinha
 *
 */
@Service
public class CommentFeedServiceImpl implements CommentFeedService{

	private static final Logger LOG = LoggerFactory.getLogger(CommentFeedServiceImpl.class);
	
	@Autowired
	private ReaderService readerService;
	
	@Autowired
	private RestService restService;
	
	private static final int MAX_NO_OF_BATCHES = 20;
	
	/**
	 * below code runs in single thread mode
	 */
	@Override
	public void processComments() {
		if(isDiscussionOutputPresent()){
			List<CommentExcelModel> commentData = readerService.readCommentFeedFile(AppConstants.COMMENTS_FEED_FILE);
			if(commentData!=null && !commentData.isEmpty()){
				for(CommentExcelModel inputData: commentData){
					CommentModel commentModel = CommentConverterUtils.convertComment(inputData);
					restService.processCommentOverHttp(commentModel);
				}
			}
		}else{
			LOG.error("Cannot proceed with Comment feed because discussion output file is not present. It indicates that discussion feed has not been run first and running comment feed before it can corrupt data.Exiting!!!");
		}
	}

	/**
	 * below code template will split the data set into batches and process them concurrently
	 * 
	 */
	@Override
	public void processCommentsConcurrently() {
		if(isDiscussionOutputPresent()){
			List<CommentExcelModel> commentData = readerService.readCommentFeedFile(AppConstants.COMMENTS_FEED_FILE);
			if(commentData!=null && !commentData.isEmpty()){
				List<List<CommentExcelModel>> splitList = CommonUtils.createBatches(commentData, MAX_NO_OF_BATCHES);
				int size = splitList.size();
				ExecutorService executorService = Executors.newFixedThreadPool(size);
				LOG.info("Initializing Thread pool of size: "+splitList.size());
				try{
					ExecutorCompletionService<Map<String,String>> completionService = new ExecutorCompletionService<>(executorService);
					for(List<CommentExcelModel> commentBatch: splitList){
						LOG.info("Batch size to be procesed is: "+commentBatch.size());
						completionService.submit(new CallableCommentTask(restService,commentBatch));
					}CommonUtils.pollForResult(completionService, size,AppConstants.TYPE_COMMENT);
				}finally{
					executorService.shutdown();
				}
			}
		}else{
			LOG.error("Cannot proceed with Comment feed because discussion output file is not present. It indicates that discussion feed has not been run first and running comment feed before it can corrupt data.Exiting!!!");
		}
		
	}

	private boolean isDiscussionOutputPresent(){
		String fileName = AppConstants.FILE_RELATIVE_PATH + AppConstants.DISCUSSION_OUTPUT_FILE;
		File file = new File(fileName);
		return file.exists();
	}
}
