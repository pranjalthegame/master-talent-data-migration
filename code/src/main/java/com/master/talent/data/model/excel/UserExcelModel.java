package com.master.talent.data.model.excel;

import com.poiji.internal.annotation.ExcelCell;

public class UserExcelModel
{
	
	@ExcelCell(0)
	private String gender;
	@ExcelCell(1)
	private String password;
	@ExcelCell(2)
	private String lname;
	@ExcelCell(3)
	private String emailAddress;
	@ExcelCell(4)
	private String relationship;
	@ExcelCell(5)
	private boolean follower;
	@ExcelCell(6)
	private String fname;
	@ExcelCell(7)
	private String brief;
	@ExcelCell(8)
	private boolean active;
	@ExcelCell(9)
	private String referral;
	@ExcelCell(10)
	private boolean bannerDefultImage;
	@ExcelCell(11)
	private String bannerDefaultPath;
	@ExcelCell(12)
	private String otp;
	@ExcelCell(13)
	private String employmentType;
	@ExcelCell(14)
	private String employmentWorkStatus;
	@ExcelCell(15)
	private String employmentCategory;
	@ExcelCell(16)
	private String employmentExpertise;
	@ExcelCell(17)
	private String registerLevel;
	@ExcelCell(18)
	private String phone;
	@ExcelCell(19)
	private String dob;
	@ExcelCell(20)
	private String userType;
	@ExcelCell(21)
	private String username;
	@ExcelCell(22)
	private String proInterest;
	@ExcelCell(23)
	private String createdDate;
	public String getGender() {
		return gender;
	}
	public String getPassword() {
		return password;
	}
	public String getLname() {
		return lname;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public String getRelationship() {
		return relationship;
	}
	public boolean isFollower() {
		return follower;
	}
	public String getFname() {
		return fname;
	}
	public String getBrief() {
		return brief;
	}
	public boolean isActive() {
		return active;
	}
	public String getReferral() {
		return referral;
	}
	public boolean isBannerDefultImage() {
		return bannerDefultImage;
	}
	public String getBannerDefaultPath() {
		return bannerDefaultPath;
	}
	public String getOtp() {
		return otp;
	}
	public String getEmploymentType() {
		return employmentType;
	}
	public String getEmploymentWorkStatus() {
		return employmentWorkStatus;
	}
	public String getEmploymentCategory() {
		return employmentCategory;
	}
	public String getEmploymentExpertise() {
		return employmentExpertise;
	}
	public String getRegisterLevel() {
		return registerLevel;
	}
	public String getPhone() {
		return phone;
	}
	public String getDob() {
		return dob;
	}
	public String getUserType() {
		return userType;
	}
	public String getUsername() {
		return username;
	}
	public String getProInterest() {
		return proInterest;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	@Override
	public String toString() {
		return "UserExcelModel [gender=" + gender + ", password=" + password + ", lname=" + lname + ", emailAddress="
				+ emailAddress + ", relationship=" + relationship + ", follower=" + follower + ", fname=" + fname
				+ ", brief=" + brief + ", active=" + active + ", referral=" + referral + ", bannerDefultImage="
				+ bannerDefultImage + ", bannerDefaultPath=" + bannerDefaultPath + ", otp=" + otp + ", employmentType="
				+ employmentType + ", employmentWorkStatus=" + employmentWorkStatus + ", employmentCategory="
				+ employmentCategory + ", employmentExpertise=" + employmentExpertise + ", registerLevel="
				+ registerLevel + ", phone=" + phone + ", dob=" + dob + ", userType=" + userType + ", username="
				+ username + ", proInterest=" + proInterest + ", createdDate=" + createdDate + "]";
	}
	
//	private String fullName;
	
	


}
