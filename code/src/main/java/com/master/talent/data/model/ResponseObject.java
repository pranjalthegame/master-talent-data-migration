/**
 * 
 */
package com.master.talent.data.model;

/**
 * @author pranjal.sinha
 *
 */
public class ResponseObject {

	private int responseCode;
	private String responseBody;
	
	public ResponseObject(){
		
	}
	
	public ResponseObject(int responseCode,String responseBody){
		this.responseCode = responseCode;
		this.responseBody = responseBody;
	}
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	
	
}
