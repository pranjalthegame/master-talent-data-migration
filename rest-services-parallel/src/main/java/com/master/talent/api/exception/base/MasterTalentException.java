/**
 * 
 */
package com.master.talent.api.exception.base;

/**
 * @author pranjal.sinha
 *
 */
public class MasterTalentException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String exceptionCode;
	
	

	public MasterTalentException() {
		super();
	}

	public MasterTalentException(String message, Throwable cause) {
		super(message, cause);
	}

	public MasterTalentException(String message) {
		super(message);
	}

	public MasterTalentException(Throwable cause) {
		super(cause);
	}
	
	public MasterTalentException(String exceptionCode,String message){
		super(message);
		this.exceptionCode = exceptionCode;
	}
	
	public MasterTalentException(String exceptionCode,String message, Throwable cause){
		super(message,cause);
		this.exceptionCode = exceptionCode;
	}
	
	public String getExceptionCode() {
		return exceptionCode;
	}

	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}
	
	

}
