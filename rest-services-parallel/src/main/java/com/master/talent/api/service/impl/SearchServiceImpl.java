package com.master.talent.api.service.impl;

import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.master.talent.api.dao.SearchDao;
import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.exception.InvalidRequestException;
import com.master.talent.api.exception.SearchException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.SearchUser;
import com.master.talent.api.models.User;
import com.master.talent.api.service.SearchService;
import com.master.talent.api.solr.entity.MasterTalentItem;

@CrossOriginResourceSharing(
        allowOrigins = {
           "*"
        }, 
        allowCredentials = true, 
        maxAge = 1, 
        allowHeaders = {
           "MTUser"
        }
)
public class SearchServiceImpl implements SearchService {
	
	@Autowired
	private SearchDao dao;

	private final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);
	
	@Autowired
	private  RequestValidator validator;
	
	private final Gson serializer = new Gson();
	
	@Autowired
	private SearchDao searchDao;
	
	@Override
	public Response searchAutoComplete(HttpHeaders headers, String searchTerm, int start, int end) {
		validator.validateBefore(headers);
		try{			
			String[] searchTerms = searchTerm.split(" ");
			Object response = dao.searchRecommendation(searchTerms, start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}catch(Exception e){
			logger.debug("searchAutoComplete", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_AUTO_COMPLETE_FAIL,e.getMessage(),e);
		}
	}
	
	@Override
	public Response searchCompany(HttpHeaders headers, String searchTerm, int start, int end) {
		validator.validateBefore(headers);
		try{			
			String[] searchTerms = searchTerm.split(" ");
			Object response = dao.searchCompany(searchTerms, start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}catch(Exception e){
			logger.debug("searchAutoComplete", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_AUTO_COMPLETE_FAIL,e.getMessage(),e);
		}
	}

	@Override
	public Response searchItemsCompany(HttpHeaders headers, String type, String pageId, int start, int end) {
		validator.validateBefore(headers);
		try{			
			Object response = dao.searchItemsCompany(pageId, type, start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}catch(Exception e){
			logger.debug("searchAutoComplete", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_AUTO_COMPLETE_FAIL,e.getMessage(),e);
		}
	}


	@Override
	public Response getItemsByuser(HttpHeaders headers, String username, String type, int start, int end) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{
		List<MasterTalentItem> response = dao.listItemsByUsername(username, type, start, end);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
		}catch(Exception e){
			logger.debug("getItemsByuser", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_GET_ITEMS_FAIL,e.getMessage(),e);
		}
	}
	
	@Override
	public Response getDashboardItemsByuser(HttpHeaders headers, int start, int end) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{
		List<MasterTalentItem> response = dao.listDashboardItemsByUsername(session.getUsername(), start, end);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
		}catch(Exception e){
			logger.debug("getDashboardItemsByuser", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_GET_DASHBOARD_ITEMS_FAIL,e.getMessage(),e);
		}
	}

	@Override
	public Response searchItems(HttpHeaders headers, String type, String searchTerm, int start, int end, boolean subCategory) {
		validator.validateBefore(headers);
		Object response = null;
		try{			
			if(subCategory){
				response = dao.searchTutorialItems(searchTerm, start, end);
			} else {
				String[] searchTerms = searchTerm.split(" ");
				response = dao.searchItems(type, searchTerms, start, end);
			}
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}catch(Exception e){
			logger.debug("searchItems", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}
	
	@Override
	public Response searchTutorialItems(HttpHeaders headers, String category, int start, int end) {
		validator.validateBefore(headers);
		try{			
			Object response = dao.searchTutorialItems(category, start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}catch(Exception e){
			logger.debug("searchItems", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}

	@Override
	public Response getShortlistItems(HttpHeaders headers, int start, int end) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{			
			Object response = dao.searchShortlistItems(session.getUsername(), start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}catch(Exception e){
			logger.debug("searchItems", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}

	@Override
	public Response getAppliedItems(HttpHeaders headers, int start, int end) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{			
			Object response = dao.searchAppliedItems(session.getUsername(), start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}catch(Exception e){
			logger.debug("searchItems", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}
	
	@Override
	public Response gerRecruiterJobListings(HttpHeaders headers, int start, int end) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{			
			Object response = dao.gerRecruiterJobListings(session.getUsername(), start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}catch(Exception e){
			logger.debug("searchItems", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}
	
	@Override
	public Response getLikeItems(HttpHeaders headers, int start, int end) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{			
			Object response = dao.searchLikedItems(session.getUsername(), start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}catch(Exception e){
			logger.debug("searchItems", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}

	@Override
	public Response getExploreItems(HttpHeaders headers, String type, int start, int end) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{
			Object response =null;
			switch(type.toLowerCase()){ 
		    case "article":
		    	response = dao.processArticle(session.getArticleRecommdations(), type, start, end);
		    	break;
		    case "discussion":
		    	response = dao.processDiscussion(session.getDiscussionRecommdations(), type, start, end);
		    	break; 
		    case "media":
		    	response = dao.processMedia(session.getTutorialsRecommdations(),type, start, end);
		    	break; 
		    case "job":
		    	response = dao.processJob(session.getJobRecommdations(), type, start, end);
		    	break; 
		    default:
		    	logger.debug("Wrong request::", type);
		    	throw new InvalidRequestException(ExceptionCodes.UECodes.UE_DEFAULT,"Invalid Type::"+type); 
		    } 
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();			
		}catch(Exception e){
			logger.debug("searchItems", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
		
	}

	@Override
	public Response getUser(HttpHeaders headers, String username) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{
			SearchUser response =dao.getUser(username);
			
			if(!session.getUsername().equalsIgnoreCase(username)){
				response.setFriend(searchDao.verifyAFirend(session.getUsername(), username));
				response.setFollower(searchDao.verifyAFollower(session.getUsername(), username));
				if(!response.isFriend())
					response.setPendingFriend(searchDao.verifyAPendingFirend(session.getUsername(), username));
			}
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();	
		}catch(Exception e){
			logger.debug("getUser", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}

	@Override
	public Response getUserPendingList(HttpHeaders headers, int start, int end) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{
			List<User> response =dao.getPendingUsersList(session.getUsername(), start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();	
		}catch(Exception e){
			logger.debug("getUser", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}

	@Override
	public Response getUserRecommnedList(HttpHeaders headers, int start, int end) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{
			List<User> response =dao.getUserRecommenedList(session.getUsername(), start, end);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();	
		}catch(Exception e){
			logger.debug("getUser", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}	
	
}
