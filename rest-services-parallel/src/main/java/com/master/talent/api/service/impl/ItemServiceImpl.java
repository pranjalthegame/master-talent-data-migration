package com.master.talent.api.service.impl;

import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.master.talent.api.dao.ItemDao;
import com.master.talent.api.dao.SearchDao;
import com.master.talent.api.entity.ApplyEntity;
import com.master.talent.api.entity.CommentEntity;
import com.master.talent.api.entity.ItemEntity;
import com.master.talent.api.entity.LikeEntity;
import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.entity.ShortListEntity;
import com.master.talent.api.entity.UserPreSignUpEntity;
import com.master.talent.api.entity.repo.ApplyEntityRepo;
import com.master.talent.api.entity.repo.CommentEntityRepo;
import com.master.talent.api.entity.repo.LikeEntityRepo;
import com.master.talent.api.entity.repo.ShortlistEntityRepo;
import com.master.talent.api.entity.repo.UserPreSignUpEntityRepo;
import com.master.talent.api.models.Events;
import com.master.talent.api.models.ItemShare;
import com.master.talent.api.service.ItemService;
import com.master.talent.api.utils.SpringPropertiesUtil;
import com.master.talent.api.utils.IndexUtility;



@CrossOriginResourceSharing(
        allowOrigins = {
           "*"
        }, 
        allowCredentials = true, 
        maxAge = 1, 
        allowHeaders = {
           "MTUser"
        }
)
public class ItemServiceImpl implements ItemService {

	private final Logger logger = LoggerFactory.getLogger(ItemServiceImpl.class);
	
	@Autowired
	private ItemDao dao;
	
	@Autowired
	private SearchDao seachDao;
	
	@Autowired
	private  RequestValidator validator;
	
	private final Gson serializer = new Gson();
	
	@Autowired
	private LikeEntityRepo likeEntityRepo;
	@Autowired
	private ApplyEntityRepo applyEntityRepo;	
	@Autowired
	private ShortlistEntityRepo shortlistEntityRepo;	
	@Autowired
	private CommentEntityRepo commentEntityRepo;	
	@Autowired
	private UserPreSignUpEntityRepo userPreSignUpEntityRepo;
	@Autowired
	private IndexUtility indexUtility;

	@Override
	public Response createItem(HttpHeaders headers, ItemEntity obj) {
//		SessionEntity sessionEntity = validator.validateAndReturn(headers);
//		String username = obj.getOwner();
//		obj.setOwner(sessionEntity.getUsername());
		ItemEntity response = dao.createItem(obj, false);
		
		/*if(obj.getItemType().equalsIgnoreCase("job")){
			boolean status = true;
			UserPreSignUpEntity userPreSignUpEntity = userPreSignUpEntityRepo.findOne(UserPreSignUpEntity.ENTITY_NAME+sessionEntity.getUsername());
			if(userPreSignUpEntity != null){
				List<String> tempList = userPreSignUpEntity.getUserType();
				for(String userType: tempList){
					if(userType.equalsIgnoreCase("recruiter")){
						status = false;
					}
				}
				
				if(status){
					userPreSignUpEntity.getUserType().add("recruiter");
					userPreSignUpEntityRepo.save(userPreSignUpEntity);
					indexUtility.updateUserTypeWithRecriter(username, "recruiter");
				}
			}			
		}*/		
		
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response updateItem(HttpHeaders headers, ItemEntity obj) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		ItemEntity response = dao.updateItem(sessionEntity.getUsername(), obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response getItem(HttpHeaders headers, String itemId) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		ItemEntity response = dao.getItem(itemId);
		Events event = new Events();
		Long commentCount = commentEntityRepo.countByItemId(response.getItemId());
		if(commentCount==null)
			event.setCommentCount(0);
		else
			event.setCommentCount(commentCount);
		
		if(response.getPageId()!=null){
			response.setPageName(seachDao.getPageName(response.getPageId()));
			response.setPageUrl(seachDao.getPageUrl(response.getPageId()));
		}
		if(seachDao.verifyLike(sessionEntity.getUsername(), response.getItemId())){
			LikeEntity likeEntity = likeEntityRepo.findOne(LikeEntity.ENTITY_NAME+itemId);
			if(likeEntity != null)
				event.setLikedCount(likeEntity.getUsers().size());
			else
				event.setLikedCount(0);
			event.setLiked(true);
		}
		else{
			LikeEntity likeEntity = likeEntityRepo.findOne(LikeEntity.ENTITY_NAME+itemId);
			if(likeEntity != null)
				event.setLikedCount(likeEntity.getUsers().size());
			else
				event.setLikedCount(0);
			event.setLiked(false);
		}
		if(seachDao.verifyApply(sessionEntity.getUsername(), response.getItemId())){
			ApplyEntity applyEntity = applyEntityRepo.findOne(ApplyEntity.ENTITY_NAME+itemId);
			if(applyEntity != null)
				event.setAppliedCount(applyEntity.getUsers().size());
			else
				event.setAppliedCount(0);
			event.setApplied(true);
		}else{
			ApplyEntity applyEntity = applyEntityRepo.findOne(ApplyEntity.ENTITY_NAME+itemId);
			if(applyEntity != null)
				event.setAppliedCount(applyEntity.getUsers().size());
			else
				event.setAppliedCount(0);
			event.setApplied(false);
		}
		if(seachDao.verifyShorlist(sessionEntity.getUsername(), response.getItemId())){
			ShortListEntity shortListEntity = shortlistEntityRepo.findOne(ShortListEntity.ENTITY_NAME+itemId);
			if(shortListEntity != null)
				event.setShortlistedCount(shortListEntity.getUsers().size());
			else
				event.setShortlistedCount(0);
			event.setShortlisted(true);
		}else{
			ShortListEntity shortListEntity = shortlistEntityRepo.findOne(ShortListEntity.ENTITY_NAME+itemId);
			if(shortListEntity != null)
				event.setShortlistedCount(shortListEntity.getUsers().size());
			else
				event.setShortlistedCount(0);
			event.setShortlisted(false);
		}		
		response.setEvents(event);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response deleteItem(HttpHeaders headers, String itemId) {
		validator.validateBefore(headers);
		dao.deleteItem(itemId);
		return Response.ok(SpringPropertiesUtil.getProperty("delete.success.json.message"), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response createComment(HttpHeaders headers, CommentEntity obj) {
//		SessionEntity sessionEntity = validator.validateAndReturn(headers);
//		obj.setUsername(sessionEntity.getUsername());
		CommentEntity response = dao.createComment(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response updateComment(HttpHeaders headers, CommentEntity obj) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		CommentEntity response = dao.updateComment(sessionEntity.getUsername(), obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response getComment(HttpHeaders headers, String itemId, int from, int to) {
		validator.validateBefore(headers);
		List<CommentEntity> response = dao.getCommentByItem(itemId, from, to);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response deleteComment(HttpHeaders headers, String commentId) {
		validator.validateBefore(headers);
		dao.deleteComment(commentId);
		return Response.ok(SpringPropertiesUtil.getProperty("delete.success.json.message"), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response shareItem(HttpHeaders headers, ItemShare obj) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		obj.setShareOwner(sessionEntity.getUsername());
		ItemEntity response = dao.createShareItem(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}
	
	@Override
	public Response likeItem(HttpHeaders headers, String itemId) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		boolean status = false;
		String type;
		if(seachDao.verifyLike(sessionEntity.getUsername(), itemId)){
			status = dao.unlikeItem(sessionEntity.getUsername(), itemId);
				type = "unlike";
		} else {
			status = dao.createLikeItem(sessionEntity.getUsername(), itemId);
			type = "like";
		}
		if(status)
			return Response.ok("{\"success\" : \"Item updated\", \"type\" : \""+type+"\"}", MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
		else
			return Response.ok("{\"error\" : \"Item update failes\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
	}

	@Override
	public Response shortlistItem(HttpHeaders headers, String itemId) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		dao.createShortlistItem(sessionEntity.getUsername(), itemId);
		return Response.ok("{\"success\" : \"Item updated\"}", MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}

	@Override
	public Response applyItem(HttpHeaders headers, String itemId) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		dao.createApplyItem(sessionEntity, itemId);
		return Response.ok("{\"success\" : \"Item updated\"}", MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}

	@Override
	public Response itemViewed(HttpHeaders headers, String itemId) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		dao.itemViewedUpdate(sessionEntity.getUsername(), itemId);
		return Response.ok("{\"success\" : \"Item view updates\"}", MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
}
