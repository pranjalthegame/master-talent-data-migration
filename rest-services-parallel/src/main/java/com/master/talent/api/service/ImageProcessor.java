package com.master.talent.api.service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;

@Path("/")
public interface ImageProcessor {

	@POST
    @Path("/upload/userProfile")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadProfile(@Context HttpHeaders headers, Attachment attachments, 
    		@Context HttpServletRequest request, @QueryParam(value = "username") String username);
	
	@POST
    @Path("/upload/userBanner")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadBanner(@Context HttpHeaders headers, Attachment attachments, 
    		@Context HttpServletRequest request, @QueryParam(value = "username") String username);
	
	@POST
    @Path("/upload/item")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadItem(@Context HttpHeaders headers, Attachment attachments);
	
	@GET
    @Path("/delete/ItemImage")
    public Response deleteItemImage(@Context HttpHeaders headers, @QueryParam(value = "itemPath") String itemPath);

}