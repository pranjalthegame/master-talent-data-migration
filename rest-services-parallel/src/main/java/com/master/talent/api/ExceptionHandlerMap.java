/**
 * 
 */
package com.master.talent.api;

import java.util.Map;

/**
 * @author pranjal.sinha
 *
 */
public class ExceptionHandlerMap {

	private static Map<String, Object> handlerMap;

	public static Map<String, Object> getHandlerMap() {
		return handlerMap;
	}

	public void setHandlerMap(Map<String, Object> handlerMap) {
		ExceptionHandlerMap.handlerMap = handlerMap;
	}
	
	
	
}
