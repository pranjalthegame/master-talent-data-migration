package com.master.talent.data.model;


import java.util.Date;
import java.util.List;

public class UserModel
{
	
	private String gender;
	private String password;
	private String lname;
	private String emailAddress;
	private String relationship;
	private boolean isFollower;
	private String fname;
	private String breif;
	private boolean active;
	private String referral;
	private Banner banner;
	private String otp;
	private List<Employment> employment;
	private String registerLevel;
	private List<PhoneNumber> phone;
	private String dob;
//	private String fullName;
	private List<String> userType;
	private String username;
	private List<String> proInterest;
	private Date createdDate;
	
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getReferral() {
		return referral;
	}
	public void setReferral(String referral) {
		this.referral = referral;
	}
	public boolean isFollower() {
		return isFollower;
	}
	public void setFollower(boolean isFollower) {
		this.isFollower = isFollower;
	}
	public List<String> getUserType() {
		return userType;
	}
	public void setUserType(List<String> userType) {
		this.userType = userType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
/*	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = this.fname +" " +this.lname;
	}*/
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public List<PhoneNumber> getPhone() {
		return phone;
	}
	public void setPhone(List<PhoneNumber> phone) {
		this.phone = phone;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getBreif() {
		return breif;
	}
	public void setBreif(String breif) {
		this.breif = breif;
	}
	public List<String> getProInterest() {
		return proInterest;
	}
	public void setProInterest(List<String> proInterest) {
		this.proInterest = proInterest;
	}
	public List<Employment> getEmployment() {
		return employment;
	}
	public void setEmployment(List<Employment> employment) {
		this.employment = employment;
	}
	public String getRegisterLevel() {
		return registerLevel;
	}
	public void setRegisterLevel(String registerLevel) {
		this.registerLevel = registerLevel;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}	
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Banner getBanner() {
		return banner;
	}
	public void setBanner(Banner banner) {
		this.banner = banner;
	}
	
	@Override
	public String toString() {
		return "UserModel [gender=" + gender + ", password=" + password + ", lname=" + lname + ", emailAddress="
				+ emailAddress + ", relationship=" + relationship + ", isFollower=" + isFollower + ", fname=" + fname
				+ ", breif=" + breif + ", active=" + active + ", referral=" + referral + ", banner=" + banner + ", otp="
				+ otp + ", employment=" + employment + ", registerLevel=" + registerLevel + ", phone=" + phone
				+ ", dob=" + dob + ", userType=" + userType + ", username=" + username + ", proInterest=" + proInterest
				+ ", createdDate=" + createdDate + "]";
	}
	
	
}
