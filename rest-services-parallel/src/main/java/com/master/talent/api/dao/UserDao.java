package com.master.talent.api.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.master.talent.api.entity.CompanyEntity;
import com.master.talent.api.entity.UserAwardsAndRecogEntity;
import com.master.talent.api.entity.UserEducationEntity;
import com.master.talent.api.entity.UserFollowersEntity;
import com.master.talent.api.entity.UserFriendsEntity;
import com.master.talent.api.entity.UserLocationEntity;
import com.master.talent.api.entity.UserOccupationEntity;
import com.master.talent.api.entity.UserPreSignUpEntity;
import com.master.talent.api.entity.UserSkillsEntity;
import com.master.talent.api.entity.UserEarningEntity;
import com.master.talent.api.entity.UserBankDetailsEntity;
import com.master.talent.api.entity.CompanyFollowersEntity;
import com.master.talent.api.entity.mapper.UserDaoMapper;
import com.master.talent.api.entity.repo.CompanyEntityRepo;
import com.master.talent.api.entity.repo.UserAwarsAndRecogEntityRepo;
import com.master.talent.api.entity.repo.UserEducationEntityRepo;
import com.master.talent.api.entity.repo.UserFollowersEntityRepo;
import com.master.talent.api.entity.repo.UserEarningEntityRepo;
import com.master.talent.api.entity.repo.UserFriendsEntityRepo;
import com.master.talent.api.entity.repo.UserLocationEntityRepo;
import com.master.talent.api.entity.repo.UserOccupationEntityRepo;
import com.master.talent.api.entity.repo.UserPreSignUpEntityRepo;
import com.master.talent.api.entity.repo.UserSkillsEntityRepo;
import com.master.talent.api.entity.repo.UserBankDetailsEntityRepo;
import com.master.talent.api.entity.repo.CompanyFollowersEntityRepo;
import com.master.talent.api.exception.CouchbaseWriteFailureException;
import com.master.talent.api.exception.ResourceNotFoundException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.Followers;
import com.master.talent.api.models.Friends;
import com.master.talent.api.models.Location;
import com.master.talent.api.models.User;
import com.master.talent.api.utils.IndexUtility;

@Component
public class UserDao {
	
	private final Logger logger = LoggerFactory.getLogger(UserDao.class);
	
	@Autowired
	private UserAwarsAndRecogEntityRepo userAwarsAndRecogEntityRepo;
	@Autowired
	private UserEducationEntityRepo userEducationEntityRepo;	
	@Autowired
	private UserOccupationEntityRepo userOccupationEntityRepo;	
	@Autowired
	private UserPreSignUpEntityRepo userPreSignUpEntityRepo;
	@Autowired
	private UserSkillsEntityRepo userSkillsEntityRepo;
	@Autowired
	private UserFriendsEntityRepo userFriendsEntityRepo;
	@Autowired
	private UserFollowersEntityRepo userFollowersEntityRepo;
	@Autowired
	private UserDaoMapper userDaoMapper;
	@Autowired
	private SearchDao searchDao;
	@Autowired
	private IndexUtility indexUtility;
	@Autowired
	private UserLocationEntityRepo userLocationEntityRepo;
	@Autowired
	private CompanyEntityRepo companyEntityRepo;
	@Autowired
	private CompanyFollowersEntityRepo companyFollowersEntityRepo;
	@Autowired
	private UserEarningEntityRepo userEarningEntityRepo;
	@Autowired
	private UserBankDetailsEntityRepo userBankDetailsEntityRepo;
	
	public UserPreSignUpEntity createUserPreSignUpEntity(UserPreSignUpEntity obj) {
		try{
			if(userPreSignUpEntityRepo.findOne(UserPreSignUpEntity.ENTITY_NAME+obj.getUsername()) == null){
				byte[] encoded = Base64.encodeBase64(obj.getPassword().getBytes());  
				obj.setPassword(new String(encoded));				
				indexUtility.indexDocToSolr(obj, "CREATE");				
				return userPreSignUpEntityRepo.save(obj);
			} else {
				logger.info("User with user name {} already exists. Throwing Exception!!",obj.getUsername());
				throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_EXISTS_USER,"");
			}
		}catch(Exception e){
			logger.debug("createUserPreSignUpEntity", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_DEFAULT,e.getMessage(),e);
		}
	}
	
	public UserPreSignUpEntity updateUserPreSignUpEntity(UserPreSignUpEntity obj) {
		try{
			UserPreSignUpEntity userPreSignUpEntity = userPreSignUpEntityRepo.findOne(UserPreSignUpEntity.ENTITY_NAME+obj.getUsername());
			UserPreSignUpEntity finalEntity = userDaoMapper.mapper(userPreSignUpEntity, obj);
			indexUtility.indexDocToSolr(userPreSignUpEntity, "UPDATE");
			return userPreSignUpEntityRepo.save(finalEntity);
		}catch(Exception e){
			logger.debug("updateUserPreSignUpEntity", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USER_UPDATE_FAIL,e.getMessage(),e);
		}
	}	
	public UserPreSignUpEntity getUserPreSignUpEntity(String username) {
		try{
			UserPreSignUpEntity userPreSignUpEntity = userPreSignUpEntityRepo.findOne(UserPreSignUpEntity.ENTITY_NAME+username);
			
			return userPreSignUpEntity;
		}catch(Exception e){
			logger.debug("getUserPreSignUpEntity", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_NOT_FOUND,e.getMessage(),e);
		}
	}	
	
	public List<User> getUserPreSignUpByType(String type, int start, int end) {
		try{
			return indexUtility.getPendingUsers(type, start, end);
		}catch(Exception e){
			logger.debug("getUserPreSignUpEntity", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_NOT_FOUND,e.getMessage(),e);
		}
	}
	
	public UserPreSignUpEntity getUserPreSignUpEntityForLogin(String username) {		
			UserPreSignUpEntity userPreSignUpEntity = userPreSignUpEntityRepo.findOne(UserPreSignUpEntity.ENTITY_NAME+username);	
			if(userPreSignUpEntity == null){
				return null;
			}else{
				return userPreSignUpEntity;
			}
	}	
	
	public UserPreSignUpEntity getUserByEmail(String emailAddress) {
		List<UserPreSignUpEntity> userPreSignUpEntity =userPreSignUpEntityRepo.findByEmailAddress(emailAddress);
		if(userPreSignUpEntity != null){
			if(!userPreSignUpEntity.isEmpty()){
				return userPreSignUpEntity.get(0);
			}else{
				return null;
			}
		}else{
			return null;
		}
	}	
	
	public UserPreSignUpEntity deleteUserPreSignUpEntity(String username) {
		try{
			UserPreSignUpEntity userPreSignUpEntity = userPreSignUpEntityRepo.findOne(UserPreSignUpEntity.ENTITY_NAME+username);
			userPreSignUpEntity.setDeleted(true);
			return userPreSignUpEntityRepo.save(userPreSignUpEntity);
		}catch(Exception e){
			logger.debug("deleteUserPreSignUpEntity", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_NOT_FOUND,e.getMessage(),e);
		}
	}
	
	public UserSkillsEntity createUserSkills(UserSkillsEntity obj) {
		try{
			if(userSkillsEntityRepo.findOne(UserSkillsEntity.ENTITY_NAME+obj.getUsername()) == null){
				return userSkillsEntityRepo.save(obj);
			} else {
				throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_EXISTS_USERSKILLS,"");
			}
		}catch(Exception e){
			logger.debug("createUserSkills", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USERSKILLS_CREATE_FAIL,e.getMessage(),e);
		}
	}
	public UserSkillsEntity updateUserSkills(UserSkillsEntity obj) {
		try{
			return userSkillsEntityRepo.save(obj);
		}catch(Exception e){
			logger.debug("updateUserSkills", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USERSKILLS_UPDATE_FAIL,e.getMessage(),e);
		}
	}	
	public UserSkillsEntity getUserUserSkills(String username) {
		try{
			UserSkillsEntity userSkillsEntity = userSkillsEntityRepo.findOne(UserSkillsEntity.ENTITY_NAME+username);
			return userSkillsEntity;
		}catch(Exception e){
			logger.debug("getUserUserSkills", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_SKILLS_NOT_FOUND,e.getMessage(),e);
		}
	}
	
	public UserSkillsEntity deleteUserSkills(String username) {
		try{
			UserSkillsEntity userSkillsEntity = userSkillsEntityRepo.findOne(UserSkillsEntity.ENTITY_NAME+username);
			userSkillsEntity.setDeleted(true);
			return userSkillsEntityRepo.save(userSkillsEntity);
		}catch(Exception e){
			logger.debug("deleteUserSkills", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_SKILLS_DELETE_FAIL,e.getMessage(),e);
		}
	}	
	
	public UserOccupationEntity createUserOccupation(UserOccupationEntity obj) {
		try{
			if(userOccupationEntityRepo.findOne(UserOccupationEntity.ENTITY_NAME+obj.getUsername()) == null){
				indexUtility.indexOccupationToSolr(obj);
				return userOccupationEntityRepo.save(obj);
			} else {
				throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_EXISTS_USER_OCCUPATION,"");
			}
		}catch(Exception e){
			logger.debug("createUserOccupation", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USER_OCC_CREATE_FAIL,e.getMessage(),e);
		}
	}
	
	public UserOccupationEntity updateUserOccupation(UserOccupationEntity obj) {
		try{
			indexUtility.indexOccupationToSolr(obj);
			return userOccupationEntityRepo.save(obj);			
		}catch(Exception e){
			logger.debug("updateUserOccupation", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USER_OCC_UPDATE_FAIL,e.getMessage(),e);
		}
	}	
	
	public UserOccupationEntity getUserUserOccupation(String username) {
		try{
			UserOccupationEntity userOccupationEntity = userOccupationEntityRepo.findOne(UserOccupationEntity.ENTITY_NAME+username);
			return userOccupationEntity;
		}catch(Exception e){
			logger.debug("getUserUserOccupation", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_OCC_NOT_FOUND,e.getMessage(),e);
		}
	}	
	public UserOccupationEntity deleteUserOccupation(String username) {
		try{
			UserOccupationEntity userOccupationEntity = userOccupationEntityRepo.findOne(UserOccupationEntity.ENTITY_NAME+username);
			userOccupationEntity.setDeleted(true);
			return userOccupationEntityRepo.save(userOccupationEntity);
		}catch(Exception e){
			logger.debug("deleteUserOccupation", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_OCC_DELETE_FAIL,e.getMessage(),e);
		}
	}
	
	
	public UserEducationEntity createUserEducation(UserEducationEntity obj) {
		try{
			if(userEducationEntityRepo.findOne(UserEducationEntity.ENTITY_NAME+obj.getUsername()) == null){
				return userEducationEntityRepo.save(obj);
			} else {
				throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_EXISTS_USER_EDUCATION,"");
			}
		}catch(Exception e){
			logger.debug("createUserEducation", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USER_EDU_CREATE_FAIL,e.getMessage(),e);
		}
	}
	public UserEducationEntity updateUserEducation(UserEducationEntity obj) {
		try{
			return userEducationEntityRepo.save(obj);
		}catch(Exception e){
			logger.debug("updateUserEducation", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USER_EDU_UPDATE_FAIL,e.getMessage(),e);
		}
	}
	public UserEducationEntity getUserEducation(String username) {
		try{
			UserEducationEntity userEducationEntity = userEducationEntityRepo.findOne(UserEducationEntity.ENTITY_NAME+username);
			return userEducationEntity;
		}catch(Exception e){
			logger.debug("getUserEducation", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_EDU_NOT_FOUND,e.getMessage(),e);
		}
	}	
	public UserEducationEntity deleteUserEducation(String username) {
		try{
			UserEducationEntity userEducationEntity = userEducationEntityRepo.findOne(UserEducationEntity.ENTITY_NAME+username);
			userEducationEntity.setDeleted(true);
			return userEducationEntityRepo.save(userEducationEntity);
		}catch(Exception e){
			logger.debug("deleteUserEducation", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_EDU_DELETE_FAIL,e.getMessage(),e);
		}
	}
	
	
	public UserAwardsAndRecogEntity createUserAwardsAndRecog(UserAwardsAndRecogEntity obj) {
		try{
			if(userAwarsAndRecogEntityRepo.findOne(UserAwardsAndRecogEntity.ENTITY_NAME+obj.getUsername()) == null){
				return userAwarsAndRecogEntityRepo.save(obj);
			} else {
				throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_EXISTS_USER_AWARDS,"");
			}
		}catch(Exception e){
			logger.debug("createUserAwardsAndRecog", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USER_AWARDS_CREATE_FAIL,e.getMessage(),e);
		}
	}
	public UserAwardsAndRecogEntity updateUserAwardsAndRecog(UserAwardsAndRecogEntity obj) {
		try{
				return userAwarsAndRecogEntityRepo.save(obj);
		}catch(Exception e){
			logger.debug("updateUserAwardsAndRecog", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USER_AWARDS_UPDATE_FAIL,e.getMessage(),e);
		}
	}	
	public UserAwardsAndRecogEntity getUserAwardsAndRecog(String username) {
		try{
			UserAwardsAndRecogEntity userAwardsAndRecogEntity = userAwarsAndRecogEntityRepo.findOne(UserAwardsAndRecogEntity.ENTITY_NAME+username);
			return userAwardsAndRecogEntity;
		}catch(Exception e){
			logger.debug("getUserAwardsAndRecog", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_AWARDS_NOT_FOUND,e.getMessage(),e);
		}
	}	
	public UserAwardsAndRecogEntity deleteUserAwardsAndRecog(String username) {
		try{
			UserAwardsAndRecogEntity userAwardsAndRecogEntity = userAwarsAndRecogEntityRepo.findOne(UserAwardsAndRecogEntity.ENTITY_NAME+username);
			userAwardsAndRecogEntity.setDeleted(true);
			return userAwarsAndRecogEntityRepo.save(userAwardsAndRecogEntity);
		}catch(Exception e){
			logger.debug("deleteUserAwardsAndRecog", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_EDU_DELETE_FAIL,e.getMessage(),e);
		}
	}
	public boolean validateOTP(String username, String otp) {
		try{
			UserPreSignUpEntity userPreSignUpEntity = userPreSignUpEntityRepo.findOne(UserPreSignUpEntity.ENTITY_NAME+username);
			
			if(otp.equalsIgnoreCase(userPreSignUpEntity.getOtp())){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			logger.debug("validateOTp", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_VALIDATE_OTP_FAIL,e.getMessage(),e);
		}
		
	}
	public boolean addFriend(String requestor, String requested) {
		try{
			//update user to received a request
			addFriendandIndex(requestor, requested);
			//update user to raised a request
			addFriendandRequestorIndex(requestor, requested);
			return true;
		}catch(Exception e){
			logger.debug("addFriend", e);
			return false;
		}
	}
	public boolean deleteFriend(String username, String deleteUsername) {
		try{
			UserFriendsEntity userEntity =userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+username);
			List<Friends> friendList = userEntity.getFriends();
			for (Friends friends : friendList) {
				if(friends.getFriendName().equalsIgnoreCase(deleteUsername)){
					friendList.remove(friends);
				}
				
			}
			userEntity.setFriends(friendList);
			
			UserFriendsEntity userOtherEntity =userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+deleteUsername);
			List<Friends> friendOtherList = userOtherEntity.getFriends();
			for (Friends friends : friendOtherList) {
				if(friends.getFriendName().equalsIgnoreCase(deleteUsername)){
					friendOtherList.remove(friends);
				}
				
			}
			userOtherEntity.setFriends(friendOtherList);
			
			//update search index
			indexUtility.updateUserFriendsIndxList(deleteUsername,username,"DELETE");			
			userFriendsEntityRepo.save(userEntity);
			userFriendsEntityRepo.save(userOtherEntity);
			return true;
		}catch(Exception e){
			logger.debug("addFriend", e);
			return false;
		}
	}
	public boolean acceptFriend(String username, String requestor) {
		try{
			UserFriendsEntity userEntity =userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+username);
			List<Friends> friendList = userEntity.getFriends();
			for (Friends friends : friendList) {
				if(friends.getFriendName().equalsIgnoreCase(requestor)){
					friends.setIsActive(true);
					friends.setAcceptedTime(DateTime.now());
				}
				
			}
			userEntity.setFriends(friendList);
			
			UserFriendsEntity userRequestEntity =userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+requestor);
			List<Friends> friendRequestList = userEntity.getFriends();
			for (Friends friendRequest : friendRequestList) {
				if(friendRequest.getFriendName().equalsIgnoreCase(username)){
					friendRequest.setIsActive(true);
					friendRequest.setAcceptedTime(DateTime.now());
				}
				
			}
			userRequestEntity.setFriends(friendRequestList);
			
			//update search index
			indexUtility.updateUserFriendsRequestorIndxList(requestor, username, "ACCEPT");	
			indexUtility.updateUserFriendsIndxList(username, requestor,"ACCEPT");	
			
			userFriendsEntityRepo.save(userRequestEntity);
			userFriendsEntityRepo.save(userEntity);
			return true;
		}catch(Exception e){
			logger.debug("addFriend", e);
			return false;
		}
	}
	private void addFriendandIndex(String requestor, String requested) {
		try{
			UserFriendsEntity userEntity;
			if(userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+requested) == null){
				userEntity = new UserFriendsEntity();
				userEntity.setUsername(requested);
				
				//create friend
				Friends friends = new Friends();
				friends.setFriendName(requestor);
				friends.setIsActive(false);
				friends.setRequestedTime(DateTime.now());
				friends.setRequestor(true);				
				List<Friends> listFriends = new ArrayList<Friends>();
				listFriends.add(friends);
				userEntity.setFriends(listFriends);				
				indexUtility.updateUserFriendsIndxList(requestor,requested,"REQUEST");
			} else{
				userEntity = userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+requested);
				List<Friends> listFriends = userEntity.getFriends();
				
				Friends friends = new Friends();
				friends.setFriendName(requestor);
				friends.setIsActive(false);
				friends.setRequestedTime(DateTime.now());	
				friends.setRequestor(true);		
				listFriends.add(friends);
				userEntity.setFriends(listFriends);					
				indexUtility.updateUserFriendsIndxList(requestor,requested,"REQUEST");
			}
			userFriendsEntityRepo.save(userEntity);
		}catch(Exception e){
			logger.debug("addFriend", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_ADD_FRIEND_AND_INDEX_FAIL,e.getMessage(),e);
		}
	}
	
	private void addFriendandRequestorIndex(String requestor, String requested) {
		try{
			UserFriendsEntity userEntity;
			if(userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+requestor) == null){
				userEntity = new UserFriendsEntity();
				userEntity.setUsername(requestor);
				
				//create friend
				Friends friends = new Friends();
				friends.setFriendName(requested);
				friends.setIsActive(false);
				friends.setRequestedTime(DateTime.now());
				friends.setRequestor(false);
				List<Friends> listFriends = new ArrayList<Friends>();
				listFriends.add(friends);
				userEntity.setFriends(listFriends);				
				indexUtility.updateUserFriendsRequestorIndxList(requestor,requested,"REQUEST");
			} else{
				userEntity = userFriendsEntityRepo.findOne(UserFriendsEntity.ENTITY_NAME+requestor);
				List<Friends> listFriends = userEntity.getFriends();
				
				Friends friends = new Friends();
				friends.setFriendName(requested);
				friends.setIsActive(false);
				friends.setRequestedTime(DateTime.now());	
				friends.setRequestor(false);	
				listFriends.add(friends);
				userEntity.setFriends(listFriends);					
				indexUtility.updateUserFriendsRequestorIndxList(requestor,requested,"REQUEST");
			}
			userFriendsEntityRepo.save(userEntity);
		}catch(Exception e){
			logger.debug("addFriend", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_ADD_FRIEND_AND_INDEX_FAIL,e.getMessage(),e);
		}
	}
	
	
	public boolean addFollower(String follower, String followed) {
		try{
			//update user to received a request
			addFollowerIndex(follower, followed, "REQUESTED");
			return true;
		}catch(Exception e){
			logger.debug("addFriend", e);
			return false;
		}
	}
	
	public boolean addCompanyFollower(String username, String compId) {
		try{
			//update user to received a request
			addCompanyFollowerIndex(username, compId, "REQUESTED");
			return true;
		}catch(Exception e){
			logger.debug("addFriend", e);
			return false;
		}
	}
	
	private void addCompanyFollowerIndex(String username, String compId, String type) {
		try{
			//create or update the followed
			CompanyFollowersEntity companyFollowersEntity;
			if(companyFollowersEntityRepo.findOne(compId) == null){
				companyFollowersEntity = new CompanyFollowersEntity();
				companyFollowersEntity.setCompId(compId);
				
				Followers followers = new Followers();
				followers.setFollowerName(username);
				followers.setRequestedTime(DateTime.now());
				
				List<Followers> listFollowers = new ArrayList<Followers>();
				listFollowers.add(followers);
				companyFollowersEntity.setFollowers(listFollowers);	
				//ashok
				indexUtility.updateCompanyFollowIndxList(username,compId, "REQUEST");
			} else{
				companyFollowersEntity = companyFollowersEntityRepo.findOne(compId);
				List<Followers> listFollowers = companyFollowersEntity.getFollowers();
				
				Followers followers = new Followers();
				followers.setFollowerName(username);
				followers.setRequestedTime(DateTime.now());
				
				listFollowers.add(followers);
				companyFollowersEntity.setFollowers(listFollowers);
				//ashok
				indexUtility.updateCompanyFollowIndxList(username,compId, "REQUEST");
			}
			companyFollowersEntityRepo.save(companyFollowersEntity);
			
			//create or update the follower			
			UserFollowersEntity followeeUserEntity;
			if(userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+username) == null){
				followeeUserEntity = new UserFollowersEntity();
				followeeUserEntity.setUsername(username);
				
				//create friend
				Followers followers = new Followers();
				followers.setFollowerName(compId);
				followers.setRequestedTime(DateTime.now());
				
				List<Followers> listFollowers = new ArrayList<Followers>();
				listFollowers.add(followers);
				followeeUserEntity.setCompanies(listFollowers);				
			} else{
				followeeUserEntity = userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+username);
				List<Followers> listFollowers;
				if(followeeUserEntity.getCompanies() !=null){
					listFollowers = followeeUserEntity.getCompanies();
				}else{
					listFollowers = new ArrayList<Followers>();
				}
				Followers followers = new Followers();
				followers.setFollowerName(compId);
				followers.setRequestedTime(DateTime.now());				
				listFollowers.add(followers);
				followeeUserEntity.setCompanies(listFollowers);					
			}
			userFollowersEntityRepo.save(followeeUserEntity);
			
		}catch(Exception e){
			logger.debug("addFriend", e);
			//throw new CouchbaseWriteFailureException("User friends write failure for user::"+requested);
		}
	}
	
	private void addFollowerIndex(String follower, String followed, String type) {
		try{
			//create or update the followed
			UserFollowersEntity userEntity;
			if(userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+followed) == null){
				userEntity = new UserFollowersEntity();
				userEntity.setUsername(followed);
				
				//create friend
				Followers followers = new Followers();
				followers.setFollowerName(follower);
				followers.setRequestedTime(DateTime.now());
				
				List<Followers> listFollowers = new ArrayList<Followers>();
				listFollowers.add(followers);
				userEntity.setFollowers(listFollowers);				
				indexUtility.updateUserFollowIndxList(follower,followed, "REQUEST");
			} else{
				userEntity = userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+followed);
				List<Followers> listFollowers = userEntity.getFollowers();
				
				Followers followers = new Followers();
				followers.setFollowerName(follower);
				followers.setRequestedTime(DateTime.now());
				
				listFollowers.add(followers);
				userEntity.setFollowers(listFollowers);					
				indexUtility.updateUserFollowIndxList(follower,followed, "REQUEST");
			}
			userFollowersEntityRepo.save(userEntity);
			
			//create or update the follower			
			UserFollowersEntity followeeUserEntity;
			if(userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+follower) == null){
				followeeUserEntity = new UserFollowersEntity();
				followeeUserEntity.setUsername(follower);
				
				//create friend
				Followers followers = new Followers();
				followers.setFollowerName(followed);
				followers.setRequestedTime(DateTime.now());
				
				List<Followers> listFollowers = new ArrayList<Followers>();
				listFollowers.add(followers);
				followeeUserEntity.setFollowee(listFollowers);				
			} else{
				followeeUserEntity = userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+follower);
				List<Followers> listFollowers;
				if(followeeUserEntity.getFollowee() !=null){
					listFollowers = followeeUserEntity.getFollowee();
				}else{
					listFollowers = new ArrayList<Followers>();
				}
				Followers followers = new Followers();
				followers.setFollowerName(followed);
				followers.setRequestedTime(DateTime.now());				
				listFollowers.add(followers);
				followeeUserEntity.setFollowee(listFollowers);					
			}
			userFollowersEntityRepo.save(followeeUserEntity);
			
		}catch(Exception e){
			logger.debug("addFriend", e);
			//throw new CouchbaseWriteFailureException("User friends write failure for user::"+requested);
		}
	}
	
	public boolean unCompanyFollow(String username, String compId) {
		try{
			UserFollowersEntity userEntity =userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+username);
			if(userEntity !=null){
				List<Followers> followeeList = userEntity.getCompanies();
				
				Iterator<Followers> followeeListIterator = followeeList.iterator();
				while (followeeListIterator.hasNext()) {
					Followers followers = followeeListIterator.next();
				    if (followers.getFollowerName().equals(compId)) {
				    	followeeListIterator.remove();
				    }
				}
				userEntity.setCompanies(followeeList);
			}
			CompanyFollowersEntity companyFollowersEntity = companyFollowersEntityRepo.findOne(compId);
			if(companyFollowersEntity !=null){
				List<Followers> followersList = companyFollowersEntity.getFollowers();
				if(followersList!=null) {
					Iterator<Followers> followersListIterator = followersList.iterator();
					while (followersListIterator.hasNext()) {
						Followers followers = followersListIterator.next();
					    if (followers.getFollowerName().equals(username)) {
					    	followersListIterator.remove();
					    }
					}
				companyFollowersEntity.setFollowers(followersList);
				}
			}
			//update search index
			indexUtility.updateCompanyFollowIndxList(username,compId,"DELETE");
			userFollowersEntityRepo.save(userEntity);	
			companyFollowersEntityRepo.save(companyFollowersEntity);
			return true;
		}catch(Exception e){
			logger.debug("unFollow", e);
			return false;
		}
	}
	
	public boolean unFollow(String follower, String followed) {
		try{
			UserFollowersEntity userEntity =userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+follower);
			if(userEntity !=null){
				List<Followers> followeeList = userEntity.getFollowee();
				
				Iterator<Followers> followeeListIterator = followeeList.iterator();
				while (followeeListIterator.hasNext()) {
					Followers followers = followeeListIterator.next();
				    if (followers.getFollowerName().equals(followed)) {
				    	followeeListIterator.remove();
				    }
				}
				userEntity.setFollowee(followeeList);
			}
			UserFollowersEntity userFollwedEntity =userFollowersEntityRepo.findOne(UserFollowersEntity.ENTITY_NAME+followed);
			if(userFollwedEntity !=null){
				List<Followers> followersList = userFollwedEntity.getFollowers();
				if(followersList!=null) {
					Iterator<Followers> followersListIterator = followersList.iterator();
					while (followersListIterator.hasNext()) {
						Followers followers = followersListIterator.next();
					    if (followers.getFollowerName().equals(follower)) {
					    	followersListIterator.remove();
					    }
					}
				userFollwedEntity.setFollowers(followersList);
				}
			}
			//update search index
				
			indexUtility.updateUserFollowIndxList(follower,followed,"DELETE");
			userFollowersEntityRepo.save(userFollwedEntity);
			userFollowersEntityRepo.save(userEntity);				
			return true;
		}catch(Exception e){
			logger.debug("unFollow", e);
			return false;
		}
	}

	public UserLocationEntity getUserLocation(String username) {
		try{
			return userLocationEntityRepo.findOne(UserLocationEntity.ENTITY_NAME+username);
		}catch(Exception e){
			logger.debug("getUserLocation", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_NOT_FOUND,e.getMessage(),e);
		}
	}

	public UserLocationEntity createUserLocation(UserLocationEntity obj) {
		try{
			boolean status = true;
			UserLocationEntity userLocationEntity = userLocationEntityRepo.findOne(UserLocationEntity.ENTITY_NAME+obj.getUsername());
			if(userLocationEntity ==null){
				List<Location> locationList = obj.getLocation();
				for (Location location : locationList) {
					if(location.isCurrent() == true){
						status = false;
						indexUtility.indexLocationToUserSolr(obj.getUsername(), location.getLocation(), location.getCountry());	
					}					
				}
				if(status)
					throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USER_LOCATION_FAIL);
				UserLocationEntity returnObj = userLocationEntityRepo.save(obj);
				return returnObj;
			} else {
				throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USERSKILLS_UPDATE_FAIL);
			}
		}catch(Exception e){
			logger.debug("createUserLocation", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USERSKILLS_UPDATE_FAIL,e.getMessage(),e);
		}
	}

	public UserLocationEntity updateUserLocation(UserLocationEntity obj) {
		try{
			boolean status = true;
			List<Location> locationList = obj.getLocation();
			for (Location location : locationList) {
				if(location.isCurrent() == true){
					status = false;
					indexUtility.indexLocationToUserSolr(obj.getUsername(), location.getLocation(), location.getCountry());	
				}					
			}
			if(status)
				throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USER_LOCATION_FAIL);
			UserLocationEntity userLocationEntity =userLocationEntityRepo.save(obj);
			return userLocationEntity;
		}catch(Exception e){
			logger.debug("createUserLocation", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USERSKILLS_UPDATE_FAIL,e.getMessage(),e);
		}
	}

	public CompanyEntity createCompany(String username, CompanyEntity obj) {
		try{
			obj.setPageId(UUID.randomUUID().toString());
			obj.setOwner(username);
			obj.setCreated(new DateTime());
			indexUtility.indexCompanyToSolr(obj, "CREATE");
			CompanyEntity companyEntity =companyEntityRepo.save(obj);
			return companyEntity;
		}catch(Exception e){
			logger.debug("createCompany", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USERSKILLS_UPDATE_FAIL,e.getMessage(),e);
		}
	}

	public CompanyEntity updateCompany(String username, CompanyEntity dbObj, CompanyEntity obj) {
		try{
			dbObj = userDaoMapper.companyMapper(dbObj, obj);
			indexUtility.indexCompanyToSolr(dbObj, "UPDATE");
			return companyEntityRepo.save(dbObj);
		}catch(Exception e){
			logger.debug("updateCompany", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_USERSKILLS_UPDATE_FAIL,e.getMessage(),e);
		}
	}

	public List<CompanyEntity> getAllCompanyByUser(String username, int start, int end) {
		try{
			return indexUtility.getAllCompanyByUser(username, start, end);
		}catch(Exception e){
			logger.debug("updateCompany", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_EDU_DELETE_FAIL,e.getMessage(),e);
		}
	}
	
	public List<UserEarningEntity> getAllEarnings(String username, String type) {
		try{
			if(type.equalsIgnoreCase("nr")){
				return userEarningEntityRepo.getByUsername(username);
			} else if(type.equalsIgnoreCase("all")){
				return userEarningEntityRepo.getById(username);
			}
			return new ArrayList<UserEarningEntity>();
		}catch(Exception e){
			logger.debug("updateCompany", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_USER_EDU_DELETE_FAIL,e.getMessage(),e);
		}
	}
	
	public UserBankDetailsEntity createBankDetails(UserBankDetailsEntity obj) {
		try{
			return userBankDetailsEntityRepo.save(obj);
		}catch(Exception e){
			logger.debug("createBankDetails", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_DEFAULT,e.getMessage(),e);
		}
	}

	public UserBankDetailsEntity getBankDetails(String username) {
		try{
			UserBankDetailsEntity userBankDetailsEntity = userBankDetailsEntityRepo.findOne(UserBankDetailsEntity.ENTITY_NAME+username);
			return userBankDetailsEntity;
		}catch(Exception e){
			logger.debug("getBankDetails", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_DEFAULT,e.getMessage(),e);
		}
	}
	
}
