package com.master.talent.api.entity;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Education;
import com.master.talent.api.models.Followers;

@Document(expiry=0)
public class CompanyFollowersEntity {
	
	@Id
	@Field
	private String compId;
	
	
	@Field
	private List<Followers> followers;

	public String getCompId() {
		return compId;
	}

	public void setCompId(String compId) {
		this.compId = compId;
	}

	public List<Followers> getFollowers() {
		return followers;
	}

	public void setFollowers(List<Followers> followers) {
		this.followers = followers;
	}

	@Override
	public String toString() {
		return "CompanyFollowersEntity [compId=" + compId + ", followers="
				+ followers + "]";
	}
	
}
