/**
 * 
 */
package com.master.talent.data.service;

/**
 * @author pranjal.sinha
 *
 */
public interface UserFeedService {

	void processUsers();
	
	void processUsersConcurrently();
}
