package com.master.talent.api.models;

import org.joda.time.DateTime;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.GenerationStrategy;


public class Chat {	
	private String chatId;
	private String chatOwner;
	private String mesage;
    private DateTime created;
    private boolean deleted;
    
	public String getChatOwner() {
		return chatOwner;
	}
	public void setChatOwner(String chatOwner) {
		this.chatOwner = chatOwner;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	public String getMesage() {
		return mesage;
	}
	public void setMesage(String mesage) {
		this.mesage = mesage;
	}
	public DateTime getCreated() {
		return created;
	}
	public void setCreated(DateTime created) {
		this.created = created;
	}
	@Override
	public String toString() {
		return "Chat [chatId=" + chatId + ", mesage=" + mesage + ", created="
				+ created + "]";
	}
}
