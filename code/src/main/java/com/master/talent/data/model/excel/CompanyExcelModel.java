/**
 * 
 */
package com.master.talent.data.model.excel;

import com.poiji.internal.annotation.ExcelCell;

/**
 * @author pranjal.sinha
 *
 */
public class CompanyExcelModel {

	@ExcelCell(0)
	private String owner;
	@ExcelCell(1)
	private String ownershipType;
	@ExcelCell(2)
	private String offers;//list<String>
	@ExcelCell(3)
	private String address;//List<String>
	@ExcelCell(4)
	private String turnOverCurrency;
	@ExcelCell(5)
	private String foundedOn;
	@ExcelCell(6)
	private String created; //DateTime
	@ExcelCell(7)
	private String turnOverValue;
	@ExcelCell(8)
	private String processAuditStatus;
	@ExcelCell(9)
	private String type;
	@ExcelCell(10)
	private String title;
	@ExcelCell(11)
	private boolean isDeleted;
	@ExcelCell(12)
	private String websiteUrl; //List<String>
	@ExcelCell(13)
	private String phone; //List<PhoneNumber>
	@ExcelCell(14)
	private String pageUrl;
	@ExcelCell(15)
	private String financialAuditStatus;
	@ExcelCell(16)
	private String category;
	@ExcelCell(17)
	private String staffMembers;
	@ExcelCell(18)
	private String businessType;
	@ExcelCell(19)
	private boolean isFollower;
	@ExcelCell(20)
	private String admins; //List<String>
	@ExcelCell(21)
	private String desc;
	@ExcelCell(22)
	private String addressCords;
	@ExcelCell(23)
	private String rating;
	@ExcelCell(24)
	private String boardingFacility;
	@ExcelCell(25)
	private String placementAssistance;
	@ExcelCell(26)
	private String financialAssistance;
	@ExcelCell(27)
	private String infraFacility;
	@ExcelCell(28)
	private boolean bannerDefultImage;
	@ExcelCell(29)
	private String bannerDefaultPath;
	
	
	public String getOwner() {
		return owner;
	}
	public String getOwnershipType() {
		return ownershipType;
	}
	public String getOffers() {
		return offers;
	}
	public String getAddress() {
		return address;
	}
	public String getTurnOverCurrency() {
		return turnOverCurrency;
	}
	public String getFoundedOn() {
		return foundedOn;
	}
	public String getCreated() {
		return created;
	}
	public String getTurnOverValue() {
		return turnOverValue;
	}
	public String getProcessAuditStatus() {
		return processAuditStatus;
	}
	public String getType() {
		return type;
	}
	public String getTitle() {
		return title;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public String getPhone() {
		return phone;
	}
	public String getPageUrl() {
		return pageUrl;
	}
	public String getFinancialAuditStatus() {
		return financialAuditStatus;
	}
	public String getCategory() {
		return category;
	}
	public String getStaffMembers() {
		return staffMembers;
	}
	public String getBusinessType() {
		return businessType;
	}
	public boolean isFollower() {
		return isFollower;
	}
	public String getAdmins() {
		return admins;
	}
	public String getDesc() {
		return desc;
	}
	public String getAddressCords() {
		return addressCords;
	}
	public String getRating() {
		return rating;
	}
	public String getBoardingFacility() {
		return boardingFacility;
	}
	public String getPlacementAssistance() {
		return placementAssistance;
	}
	public String getFinancialAssistance() {
		return financialAssistance;
	}
	public String getInfraFacility() {
		return infraFacility;
	}
	public boolean isBannerDefultImage() {
		return bannerDefultImage;
	}
	public String getBannerDefaultPath() {
		return bannerDefaultPath;
	}
	@Override
	public String toString() {
		return "CompanyExcelModel [owner=" + owner + ", ownershipType=" + ownershipType + ", offers=" + offers
				+ ", address=" + address + ", turnOverCurrency=" + turnOverCurrency + ", foundedOn=" + foundedOn
				+ ", created=" + created + ", turnOverValue=" + turnOverValue + ", processAuditStatus="
				+ processAuditStatus + ", type=" + type + ", title=" + title + ", isDeleted=" + isDeleted
				+ ", websiteUrl=" + websiteUrl + ", phone=" + phone + ", pageUrl=" + pageUrl + ", financialAuditStatus="
				+ financialAuditStatus + ", category=" + category + ", staffingMembers=" + staffMembers
				+ ", businessType=" + businessType + ", isFollower=" + isFollower + ", admins=" + admins + ", desc="
				+ desc + ", addressCords=" + addressCords + ", rating=" + rating + ", boardingFacility="
				+ boardingFacility + ", placementAssistance=" + placementAssistance + ", financialAssistance="
				+ financialAssistance + ", infraFacility=" + infraFacility + ", bannerDefultImage=" + bannerDefultImage
				+ ", bannerDefaultPath=" + bannerDefaultPath + "]";
	}
	
	
	
	
}
