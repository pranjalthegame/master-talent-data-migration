package com.master.talent.api.entity.mapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.master.talent.api.dao.UserDao;
import com.master.talent.api.entity.CompanyEntity;
import com.master.talent.api.entity.ReviewEntity;
import com.master.talent.api.entity.UserPreSignUpEntity;
import com.master.talent.api.entity.repo.ProfielViewEntityRepo;
import com.master.talent.api.models.SearchUser;
import com.master.talent.api.utils.EmailUtility;
import com.master.talent.api.utils.IndexUtility;

@Component
public class UserDaoMapper {

	private final Logger logger = LoggerFactory.getLogger(UserDao.class);
	
	@Autowired
	private ProfielViewEntityRepo profielViewEntityRepo;
	
	@Autowired
	private IndexUtility indexUtility;
	
	@Autowired
	private EmailUtility emailUtility;
	
	public UserPreSignUpEntity mapper(UserPreSignUpEntity dbObj, UserPreSignUpEntity userprovidedObj) {
		
		logger.debug("userprovidedObj", userprovidedObj.toString());
	
		if(userprovidedObj.getBreif()!=null){
			dbObj.setBreif(userprovidedObj.getBreif());
		}
		if(userprovidedObj.getDob()!=null){
			dbObj.setDob(userprovidedObj.getDob());
		}
		if(userprovidedObj.getEmailAddress()!=null){
			dbObj.setEmailAddress(userprovidedObj.getEmailAddress());
		}
		if(userprovidedObj.getFname()!=null){
			dbObj.setFname(userprovidedObj.getFname());
			dbObj.setFullname(userprovidedObj.getFname()+ " " + dbObj.getLname());
		}
		if(userprovidedObj.getGender()!=null){
			dbObj.setGender(userprovidedObj.getGender());
		}
		if(userprovidedObj.getLname()!=null){
			dbObj.setLname(userprovidedObj.getLname());
			dbObj.setFullname(dbObj.getFname()+ " " + userprovidedObj.getLname());
		}
		if(userprovidedObj.getRegisterLevel()!=null){
			dbObj.setRegisterLevel(userprovidedObj.getRegisterLevel());
		}
		if(userprovidedObj.isActive()){
			dbObj.setActive(true);
		}
		if(userprovidedObj.getRelationship()!=null){
			dbObj.setRelationship(userprovidedObj.getRelationship());
		}
		if(userprovidedObj.getOtp()!=null){
			dbObj.setOtp(userprovidedObj.getOtp());
		}
		if(userprovidedObj.getUserType()!=null){
			if(dbObj.getUserType() == null){
				dbObj.setUserType(new ArrayList<String>());
			}
			if(!userprovidedObj.getUserType().isEmpty()){
				for(String userType: userprovidedObj.getUserType()){					
					if(userType.equalsIgnoreCase("pendingTrainer")){
						dbObj.getUserType().add("pendingTrainer");
					} else if(userType.equalsIgnoreCase("rejectTrainer")){
						dbObj.getUserType().add(userType);
						Iterator<String> userIte = dbObj.getUserType().iterator();
						while (userIte.hasNext()) {
							String user = userIte.next();
						    if (user.equals("pendingTrainer")) {
						    	userIte.remove();
						    }
						}												
						emailUtility.trainerNotification(dbObj.getEmailAddress(), "rejectTrainer");
					} else if(userType.equalsIgnoreCase("trainer")){
						dbObj.getUserType().add(userType);
						Iterator<String> userIte = dbObj.getUserType().iterator();
						while (userIte.hasNext()) {
							String user = userIte.next();
						    if (user.equals("pendingTrainer")) {
						    	userIte.remove();
						    }
						}
						emailUtility.trainerNotification(dbObj.getEmailAddress(), "trainer");
					}
				}
			}			
		}
		if(userprovidedObj.getEmployment() != null){
			if(!userprovidedObj.getEmployment().isEmpty()){
				dbObj.setEmployment(userprovidedObj.getEmployment());
			}
		}
		if(userprovidedObj.getPhone() != null){
			if(!userprovidedObj.getPhone().isEmpty()){
				dbObj.setPhone(userprovidedObj.getPhone());
			}
		}
		if(userprovidedObj.getProInterest() != null){			
			if(!userprovidedObj.getProInterest().isEmpty()){
				dbObj.setProInterest(userprovidedObj.getProInterest());
			}			
		}
		if(userprovidedObj.getBanner() != null){			
			dbObj.setBanner(userprovidedObj.getBanner());
		}
		if(userprovidedObj.getPassword() != null){			
			dbObj.setPassword(userprovidedObj.getPassword());
		}
		return dbObj;
	}

	@SuppressWarnings("unchecked")
	public SearchUser searchMapper(SearchUser searchUser, SolrDocument doc) {	
		List<String> list = null;
		if(doc.getFieldValue("activeTime") !=null)
		searchUser.setActiveTime(doc.getFieldValue("activeTime").toString());
		if(doc.getFieldValue("brief") !=null)
		searchUser.setBrief(doc.getFieldValue("brief").toString());
		if(doc.getFieldValue("created") !=null)
		searchUser.setCreated(doc.getFieldValue("created").toString());
		if(doc.getFieldValue("fullname") !=null)
		searchUser.setDisplayName(doc.getFieldValue("fullname").toString());
		if(doc.getFieldValue("phone") !=null)
		searchUser.setPhone(doc.getFieldValue("phone").toString());
		if(doc.getFieldValue("id") !=null)
		searchUser.setUsername(doc.getFieldValue("id").toString());
		if(doc.getFieldValue("workExp") !=null)
		searchUser.setWorkExp(doc.getFieldValue("workExp").toString());
		if(doc.getFieldValue("workStatus") !=null)
		searchUser.setWorkStatus(doc.getFieldValue("workStatus").toString());
		if(doc.getFieldValue("location") !=null)
		searchUser.setLocation(doc.getFieldValue("location").toString());
		if(doc.getFieldValue("country") !=null)
			searchUser.setLocation(doc.getFieldValue("country").toString());
		if(doc.getFieldValue("workType") !=null)
		searchUser.setWorkType(doc.getFieldValue("workType").toString());
		if(doc.getFieldValue("followersList") !=null)
			list = (List<String>) doc.getFieldValue("followersList");
		if(doc.getFieldValue("rating") !=null){
			String string = doc.getFieldValue("rating").toString();
			String[] temp =string.split("/");
			searchUser.setRating(temp[0]);
		}
		
		if(list==null)
			searchUser.setFollowCount(0);
		else
			searchUser.setFollowCount(list.size());
		
		Long profileCount = profielViewEntityRepo.countByViewee(searchUser.getUsername());
		if(profileCount==null)
			searchUser.setProfileViews(new Long(0));
		else
			searchUser.setProfileViews(profileCount);
		
		return searchUser;
	}

	public CompanyEntity companyMapper(CompanyEntity dbObj, CompanyEntity obj) {		
		if(obj.getTitle() !=null){
			dbObj.setTitle(obj.getTitle());
		}
		if(obj.getAdmins() !=null){
			dbObj.setAdmins(obj.getAdmins());
		}
		if(obj.getPageUrl() !=null){
			dbObj.setPageUrl(obj.getPageUrl());
		}
		if(obj.getBanner() != null){			
			dbObj.setBanner(obj.getBanner());
		}
		if(obj.getDesc() !=null){
			dbObj.setDesc(obj.getDesc());
		}
		if(obj.getOffers() !=null){
			dbObj.setOffers(obj.getOffers());
		}
		if(obj.getAddress() !=null){
			dbObj.setAddress(obj.getAddress());
		}
		if(obj.getWebsiteUrl() !=null){
			dbObj.setWebsiteUrl(obj.getWebsiteUrl());
		}
		if(obj.getAddressCords() !=null){
			dbObj.setAddressCords(obj.getAddressCords());
		}
		if(obj.getBusinessType() !=null){
			dbObj.setBusinessType(obj.getBusinessType());
		}
		if(obj.getCategory() !=null){
			dbObj.setCategory(obj.getCategory());
		}
		if(obj.getFinancialAuditStatus() !=null){
			dbObj.setFinancialAuditStatus(obj.getFinancialAuditStatus());
		}
		if(obj.getFoundedOn() !=null){
			dbObj.setFoundedOn(obj.getFoundedOn());
		}
		if(obj.getOwnershipType() !=null){
			dbObj.setOwnershipType(obj.getOwnershipType());
		}
		if(obj.getPhone() !=null){
			dbObj.setPhone(obj.getPhone());
		}
		if(obj.getProcessAuditStatus() !=null){
			dbObj.setProcessAuditStatus(obj.getProcessAuditStatus());
		}
		if(obj.getStaffMembers() !=null){
			dbObj.setStaffMembers(obj.getStaffMembers());
		}
		if(obj.getTurnOverCurrency() !=null){
			dbObj.setTurnOverCurrency(obj.getTurnOverCurrency());
		}
		if(obj.getTurnOverValue() !=null){
			dbObj.setTurnOverValue(obj.getTurnOverValue());
		}
		
		return dbObj;
	}

	public ReviewEntity updateReview(ReviewEntity dbObj, ReviewEntity obj) {
		/*if(obj.getReviewMessage() !=null){
			dbObj.setReviewMessage(obj.getReviewMessage());
		}*/
		if(obj.isPublish()){
			dbObj.setPublish(obj.isPublish());			
			//update review in indx
			if (dbObj.getType().equalsIgnoreCase("user")){
				String rating = indexUtility.getUserReview(dbObj.getReviewId());
				rating = processRating(rating, dbObj.getReviewRating());
				indexUtility.updateUserRatingSolr(dbObj.getReviewId(), rating);
			} else if (dbObj.getType().equalsIgnoreCase("company")){
				String rating = indexUtility.getCompanyReview(dbObj.getReviewId());
				rating = processRating(rating, dbObj.getReviewRating());
				indexUtility.updateCompanyRatingSolr(dbObj.getReviewId(), rating);
			}
		}
		return dbObj;
	}

	private String processRating(String rating, String currentRating) {
		String[] string = rating.split("/");
		
		double rate = Double.parseDouble(string[0]);
		Integer count = Integer.parseInt(string[1]);
		rate = rate+Double.parseDouble(currentRating);
		count++;
		
		rate = rate/count;		
		return rate+"/"+count;		
	}

}
