package com.master.talent.api.entity.repo;


import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.LikeEntity;


@Repository
public interface LikeEntityRepo extends CouchbaseRepository<LikeEntity, String>{
	
	/*@View(designDocument = "itemEntity", viewName = "checkLike")
	List<LikeEntity> findByItemIdAndUsername(Query query);*/
	//List<LikeEntity> findByItemId(String itemId);
}

