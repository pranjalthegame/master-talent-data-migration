/**
 * Created by Apache CXF WadlToJava code generator
**/
package com.master.talent.api.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Path("/")
public interface SearchService {
    
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/user/{searchTerm}/{start}/{end}")
	Response searchAutoComplete(@Context HttpHeaders headers, @PathParam(value = "searchTerm") String searchTerm, @PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/search/company/{searchTerm}/{start}/{end}")
	Response searchCompany(@Context HttpHeaders headers, @PathParam(value = "searchTerm") String searchTerm, @PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/searchItems/page/{type}/{pageId}/{start}/{end}")
	Response searchItemsCompany(@Context HttpHeaders headers, @PathParam(value = "type") String type, @PathParam(value = "pageId") String pageId, @PathParam("start") int start, @PathParam("end") int end);	

	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/searchItems/{type}/{searchTerm}/{start}/{end}")
	Response searchItems(@Context HttpHeaders headers, @PathParam("type") String type, @PathParam("searchTerm") String searchTerm,
			@PathParam("start") int start, @PathParam("end") int end, @QueryParam(value = "subCategory") boolean subCategory);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/searchItems/category/tutorial/{category}/{start}/{end}")
	Response searchTutorialItems(@Context HttpHeaders headers, @PathParam("category") String category, @PathParam("start") int start, @PathParam("end") int end);	

	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userList/{username}/{type}/{start}/{end}")
	Response getItemsByuser(@Context HttpHeaders headers, @PathParam("username") String username, @PathParam("type") String type, @PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/exploreList/{type}/{start}/{end}")
	Response getExploreItems(@Context HttpHeaders headers, @PathParam("type") String type, @PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userDashboardList/{start}/{end}")
	Response getDashboardItemsByuser(@Context HttpHeaders headers, @PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userShortListList/{start}/{end}")
	Response getShortlistItems(@Context HttpHeaders headers,@PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userPendingList/{start}/{end}")
	Response getUserPendingList(@Context HttpHeaders headers,@PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userRecommenedList/{start}/{end}")
	Response getUserRecommnedList(@Context HttpHeaders headers,@PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userAppliedList/{start}/{end}")
	Response getAppliedItems(@Context HttpHeaders headers,@PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/recruiterJobs/{start}/{end}")
	Response gerRecruiterJobListings(@Context HttpHeaders headers,@PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userLikeList/{start}/{end}")
	Response getLikeItems(@Context HttpHeaders headers,@PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/getIndiviualUser/{username}")
	Response getUser(@Context HttpHeaders headers, @PathParam("username") String username);
	
}