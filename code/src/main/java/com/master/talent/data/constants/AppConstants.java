/**
 * 
 */
package com.master.talent.data.constants;

/**
 * @author pranjal.sinha
 *
 */
public class AppConstants {
	
	public static final boolean IS_PARALLEL_PROCESSING_ENABLED = true;
	
	public static final String PROTOCOL = "http://";

	public static final String HOSTNAME = "localhost";
	public static final String FILE_RELATIVE_PATH = "../feed files/";
	
	public static final String TYPE_USER = "user";
	public static final String TYPE_DISCUSSION = "discussion";
	public static final String TYPE_COMPANY = "company";
	public static final String TYPE_COMMENT = "comment";
	
	public static final String USER_FEED_FILE = "usertemplate.xlsx";
	public static final String DISCUSSION_FEED_FILE = "discussiontemplate.xlsx";
	public static final String COMPANY_FEED_FILE = "companytemplate.xlsx";
	public static final String COMMENTS_FEED_FILE = "commentstemplate.xlsx";
	public static final String DISCUSSION_OUTPUT_FILE = "discussionoutput.xls";
	
	public static final String USER_SERVICE_PORT = "5100";
	public static final String DISCUSSION_SERVICE_PORT = "5500";
	public static final String COMPANY_SERVICE_PORT = "5100";
	public static final String COMMENT_SERVICE_PORT = "5500";
	
	public static final String USER_SERVICE_ENDPOINT = "/api/user/userPreSignup";
	public static final String DISCUSSION_SERVICE_ENDPOINT = "/api/item";
	public static final String COMPANY_SERVICE_ENDPOINT = "/api/user/company";
	public static final String COMMENT_SERVICE_ENDPOINT = "/api/item/comment";
}
