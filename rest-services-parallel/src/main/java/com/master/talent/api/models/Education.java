package com.master.talent.api.models;

public class Education {
	
	private String courseName;
	private String instituteName;
	private String location;
	private boolean completed;
	private String completedMonth;
	private String completedYear;
	
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getInstituteName() {
		return instituteName;
	}
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public boolean isCompleted() {
		return completed;
	}
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}	
	public String getCompletedMonth() {
		return completedMonth;
	}
	public void setCompletedMonth(String completedMonth) {
		this.completedMonth = completedMonth;
	}
	public String getCompletedYear() {
		return completedYear;
	}
	public void setCompletedYear(String completedYear) {
		this.completedYear = completedYear;
	}
	@Override
	public String toString() {
		return "Education [courseName=" + courseName + ", instituteName="
				+ instituteName + ", location=" + location + ", completed="
				+ completed + ", completedMonth=" + completedMonth + ", completedYear=" + completedYear + "]";
	}
}
