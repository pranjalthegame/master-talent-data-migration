package com.master.talent.api.entity.mapper;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.master.talent.api.dao.UserDao;
import com.master.talent.api.entity.ItemEntity;

@Component
public class ItemDaoMapper {

	private final Logger logger = LoggerFactory.getLogger(UserDao.class);
	
	public ItemEntity mapper(ItemEntity dbObj, ItemEntity userprovidedObj) {
		switch(userprovidedObj.getItemType().toUpperCase()){ 
	    case "article":
	    	 if (userprovidedObj.getArticle()!=null){
	    		 dbObj.setArticle(userprovidedObj.getArticle());
	    	 }
	    	break;
	    case "discussion":
	    	if (userprovidedObj.getDiscussion()!=null){
	    		 dbObj.setDiscussion(userprovidedObj.getDiscussion());
	    	 }
	    	break; 
	    case "media":
	    	if (userprovidedObj.getMedia()!=null){
	    		 dbObj.setMedia(userprovidedObj.getMedia());
	    	 }
	    	break; 
	    case "job":
	    	if (userprovidedObj.getJob()!=null){
	    		 dbObj.setJob(userprovidedObj.getJob());
	    	 }
	    	break; 
	    default:
	    	logger.debug("Invalid Payload::", userprovidedObj);
		}
		dbObj.setUpdated(DateTime.now());
		return dbObj;
	}

}
