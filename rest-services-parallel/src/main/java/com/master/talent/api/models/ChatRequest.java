package com.master.talent.api.models;

import java.util.List;

public class ChatRequest {
	
	private String chatId;
	private List<String> users;
	private String message;
	
	
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	public List<String> getUsers() {
		return users;
	}
	public void setUsers(List<String> users) {
		this.users = users;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "ChatRequest [chatId=" + chatId + ", users=" + users
				+ ", message=" + message + "]";
	}
}
