package com.master.talent.api.entity.repo;


import java.util.List;

import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.ReviewEntity;
import com.master.talent.api.entity.UserEarningEntity;


@Repository
public interface UserEarningEntityRepo extends CouchbaseRepository<UserEarningEntity, String>
{
	
	@View(designDocument = "earnings", viewName = "getNotRedemmedList")
	List<UserEarningEntity> getByUsername(String username);
	
	@View(designDocument = "earnings", viewName = "getAllList")
	List<UserEarningEntity> getById(String username);
}

