package com.master.talent.api.models;

public class Skill {
	
	private String skillLevel;
	private String skillName;
	
	public String getSkillLevel() {
		return skillLevel;
	}
	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	
	@Override
	public String toString() {
		return "Skill [skillLevel=" + skillLevel + ", skillName=" + skillName
				+ "]";
	}
}
