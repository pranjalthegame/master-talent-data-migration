import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;

import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.entity.UserPurchasedServicesEntity;
import com.master.talent.api.models.Purchases;
import com.master.talent.api.solr.entity.MasterTalentItem;

public class Test {

	
	private static HttpClient getHttpClient() {
	    CredentialsProvider provider = new BasicCredentialsProvider();
	    UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("admin", "Master@1234$");
	    provider.setCredentials(AuthScope.ANY, credentials);
	    return HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
	}
	
	private static String retriveRespectiveId(String requestUri){
		String[] localRequestUri = null;
		if(requestUri.contains("?")){
			localRequestUri = requestUri.split("\\?");
			localRequestUri = localRequestUri[0].split("/");
		} else {
			localRequestUri = requestUri.split("/");
		}
		return localRequestUri[localRequestUri.length-1];
	}
	
	public static SessionEntity processIndexesForServices(UserPurchasedServicesEntity obj, SessionEntity sess)  {
		try{
			SolrClient solrClient = new HttpSolrClient.Builder("http://67.219.144.110:8983/solr/").build();			
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", "ridder.ashok");	
			if(obj.getPurchased()!=null){
				if(!obj.getPurchased().isEmpty()){
					for(Purchases purchases: obj.getPurchased()){ 
						if(purchases.getServiceId().equals("SERVICE001")){
							doc.addField("priorityApplication", true);
							sess.setPriorityApplication(true);
						} else {
							doc.addField("priorityApplication", false);
							sess.setPriorityApplication(false);
						}
						if(purchases.getServiceId().equals("SERVICE002")){
							doc.addField("proListing", true);
							sess.setProListing(true);
						} else {
							doc.addField("proListing", false);
							sess.setProListing(false);
						}
						if(purchases.getServiceId().equals("SERVICE004")){
							doc.addField("jobListing", true);
							sess.setJobListing(true);
						} else {
							doc.addField("jobListing", false);
							sess.setJobListing(false);
						}
						if(purchases.getServiceId().equals("SERVICE005")){
							doc.addField("contactViewPack", true);
							sess.setContactViewPack(true);
						} else {
							doc.addField("contactViewPack", false);
							sess.setJobListing(false);
						}
					}				
				}
			}
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("set", ZonedDateTime.now(ZoneOffset.UTC).toString());
			doc.addField("activeTime", partialUpdate);
			
			UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials("admin", "Master@1234$");
			req.add(doc); 			
			req.setCommitWithin(1000); 
			req.process(solrClient, "mastertalentuser"); 
			return sess;
		} catch(Exception e){
			System.out.println("ERRRRRRRRRRRRRRRRROOOOOOOOOOOOORRRRRRRRRRRRRRRRRRRRRR");
			return sess;
		}		
	}
	
	public static List<MasterTalentItem> gerRecruiterJobListings(String username, int start, int end) throws SolrServerException, IOException {
		
		SolrClient solrClient = new HttpSolrClient.Builder("http://67.219.144.110:8983/solr/").build();	
		List<MasterTalentItem> itemList = new ArrayList<MasterTalentItem>();
		SolrQuery appliedQuery = new SolrQuery();
		appliedQuery.setQuery("itemType:job AND username:"+username);
		appliedQuery.setFilterQueries("proApplyList:['' TO *] OR applyList:['' TO *]");
		appliedQuery.setStart(start);
		appliedQuery.setRows(end);

		QueryRequest req = new QueryRequest(appliedQuery);
		req.setBasicAuthCredentials("admin", "Master@1234$");
		QueryResponse rsp = req.process(solrClient, "mastertalentitem"); 
		
		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				//local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				local.setPostedOn((Date) doc.getFieldValue("created"));
				
				List<String> appliedlist = new ArrayList<String>();
				appliedlist = (List<String>) doc.getFieldValue("applyList");
				
				appliedlist.addAll((List<String>) doc.getFieldValue("applyList"));
				
				 for (String user : appliedlist) {
					System.out.println(user);
				}
			}
		}
		return itemList;
	}
	
	
	public static void main(String[] args) throws SolrServerException, IOException {
		System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		
		//String resp = retriveRespectiveId("http://mastertalent.com/item/discussion/3b9c43a2-dccc-46ed-b672-df3cb57e674f");
		/*SessionEntity sess = new SessionEntity();
		UserPurchasedServicesEntity activeServiceList = new UserPurchasedServicesEntity();
		sess = processIndexesForServices(activeServiceList, sess);*/
		
		gerRecruiterJobListings("dagar.yk", 0 ,1);
		
		/*//SolrClient client = new HttpSolrClient.Builder("http://67.219.144.110:8983/solr/mastertalentuser").withHttpClient(getHttpClient()).build();
		
		SolrClient client = new HttpSolrClient.Builder("http://67.219.144.110:8983/solr").build();
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", "ridder.ashok");
		
		Map<String, String> userType = new HashMap<String, String>();
		userType.put("add", "trainer");
		//Map<String, String> removeUserType = new HashMap<String, String>();
		//removeUserType.put("remove", "pendingTrainer");
		doc.addField("userType", userType);
		//doc.addField("userType", removeUserType);
		
		
		Map<String, String> userType = new HashMap<String, String>();
		userType.put("set", "pendingTrainer");
		doc.addField("userType", userType);
		
		UpdateRequest req = new UpdateRequest(); 
		req.setBasicAuthCredentials("admin", "Master@1234$");
		req.add(doc); 
		req.setCommitWithin(1000); 
		req.process(client, "mastertalentuser"); 
		
		System.out.println("CCCCCCCCCCCCCCCCCCCCCCCCCC" +client);
		
		SolrQuery userQuery = new SolrQuery();
		userQuery.setQuery("id:*");
		
		QueryRequest req1 = new QueryRequest(userQuery);
		req1.setBasicAuthCredentials("admin", "Master@1234$");
		QueryResponse rsp = req1.process(client, "mastertalentuser"); 
		
		System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD" +rsp.getResults());*/
	}

}
