/**
 * 
 */
package com.master.talent.data.util;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * @author pranjal.sinha
 *
 */
public class JsonUtils {

	
	public static String toJson(Object object){
		String json = null;
		ObjectMapper mapper = new ObjectMapper();
		if (object !=null){
			try{
				json = mapper.writeValueAsString(object);
			}catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}
	@SuppressWarnings("unchecked")
	public static Object toObject(String json,@SuppressWarnings("rawtypes") Class name ){
		ObjectMapper mapper = new ObjectMapper();
		Object output=null;
		 
		try {
			output = (Object) mapper.readValue(json, name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return output;
	}
}
