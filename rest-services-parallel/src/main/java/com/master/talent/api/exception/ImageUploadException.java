package com.master.talent.api.exception;

import com.master.talent.api.exception.base.MasterTalentException;

public class ImageUploadException extends MasterTalentException {
   
	private static final long serialVersionUID = 1L;
	
	public ImageUploadException() {
        super();
    }
    public ImageUploadException(String message, Throwable cause) {
        super(message, cause);
    }
    public ImageUploadException(String message) {
        super(message);
    }
    public ImageUploadException(Throwable cause) {
        super(cause);
    }
	public ImageUploadException(String exceptionCode, String message, Throwable cause) {
		super(exceptionCode, message, cause);
	}
	public ImageUploadException(String exceptionCode, String message) {
		super(exceptionCode, message);
	}
    
    
}