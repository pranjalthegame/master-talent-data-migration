package com.master.talent.api.entity;

import org.joda.time.DateTime;
import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document(expiry=0)
public class ProfileViewEntity {
	
	@Id
	private String id;
	
	@Field
	private String viewer;	
	
	@Field
	private String viewee;
	
	@Field
    private DateTime viewTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getViewer() {
		return viewer;
	}

	public void setViewer(String viewer) {
		this.viewer = viewer;
	}

	public String getViewee() {
		return viewee;
	}

	public void setViewee(String viewee) {
		this.viewee = viewee;
	}

	public DateTime getViewTime() {
		return viewTime;
	}

	public void setViewTime(DateTime viewTime) {
		this.viewTime = viewTime;
	}

	@Override
	public String toString() {
		return "ProfileViewEntity [id=" + id + ", viewer=" + viewer
				+ ", viewee=" + viewee + ", viewTime=" + viewTime + "]";
	}

}
