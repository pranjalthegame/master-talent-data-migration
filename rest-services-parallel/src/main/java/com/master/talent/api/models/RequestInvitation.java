package com.master.talent.api.models;

import java.util.List;


public class RequestInvitation {	
	
	private List<String> inviteEmailId;
	private String username;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getInviteEmailId() {
		return inviteEmailId;
	}

	public void setInviteEmailId(List<String> inviteEmailId) {
		this.inviteEmailId = inviteEmailId;
	}

	@Override
	public String toString() {
		return "RequestInvitation [inviteEmailId=" + inviteEmailId
				+ ", username=" + username + "]";
	}

}
