package com.master.talent.api.models;

public class Count {

	private int started;
	private int following;
	private int replied;
	private int applied;
	private int shorlisted;
	
	public int getStarted() {
		return started;
	}
	public void setStarted(int started) {
		this.started = started;
	}
	public int getFollowing() {
		return following;
	}
	public void setFollowing(int following) {
		this.following = following;
	}
	public int getReplied() {
		return replied;
	}
	public void setReplied(int replied) {
		this.replied = replied;
	}
	public int getApplied() {
		return applied;
	}
	public void setApplied(int applied) {
		this.applied = applied;
	}
	public int getShorlisted() {
		return shorlisted;
	}
	public void setShorlisted(int shorlisted) {
		this.shorlisted = shorlisted;
	}
}
