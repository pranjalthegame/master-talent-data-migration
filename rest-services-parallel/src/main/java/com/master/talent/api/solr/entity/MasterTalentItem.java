package com.master.talent.api.solr.entity;

import java.util.Date;
import java.util.List;

import com.master.talent.api.models.SearchUser;

public class MasterTalentItem {

    private String id;

    private String username;
    
    private String displayName;
    
    private String itemType;
    
    private String itemTitle;
    
    private Date postedOn;
    
    private List<SearchUser> appliedUserList;    
    
    public Date getPostedOn() {
		return postedOn;
	}

	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	public List<SearchUser> getAppliedUserList() {
		return appliedUserList;
	}

	public void setAppliedUserList(List<SearchUser> appliedUserList) {
		this.appliedUserList = appliedUserList;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemTitle() {
		return itemTitle;
	}

	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}

	@Override
	public String toString() {
		return "MasterTalentItem [id=" + id + ", username=" + username
				+ ", itemType=" + itemType + ", itemTitle=" + itemTitle + "]";
	}
    
}
