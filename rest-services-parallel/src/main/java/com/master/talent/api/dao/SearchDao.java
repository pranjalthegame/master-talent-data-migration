package com.master.talent.api.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.master.talent.api.entity.CompanyEntity;
import com.master.talent.api.entity.mapper.UserDaoMapper;
import com.master.talent.api.entity.repo.CompanyEntityRepo;
import com.master.talent.api.exception.SearchException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.Count;
import com.master.talent.api.models.SearchUser;
import com.master.talent.api.models.User;
import com.master.talent.api.solr.entity.MasterTalentItem;
import com.master.talent.api.solr.entity.MasterTalentUser;
import com.master.talent.api.utils.SpringPropertiesUtil;

@Component
public class SearchDao {
	
	private final Logger logger = LoggerFactory.getLogger(SearchDao.class);
	
	SolrClient solrClient = new HttpSolrClient.Builder(SpringPropertiesUtil.getProperty("solr.server.url")).build();
	
	@Autowired
	private UserDaoMapper userDaoMapper;
	
	@Autowired
	private CompanyEntityRepo companyEntityRepo;
	
	public List<MasterTalentItem> listItemsByUsername(String username, String type, int start, int end) throws SolrServerException, IOException{
		List<MasterTalentItem> userItemList = new ArrayList<MasterTalentItem>();
		SolrQuery userQuery = new SolrQuery();
		userQuery.setQuery("username:"+username);
		if(!type.equalsIgnoreCase("all")){
			userQuery.setFilterQueries("itemType:"+type.toLowerCase());
		}
		userQuery.setStart(start);
		userQuery.setRows(end);
		userQuery.addSort("created", ORDER.desc);
		
		QueryRequest req = new QueryRequest(userQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
		
		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				userItemList.add(local);
			}
		}
		return userItemList;
		
    }
	
	public List<MasterTalentItem> listDashboardItemsByUsername(String username, int start, int end) throws SolrServerException, IOException{
		List<MasterTalentItem> userItemList = new ArrayList<MasterTalentItem>();
		SolrQuery userQuery = new SolrQuery();
		userQuery.setQuery("viewers:"+username);
		userQuery.setStart(start);
		userQuery.setRows(end);
		userQuery.addSort("updated", ORDER.desc);		

		QueryRequest req = new QueryRequest(userQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
				
		if (!rsp.getResults().isEmpty()){			
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				userItemList.add(local);
			}
		}

		return userItemList;
    }
	
	public String getDisplayName(String username) throws SolrServerException, IOException{
		SolrQuery userQuery = new SolrQuery();
		userQuery.setQuery("id:"+username);
		
		QueryRequest req = new QueryRequest(userQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
				
		if(rsp.getResults()!=null){
			return rsp.getResults().get(0).getFieldValue("fullname").toString();
		} else {
			logger.debug("username not found ::",username);
			return "";
		}
    }
	
	public String getPageName(String pageId){
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+pageId);			

			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
			
			if(rsp.getResults()!=null){
				return rsp.getResults().get(0).getFieldValue("pageTitle").toString();
			} else {
				return "";
			}
		} catch(Exception e){
			return "";
		}
    }
	
	public String getPageUrl(String pageId){
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+pageId);			

			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
			
			if(rsp.getResults()!=null){
				return rsp.getResults().get(0).getFieldValue("pageUrl").toString();
			} else {
				return "";
			}
		} catch(Exception e){
			return "";
		}
    }
	
	public boolean verifyFriendlyId(String friendlyId){
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("pageUrl:"+friendlyId);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
						
			if(rsp.getResults()!=null){
				if(!rsp.getResults().isEmpty()){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch(Exception e){
			logger.debug("Exception  verify friendly URL::",friendlyId);
			return true;
		}
    }
	
	public CompanyEntity getCompanyFriendlyId(String friendlyId){
		CompanyEntity companyEntity = new CompanyEntity();
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("pageUrl:"+friendlyId);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 

			if(rsp.getResults()!=null){
				if(rsp.getResults().get(0)!=null){
					SolrDocument doc = rsp.getResults().get(0);				
					companyEntity= companyEntityRepo.findOne(doc.getFieldValue("id").toString());
					return companyEntity;
				} else {
					return null;
				}
			}
			return null;
		} catch(Exception e){
			logger.debug("Exception  verify friendly URL::",friendlyId);
			return null;
		}
    }
	
	public List<MasterTalentUser> searchRecommendation(String[] searchTerm, int start, int end) throws SolrServerException, IOException{
		List<MasterTalentUser> user = new ArrayList<MasterTalentUser>();
		SolrQuery userQuery = new SolrQuery();
		
		if(searchTerm.length == 1) {		
			userQuery.setQuery("fullname:*"+searchTerm[0]+"*^2" + "OR proListing:true^4");		
		}else {
			String query ="(";
			int indx =1;
			for (String search : searchTerm) {
				if(indx ==1){
					query=query+"fullname:*"+search+"*^2";
				}else{
					query=query+"OR fullname:*"+search+"*^2";
				}
				indx++;
			}
			query=query+"OR proListing:true^4)";
			userQuery.setQuery(query);
		}
		userQuery.setStart(start);
		userQuery.setRows(end);
		
		QueryRequest req = new QueryRequest(userQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
				
		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentUser localUser = new MasterTalentUser(doc.getFieldValue("id").toString()
						, doc.getFieldValue("fullname").toString());
				user.add(localUser);
			}
		}

		return user;
    }
	
	public List<CompanyEntity> searchCompany(String[] searchTerm, int start, int end) throws SolrServerException, IOException{
		//ashok
		List<CompanyEntity> companyListEntity = new ArrayList<CompanyEntity>();
		SolrQuery userQuery = new SolrQuery();
		
			String query ="(";
			int indx =1;
			for (String search : searchTerm) {
				if(indx ==1){
					query=query+"pageTitle:*"+search+"* OR " + "offers:*" +search+"* OR " + "pageUrl:*" +search+"* ";
				}else{
					query=query+"OR pageTitle:*"+search+"* OR " + "offers:*" +search+"* OR " + "pageUrl:*" +search+"* ";
				}
				indx++;
			}
			query=query+")";
			userQuery.setQuery(query);
		
		userQuery.setStart(start);
		userQuery.setRows(end);
		
		QueryRequest req = new QueryRequest(userQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
		
		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				
				CompanyEntity companyEntity = new CompanyEntity();
				companyEntity.setPageId(doc.getFieldValue("id").toString());
				companyEntity.setDesc(doc.getFieldValue("desc").toString());
				companyEntity.setTitle(doc.getFieldValue("pageTitle").toString());
				companyEntity.setPageUrl(doc.getFieldValue("pageUrl").toString());
				
				companyListEntity.add(companyEntity);
			}
		}

		return companyListEntity;
    }
	
	public List<MasterTalentItem> searchItemsCompany(String pageId, String type, int start, int end) throws SolrServerException, IOException{
		List<MasterTalentItem> userItemList = new ArrayList<MasterTalentItem>();
		SolrQuery userQuery = new SolrQuery();
		userQuery.setQuery("pageId:"+pageId);
		if(!type.toLowerCase().equalsIgnoreCase("all")){
			userQuery.setFilterQueries("itemType:"+type.toLowerCase() +"OR proJob:true^2");
		}
		userQuery.setStart(start);
		userQuery.setRows(end);
		userQuery.addSort("updated", ORDER.desc);
		
		QueryRequest req = new QueryRequest(userQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
		
		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				userItemList.add(local);
			}
		}
		return userItemList;
		
    }

	public List<User> getAllFriendsForUser(String username, int start, int end) {
		List<User> response = new ArrayList<User>();
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+username);
			userQuery.setStart(start);
			userQuery.setRows(end);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp != null ){
			if(!rsp.getResults().isEmpty())
				for (SolrDocument doc : rsp.getResults()) {
					@SuppressWarnings("unchecked")
					List<String> list =  (List<String>) doc.getFieldValue("friendsList");
					if(list!=null){
						if(!list.isEmpty()){
							for (String string : list) {
								User user = new User();
								user.setId(string);
								response.add(user);
							}
						}
					}				
				}	
			}		
		} catch(Exception e){
			logger.debug("getAllFriendsForUser", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_GET_USER_FREINDS_FAIL,e.getMessage(),e);
		}
		return response;
		
	}
	
	public List<User> getAllFollowersForUser(String username, int start, int end) {
		List<User> response = new ArrayList<User>();
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+username);
			userQuery.setStart(start);
			userQuery.setRows(end);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			for (SolrDocument doc : rsp.getResults()) {
				@SuppressWarnings("unchecked")
				List<String> list =  (List<String>) doc.getFieldValue("followersList");
				if(list!=null){
					if(!list.isEmpty()){
						for (String string : list) {
							User user = new User();
							user.setId(string);
							response.add(user);
						}
					}
				}				
			}
		} catch(Exception e){
			logger.debug("getAllFollowersForUser", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_GET_USER_FOLLOWERS_FAIL,e.getMessage(),e);
		}
		return response;
		
	}
	
	public List<User> getAllFollowersForCompany(String compId, int start, int end) {
		List<User> response = new ArrayList<User>();
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+compId);
			userQuery.setStart(start);
			userQuery.setRows(end);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
			
			for (SolrDocument doc : rsp.getResults()) {
				@SuppressWarnings("unchecked")
				List<String> list =  (List<String>) doc.getFieldValue("followersList");
				if(list!=null){
					if(!list.isEmpty()){
						for (String string : list) {
							User user = new User();
							user.setId(string);
							response.add(user);
						}
					}
				}				
			}
		} catch(Exception e){
			logger.debug("getAllFollowersForCompany", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_GET_USER_FOLLOWERS_FAIL,e.getMessage(),e);
		}
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public int getAllFollowersCountForUser(String username) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp !=null){
				List<String> temp = (List<String>) rsp.getResults().get(0).getFieldValue("followersList");
				if(temp !=null){
					return temp.size();
				} else {
					return 0;
				}
			} else {
				return 0;
			}
		} catch(Exception e){
			logger.debug("getAllFollowersForUser", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_GET_USER_FOLLOWERS_FAIL,e.getMessage(),e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public int getAllFriendsCountForUser(String username) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp !=null){
				List<String> temp = (List<String>) rsp.getResults().get(0).getFieldValue("friendsList");
				if(temp !=null){
					return temp.size();
				} else {
					return 0;
				}
			} else{
				return 0;
			}
		} catch(Exception e){
			logger.debug("getAllFriendsCountForUser", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_GET_USER_FOLLOWERS_FAIL,e.getMessage(),e);
		}
	}

	public boolean verifyAFirend(String loggedInUser, String username) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+loggedInUser);
			userQuery.setFilterQueries("friendsList:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(!rsp.getResults().isEmpty()){
				return true;
			} else {
				return false;
			}
		} catch(Exception e){
			logger.debug("verifyAFriend", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_VERIFY_FRIEND_FAIL,e.getMessage(),e);
		}
	}
	
	public boolean verifyAPendingFirend(String loggedInUser, String username) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+loggedInUser);
			userQuery.setFilterQueries("pendingFirendsList:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp !=null){
				if(!rsp.getResults().isEmpty()){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch(Exception e){
			logger.debug("verifyAFriend", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_VERIFY_FRIEND_FAIL,e.getMessage(),e);
		}
	}
	
	public boolean verifyARequestedPendingFirend(String loggedInUser, String username) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+loggedInUser);
			userQuery.setFilterQueries("requestedPendingFirendsList:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp !=null){
				if(!rsp.getResults().isEmpty()){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch(Exception e){
			logger.debug("verifyAFriend", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_VERIFY_FRIEND_FAIL,e.getMessage(),e);
		}
	}
	
	public boolean verifyAFollower(String loggedInUser, String username) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+loggedInUser);
			userQuery.setFilterQueries("followersList:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp != null){
				if(rsp.getResults().isEmpty()){
					return false;
				} else {
					return true;
				}
			}
			return false;	
		} catch(Exception e){
			logger.debug("verifyAFollower", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_VERIFY_FOLLOWER_FAIL,e.getMessage(),e);
		}
	}
	
	public boolean verifyACompanyFollower(String compId, String username) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+compId);
			userQuery.setFilterQueries("followersList:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
			
			if(rsp != null){
				if(rsp.getResults().isEmpty()){
					return false;
				} else {
					return true;
				}
			}
			return false;	
		} catch(Exception e){
			logger.debug("verifyAFollower", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_VERIFY_FOLLOWER_FAIL,e.getMessage(),e);
		}
	}
	
	public boolean verifyLike(String username, String itemId) {
		try{
			SolrQuery likeQuery = new SolrQuery();
			likeQuery.setQuery("id:"+itemId);
			likeQuery.setFilterQueries("likeList:"+username);
			
			QueryRequest req = new QueryRequest(likeQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
			if(!rsp.getResults().isEmpty()){
				return true;
			} else {
				return false;
			}
		} catch(Exception e){
			logger.debug("verifyLike", e);
			throw new SearchException(e.getMessage(),e);
		}
	}
	
	public boolean verifyShorlist(String username, String itemId) {
		try{
			SolrQuery likeQuery = new SolrQuery();
			likeQuery.setQuery("id:"+itemId);
			likeQuery.setFilterQueries("shorlistList:"+username);

			QueryRequest req = new QueryRequest(likeQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
			if(!rsp.getResults().isEmpty()){
				return true;
			} else {
				return false;
			}
		} catch(Exception e){
			logger.debug("verifyShorlist", e);
			throw new SearchException(e.getMessage(),e);
		}
	}
	
	public boolean verifyApply(String username, String itemId) {
		try{
			SolrQuery likeQuery = new SolrQuery();
			likeQuery.setQuery("id:"+itemId);
			likeQuery.setFilterQueries("applyList:"+username);

			QueryRequest req = new QueryRequest(likeQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
			if(!rsp.getResults().isEmpty()){
				return true;
			} else {
				return false;
			}
		} catch(Exception e){
			logger.debug("verifyShorlist", e);
			throw new SearchException(e.getMessage(),e);
		}
	}

	public Object searchItems(String type, String[] searchTerm, int start, int end) throws SolrServerException, IOException {
		SolrQuery userQuery = new SolrQuery();
		if(!type.toLowerCase().equalsIgnoreCase("all")){
			userQuery.setQuery("itemType:"+type.toLowerCase());
		} 
		//userQuery.setQuery("itemType:"+type);
		String query ="(";
		int indx =1;
		for (String search : searchTerm) {
			if(indx ==1){
				query=query+"itemTitle:*"+search+"*^2 OR " + "itemDescription:*" +search+"*^2 ";
			}else{
				query=query+"OR itemTitle:*"+search+"*^2 OR " + "itemDescription:*" +search+"*^2 ";
			}
			indx++;
		}
		query=query+"OR proJob:true^8)";		
		userQuery.setQuery(query);
		userQuery.setStart(start);
		userQuery.setRows(end);

		QueryRequest req = new QueryRequest(userQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
		
		return rsp.getResults();
	}
	
	public Object searchTutorialItems(String category, int start, int end) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("itemSubCategory:"+category +"OR itemSubCategory:"+category.toLowerCase());	
			userQuery.addSort("updated", ORDER.desc);
			userQuery.setStart(start);
			userQuery.setRows(end);

			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
			return rsp.getResults();
		} catch(Exception e){
			return null;
		}		
	}

	public List<MasterTalentItem> searchShortlistItems(String username, int start, int end) throws SolrServerException, IOException {
		List<MasterTalentItem> itemList = new ArrayList<MasterTalentItem>();
		SolrQuery shortlistQuery = new SolrQuery();
		shortlistQuery.setQuery("id:*");
		shortlistQuery.setFilterQueries("shorlistList:"+username);
		shortlistQuery.setStart(start);
		shortlistQuery.setRows(end);
		shortlistQuery.addSort("updated", ORDER.desc);
		
		QueryRequest req = new QueryRequest(shortlistQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
		
		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				itemList.add(local);
			}
		}
		return itemList;
	}

	public List<MasterTalentItem> searchAppliedItems(String username, int start, int end) throws SolrServerException, IOException {
		List<MasterTalentItem> itemList = new ArrayList<MasterTalentItem>();
		SolrQuery appliedQuery = new SolrQuery();
		appliedQuery.setQuery("id:*");
		appliedQuery.setFilterQueries("proApplyList:"+username+"^10");
		appliedQuery.setFilterQueries("applyList:"+username);
		appliedQuery.setStart(start);
		appliedQuery.setRows(end);
		appliedQuery.addSort("updated", ORDER.desc);

		QueryRequest req = new QueryRequest(appliedQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
		
		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				itemList.add(local);
			}
		}
		return itemList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MasterTalentItem> gerRecruiterJobListings(String username, int start, int end) throws SolrServerException, IOException {
		List<MasterTalentItem> itemList = new ArrayList<MasterTalentItem>();
		SolrQuery appliedQuery = new SolrQuery();
		appliedQuery.setQuery("itemType:job AND username:"+username);
		appliedQuery.setFilterQueries("proApplyList:['' TO *] OR applyList:['' TO *]");
		appliedQuery.setStart(start);
		appliedQuery.setRows(end);

		QueryRequest req = new QueryRequest(appliedQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
		
		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				local.setPostedOn((Date) doc.getFieldValue("created"));
				List<String> appliedlist = new ArrayList<String>();
				List<SearchUser> appliedUserList = new ArrayList<SearchUser>();
				appliedlist = (List<String>) doc.getFieldValue("proApplyList");
				appliedlist.addAll((List<String>) doc.getFieldValue("applyList"));
				 for (String user : appliedlist) {
					 appliedUserList.add(getUser(user));					 
				}
			}
		}
		return itemList;
	}
	
	public List<MasterTalentItem> searchLikedItems(String username, int start, int end) throws SolrServerException, IOException {
		List<MasterTalentItem> itemList = new ArrayList<MasterTalentItem>();
		SolrQuery likeQuery = new SolrQuery();
		likeQuery.setQuery("id:*");
		likeQuery.setFilterQueries("likeList:"+username);
		likeQuery.setStart(start);
		likeQuery.setRows(end);
		likeQuery.addSort("updated", ORDER.desc);
		
		QueryRequest req = new QueryRequest(likeQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
		
		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				itemList.add(local);
			}
		}
		return itemList;
	}

	public List<MasterTalentItem> processArticle(List<String> searchTerm, String type, int start, int end) throws SolrServerException, IOException {
		List<MasterTalentItem> itemList = new ArrayList<MasterTalentItem>();
		SolrQuery itemQuery = new SolrQuery();
		itemQuery.setQuery("itemType:article");
		if(searchTerm != null){
			String query ="(";
			int indx =1;
			for (String search : searchTerm) {
				if(indx ==1){
					query=query+"itemTitle:*"+search+"* OR " + "itemDescription:*" +search+"* ";
				}else{

					query=query+"OR itemTitle:*"+search+"* OR " + "itemDescription:*" +search+"* ";
				}
				indx++;
			}
			query=query+")";		
			itemQuery.setFilterQueries(query);
		}		
		itemQuery.setStart(start);
		itemQuery.setRows(end);
		itemQuery.addSort("updated", ORDER.desc);

		QueryRequest req = new QueryRequest(itemQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 

		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				itemList.add(local);
			}
		}
		return itemList;
	}

	public Object processDiscussion(List<String> searchTerm, String type, int start, int end) throws SolrServerException, IOException {
		List<MasterTalentItem> itemList = new ArrayList<MasterTalentItem>();
		SolrQuery itemQuery = new SolrQuery();
		itemQuery.setQuery("itemType:discussion");
		if(searchTerm != null){
			String query ="(";
			int indx =1;
			for (String search : searchTerm) {
				if(indx ==1){
					query=query+"itemTitle:*"+search+"* OR " + "itemDescription:*" +search+"* ";
				}else{

					query=query+"OR itemTitle:*"+search+"* OR " + "itemDescription:*" +search+"* ";
				}
				indx++;
			}
			query=query+")";		
			itemQuery.setFilterQueries(query);
		}
		itemQuery.setStart(start);
		itemQuery.setRows(end);
		itemQuery.addSort("updated", ORDER.desc);
		
		QueryRequest req = new QueryRequest(itemQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 

		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				itemList.add(local);
			}
		}
		return itemList;
	}

	public Object processMedia(List<String> searchTerm, String type, int start, int end) throws SolrServerException, IOException {
		List<MasterTalentItem> itemList = new ArrayList<MasterTalentItem>();
		SolrQuery itemQuery = new SolrQuery();
		itemQuery.setQuery("itemType:media");
		if(searchTerm != null){
			String query ="(";
			int indx =1;
			for (String search : searchTerm) {
				if(indx ==1){
					query=query+"itemTitle:*"+search+"* OR " + "itemDescription:*" +search+"* ";
				}else{

					query=query+"OR itemTitle:*"+search+"* OR " + "itemDescription:*" +search+"* ";
				}
				indx++;
			}
			query=query+")";		
			itemQuery.setFilterQueries(query);
		}
		itemQuery.setStart(start);
		itemQuery.setRows(end);
		itemQuery.addSort("updated", ORDER.desc);

		QueryRequest req = new QueryRequest(itemQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 

		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				itemList.add(local);
			}
		}
		return itemList;
	}

	public Object processJob(List<String> searchTerm, String type, int start, int end) throws SolrServerException, IOException {
		List<MasterTalentItem> itemList = new ArrayList<MasterTalentItem>();
		SolrQuery itemQuery = new SolrQuery();
		itemQuery.setQuery("itemType:job");
		if(searchTerm != null){
			String query ="(";
			int indx =1;
			for (String search : searchTerm) {
				if(indx ==1){
					query=query+"itemTitle:*"+search+"*^2 OR " + "itemDescription:*" +search+"*^2 ";
				}else{

					query=query+"OR itemTitle:*"+search+"*^2 OR " + "itemDescription:*" +search+"*^2 ";
				}
				indx++;
			}
			query=query+"OR proJob:true^8)";		
			itemQuery.setFilterQueries(query);
		}
		itemQuery.setStart(start);
		itemQuery.setRows(end);
		itemQuery.addSort("updated", ORDER.desc);

		QueryRequest req = new QueryRequest(itemQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 

		if (!rsp.getResults().isEmpty()){
			for (SolrDocument doc : rsp.getResults()) {
				MasterTalentItem local = new MasterTalentItem();
				local.setId(doc.getFieldValue("id").toString());
				local.setDisplayName(getDisplayName(doc.getFieldValue("username").toString()));
				local.setUsername(doc.getFieldValue("username").toString());
				local.setItemType(doc.getFieldValue("itemType").toString());
				local.setItemTitle(doc.getFieldValue("itemTitle").toString());
				itemList.add(local);
			}
		}
		return itemList;
	}

	public SearchUser getUser(String username) throws SolrServerException, IOException {
		SolrQuery userQuery = new SolrQuery();
		userQuery.setQuery("id:"+username);
		
		QueryRequest req = new QueryRequest(userQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 

		SearchUser searchUser =null;
		if (!rsp.getResults().isEmpty()){
			searchUser = new SearchUser();
			SolrDocument doc = rsp.getResults().get(0);
			searchUser = userDaoMapper.searchMapper(searchUser, doc);
			searchUser.setFollowCount(getAllFollowersCountForUser(username));
			searchUser.setFriendCount(getAllFriendsCountForUser(username));
		}
		return searchUser;
	}

	@SuppressWarnings("unchecked")
	public List<User> getPendingUsersList(String username, int start, int end) {
		List<User> response = new ArrayList<User>();
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+username);
			userQuery.setStart(start);
			userQuery.setRows(end);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp!=null){
				for (SolrDocument doc : rsp.getResults()) {
					List<String> list =  (List<String>) doc.getFieldValue("pendingFirendsList");
					if(list != null){
						for (String string : list) {
							User user = new User();
							user.setId(string);
							user.setDisplayName(getDisplayName(string));	
							user.setDisplayName(getDisplayName(string));	
							user.setPending(true);
							response.add(user);
						}	
					}else {
						return null;
					}			
				}
			} else {
				return null;
			}			
		} catch(Exception e){
			logger.debug("getPendingUsersList", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_GET_USER_FREINDS_FAIL,e.getMessage(),e);
		}
		return response;	
	}
	
	public List<User> getUserRecommenedList(String username, int start, int end) {
		List<User> response = new ArrayList<User>();
		//get recommdations by firends 
		response = getRecommnedByFriends(username, response);
		if(response !=null && response.size() > end){
			return response.subList(start, end);
		}
		//get recommdations by location 
		if(response!=null){
			if(response.size()<end){
				String location = getLocationofUser(username);
				response = getRecommnedByLocation(username, location, response, end);			
			}
		}	
		if(response !=null && response.size() > end){
			return response.subList(start, end);
		}
		//get recommdations by all
		if(response!=null){
			if(response.size()<end){
				response = getAllUsers(username, response, end);
			}
		}		
		if(response !=null && response.size() > end){
			return response.subList(start, end);
		}
		return response;
	}

	private List<User> getAllUsers(String username, List<User> response, int end) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("-id:"+username);
			userQuery.setFilterQueries("-friendsList:"+username+" AND -pendingFirendsList:"+username+" AND -followersList:"+username);
			userQuery.setStart(0);
			userQuery.setRows(end);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp.getResults() != null){
				for (SolrDocument doc : rsp.getResults()) {
					User user = new User();				
					user.setId(doc.getFieldValue("id").toString());
					user.setDisplayName(doc.getFieldValue("fullname").toString());
					response.add(user);						
				}				
			}
		} catch(Exception e){
			return response;
		}
		return response;
	}

	private String getLocationofUser(String username) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp.getResults() != null){
				SolrDocument solr = rsp.getResults().get(0);
				return solr.getFieldValue("location").toString();
			} else {
				return "";
			}
		} catch(Exception e){
			return "";
		}
		
	}

	private List<User> getRecommnedByLocation(String username, String location,  List<User> response, int end) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("location:"+location + " AND -id:"+username);
			userQuery.setFilterQueries("-friendsList:"+username+" AND -pendingFirendsList:"+username+" AND -followersList:"+username);
			userQuery.setStart(0);
			userQuery.setRows(end);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp.getResults() != null){
				for (SolrDocument doc : rsp.getResults()) {
					User user = new User();				
					user.setId(doc.getFieldValue("id").toString());
					user.setDisplayName(doc.getFieldValue("fullname").toString());
					response.add(user);						
				}				
			}
		} catch(Exception e){
			return response;
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	private List<User> getRecommnedByFriends(String username, List<User> response) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp.getResults() != null){				
				SolrDocument doc = rsp.getResults().get(0);
				List<String> list =  (List<String>) doc.getFieldValue("friendsList");
				for (String string : list) {
					SolrQuery internalQuery = new SolrQuery();
					internalQuery.setQuery("id:"+string);
					internalQuery.setFilterQueries("-id:"+username);
					internalQuery.setFilterQueries("-friendsList:"+username+" AND -pendingFirendsList:"+username+" AND -followersList:"+username);
					
					QueryRequest req1 = new QueryRequest(internalQuery);
					req1.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
					QueryResponse internalList = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
					
					if(internalList.getResults() !=null){
						for (SolrDocument internalDoc : internalList.getResults()) {
							List<String> intList =  (List<String>) internalDoc.getFieldValue("friendsList");
							for (String intString : intList) {
								User user = new User();
								user.setId(intString);
								user.setDisplayName(intString);									
								response.add(user);
							}
						}
					}
				}			
			}
		} catch(Exception e){
			return response;
		}
		return response;
	}

	public Count getCounts(String username, String type) {
		Count count = new Count();
		count.setStarted(getPosted(username, type));
		count.setFollowing(getFollowing(username, type));
		count.setReplied(getReplied(username, type));
		
		if(type.equalsIgnoreCase("job")){
			count.setShorlisted(getShorlisted(username, type));
			count.setApplied(getApplied(username, type));
		}
		return count;
	}

	private int getApplied(String username, String type) {
		try{
			SolrQuery userQuery = new SolrQuery();
			//userQuery.setQuery("username"+username);
			userQuery.setQuery("itemType:"+type);
			userQuery.setFilterQueries("applyList:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
			return rsp.getResults().size();
		} catch(Exception e){
			return 0;
		}
	}

	private int getShorlisted(String username, String type) {
		try{
			SolrQuery userQuery = new SolrQuery();
			//userQuery.setQuery("username:"+username);
			userQuery.setQuery("itemType:"+type);
			userQuery.setFilterQueries("shorlistList:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
			return rsp.getResults().size();
		} catch(Exception e){
			return 0;
		}
	}

	private int getPosted(String username, String type) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("username:"+username);
			userQuery.setFilterQueries("itemType:"+type);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
			return rsp.getResults().size();
		} catch(Exception e){
			return 0;
		}
	}
	
	private int getFollowing(String username, String type) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("username:"+username +" AND itemType:"+type);
			userQuery.setFilterQueries();
			userQuery.setFilterQueries("viewedList:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
			return rsp.getResults().size();
		} catch(Exception e){
			return 0;
		}
	}
	
	private int getReplied(String username, String type) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("username:"+username +" AND itemType:"+type);
			userQuery.setFilterQueries("commentList:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.item.collection")); 
			
			return rsp.getResults().size();
		} catch(Exception e){
			return 0;
		}
	}	
}
