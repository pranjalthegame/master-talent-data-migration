package com.master.talent.api.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.master.talent.api.entity.CompanyEntity;
import com.master.talent.api.entity.UserOccupationEntity;
import com.master.talent.api.entity.UserPreSignUpEntity;
import com.master.talent.api.exception.SearchException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.Occupation;
import com.master.talent.api.models.User;

@Component
public class IndexUtility {
	
	private final Logger logger = LoggerFactory.getLogger(IndexUtility.class);
	
	SolrClient solrClient = new HttpSolrClient.Builder(SpringPropertiesUtil.getProperty("solr.server.url")).build();
		
	public void updateUserTypeWithRecriter(String username, String type) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", username);
			Map<String, String> userType = new HashMap<String, String>();
			userType.put("add", type);
			doc.addField("userType", userType);
			
			UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
				
		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}
	}
	
	public void removePendingStatus(String username) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", username);

			Map<String, String> removeUserType = new HashMap<String, String>();
			removeUserType.put("remove", "pendingTrainer");
			doc.addField("userType", removeUserType);
			
			UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}		
	}
	
	public void indexDocToSolr(UserPreSignUpEntity obj, String type) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			if(type.equalsIgnoreCase("CREATE")){
				doc.addField("id", obj.getUsername());
				doc.addField("username", obj.getUsername());
			    doc.addField("fullname", obj.getFullname().toLowerCase());
			    doc.addField("rating", "0/0");	
			    if(obj.getBreif() !=null){
			    	doc.addField("brief", obj.getBreif().toLowerCase());
			    }
			    if(obj.getPhone() !=null){
			    	doc.addField("phone", obj.getPhone().get(0).getCountryCode()+"-"+obj.getPhone().get(0).getPhoneNumber());
			    }
			    //adding created date from user object instead of using system current date	
			    doc.addField("created", obj.getCreatedDate());
			} else if(type.equalsIgnoreCase("UPDATE")){
				doc.addField("id", obj.getUsername());
				if(obj.getFullname() != null){
					Map<String, String> partialNameUpdate = new HashMap<String, String>();
					partialNameUpdate.put("set", obj.getFullname().toLowerCase());
					doc.addField("fullname", partialNameUpdate);
				} if(obj.getBreif() != null){
					Map<String, String> partialBreifUpdate = new HashMap<String, String>();
					partialBreifUpdate.put("set", obj.getBreif().toLowerCase());
					doc.addField("brief", partialBreifUpdate);
				} if(obj.getPhone() != null){
					Map<String, String> phone = new HashMap<String, String>();
					phone.put("set", obj.getPhone().get(0).getCountryCode()+"-"+obj.getPhone().get(0).getPhoneNumber());
					doc.addField("phone", phone);
				} if(obj.getUserType()!=null){
					if(!obj.getUserType().isEmpty()){
						for(String userTypeList: obj.getUserType()){
							if(userTypeList.equalsIgnoreCase("trainer")){
								Map<String, String> userType = new HashMap<String, String>();
								userType.put("add", "trainer");
								doc.addField("userType", userType);
								removePendingStatus(obj.getUsername());
							}
							if(userTypeList.equalsIgnoreCase("pendingTrainer")){
								Map<String, String> userType = new HashMap<String, String>();
								userType.put("add", "pendingTrainer");
								doc.addField("userType", userType);
							}							
							if(userTypeList.equalsIgnoreCase("rejectTrainer")){
								Map<String, String> userType = new HashMap<String, String>();
								userType.put("remove", "pendingTrainer");
								doc.addField("userType", userType);
							}
						}
					}			
				}
				if(obj.getEmployment() !=null){
					if(obj.getEmployment().get(0).getExpertise() != null){
						 Map<String, String> workExp = new HashMap<String, String>();
						 workExp.put("set", obj.getEmployment().get(0).getExpertise().toLowerCase());
						doc.addField("workExp", workExp);
					 }
				    if(obj.getEmployment().get(0).getCategory() != null){
				    	Map<String, String> workType = new HashMap<String, String>();
				    	workType.put("set", obj.getEmployment().get(0).getCategory().toLowerCase());
						doc.addField("workType", workType);
				    }
				    if(obj.getEmployment().get(0).getWorkStatus() != null){
				    	Map<String, String> workStatus = new HashMap<String, String>();
				    	workStatus.put("set", obj.getEmployment().get(0).getWorkStatus().toLowerCase());
						doc.addField("workStatus", workStatus);
				    }
				}
			}

			UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}		
	}
	
	public void updateUserFriendsIndxList(String requestor, String requested, String type) throws SolrServerException, IOException {
		SolrInputDocument doc = new SolrInputDocument();
		if(type.equalsIgnoreCase("DELETE")){
			doc.addField("id", requested);
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("set", requested);
			Map<String, String> partialRemoveUpdate = new HashMap<String, String>();
			partialRemoveUpdate.put("remove", requestor);
		    doc.addField("pendingFirendsList", partialUpdate);
		    doc.addField("friendsList", partialRemoveUpdate);
		}
		if(type.equalsIgnoreCase("REQUEST")){
			doc.addField("id", requested);
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("add", requestor);
		    doc.addField("pendingFirendsList", partialUpdate);
		}
		if(type.equalsIgnoreCase("ACCEPT")){
			doc.addField("id", requestor);
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("add", requested);
			Map<String, String> partialRequestedRemoveUpdate = new HashMap<String, String>();
			partialRequestedRemoveUpdate.put("remove", requested);
		    doc.addField("friendsList", partialUpdate);
		    doc.addField("pendingFirendsList", partialRequestedRemoveUpdate);
		}

		UpdateRequest req = new UpdateRequest(); 
	    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		req.add(doc); 
		req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
		req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
	}
	
	public void updateUserFriendsRequestorIndxList(String requestor, String requested, String type) throws SolrServerException, IOException {
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", requestor);
		if(type.equalsIgnoreCase("DELETE")){
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("set", requestor);
			Map<String, String> partialRemoveUpdate = new HashMap<String, String>();
			partialRemoveUpdate.put("remove", requestor);
		    doc.addField("requestedPendingFirendsList", partialUpdate);
		    doc.addField("friendsList", partialRemoveUpdate);
		}
		if(type.equalsIgnoreCase("REQUEST")){
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("add", requested);
		    doc.addField("requestedPendingFirendsList", partialUpdate);
		}
		if(type.equalsIgnoreCase("ACCEPT")){
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("add", requested);
			Map<String, String> partialRemoveUpdate = new HashMap<String, String>();
			partialRemoveUpdate.put("remove", requested);
		    doc.addField("friendsList", partialUpdate);
		    doc.addField("requestedPendingFirendsList", partialRemoveUpdate);
		}

		UpdateRequest req = new UpdateRequest(); 
	    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		req.add(doc); 
		req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
		req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
	}
	
	public void updateUserFollowIndxList(String follower, String followed, String type) throws SolrServerException, IOException {
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", followed);
		if(type.equalsIgnoreCase("DELETE")){
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("remove", follower);
		    doc.addField("followersList", partialUpdate);
		}
		if(type.equalsIgnoreCase("REQUEST")){
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("add", follower);
		    doc.addField("followersList", partialUpdate);
		}

		UpdateRequest req = new UpdateRequest(); 
	    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		req.add(doc); 
		req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
		req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
	}
	
	public void updateCompanyFollowIndxList(String username, String compId, String type) throws SolrServerException, IOException {
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", compId);
		if(type.equalsIgnoreCase("DELETE")){
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("remove", username);
		    doc.addField("followersList", partialUpdate);
		}
		if(type.equalsIgnoreCase("REQUEST")){
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("add", username);
		    doc.addField("followersList", partialUpdate);
		}

		UpdateRequest req = new UpdateRequest(); 
	    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		req.add(doc); 
		req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
		req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
		
	}
	
	public void indexLocationToUserSolr(String username, String locationName, String country) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", username);
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("set", locationName);
		    doc.addField("location", partialUpdate);
		    
		    Map<String, String> countrypartialUpdate = new HashMap<String, String>();
		    countrypartialUpdate.put("set", country);
		    doc.addField("country", countrypartialUpdate);
		    
		    UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}	
	}
	
	public String getCompanyReview(String compId){
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+compId);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
			
			if(rsp.getResults()!=null){
				if(!rsp.getResults().isEmpty()){
					if(rsp.getResults().get(0).getFieldValue("rating") !=null)
						return rsp.getResults().get(0).getFieldValue("rating").toString();
					else
						return "0/0";
				}				
			} else {
				logger.debug("company name not found ::",compId);
				return "0/0";
			}
			return "0/0";
		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}
    }
	
	public String getUserReview(String username){
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("id:"+username);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp.getResults()!=null){
				if(rsp.getResults().get(0).getFieldValue("rating") !=null)
					return rsp.getResults().get(0).getFieldValue("rating").toString();
				else
					return "0/0";
			} else {
				logger.debug("username not found ::",username);
				return null;
			}
		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}
    }
	
	public void updateCompanyRatingSolr(String id, String rating) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id",id);
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("set", rating);
		    doc.addField("rating", partialUpdate);			

		    UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 

		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}
	}
	
	public void updateUserRatingSolr(String id, String rating) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id",id);
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("set", rating);
		    doc.addField("rating", partialUpdate);			

		    UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 

		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}
	}

	public void indexCompanyToSolr(CompanyEntity obj, String type) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			if(type.equalsIgnoreCase("CREATE")){
				doc.addField("id", obj.getPageId());
				doc.addField("pageTitle", obj.getTitle() != null ? obj.getTitle() : "");
			    doc.addField("owner", obj.getOwner() != null ? obj.getOwner() : "");
			    doc.addField("userList", obj.getAdmins() != null ? obj.getAdmins() : "");
			    doc.addField("pageUrl", obj.getPageUrl() != null ? obj.getPageUrl() : "");
			    doc.addField("desc", obj.getDesc() != null ? obj.getDesc().toLowerCase() : "");
			    doc.addField("type", obj.getType() != null ? obj.getType().toLowerCase() : "");
			    doc.addField("offers", obj.getOffers() != null ? obj.getOffers() : "");			    
			    doc.addField("businessType", obj.getBusinessType() != null ? obj.getBusinessType().toLowerCase() : "");
			    doc.addField("placementAssistance", obj.getPlacementAssistance() != null ? obj.getPlacementAssistance().toLowerCase() : "");
			    doc.addField("financialAssistance", obj.getFinancialAssistance() != null ? obj.getFinancialAssistance().toLowerCase() : "");
			    doc.addField("pageCategory", obj.getCategory() != null ? obj.getCategory().toLowerCase() : "");			    
			    if(obj.getAddress() !=null){
			    	String addressString = null;
			    	for (String string : obj.getAddress()) {
			    		addressString = addressString + " " + string;
					}
			    doc.addField("address", addressString != null ? addressString.toLowerCase() : "");
			    }
				UpdateRequest req = new UpdateRequest(); 
			    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
				req.add(doc); 
				req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
				req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection"));			    
			}
			if(type.equalsIgnoreCase("DELETE")){
				if(obj.isDeleted()){									
					UpdateRequest req = new UpdateRequest(); 
				    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
				    req.deleteById(obj.getPageId());
					req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
					req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
				}
			}
			if(type.equalsIgnoreCase("UPDATE")){
				doc.addField("id", obj.getPageId());
				if(obj.getTitle() !=null){
					Map<String, String> partialNameUpdate = new HashMap<String, String>();
					partialNameUpdate.put("set", obj.getTitle().toLowerCase());
					doc.addField("pageTitle", partialNameUpdate);
				}
				if(obj.getOwner() !=null){
				    Map<String, String> partialNameUpdate = new HashMap<String, String>();
					partialNameUpdate.put("set", obj.getOwner().toLowerCase());
					doc.addField("owner", partialNameUpdate);
				}
				if(obj.getAdmins() !=null){
					doc.addField("userList", obj.getAdmins());
				}
				if(obj.getPageUrl() !=null){
					Map<String, String> partialNameUpdate = new HashMap<String, String>();
					partialNameUpdate.put("set", obj.getPageUrl().toLowerCase());
					doc.addField("pageUrl", partialNameUpdate);
				}
				if(obj.getDesc() !=null){
				    Map<String, String> partialNameUpdate = new HashMap<String, String>();
					partialNameUpdate.put("set", obj.getDesc().toLowerCase());
					doc.addField("desc", partialNameUpdate);
				}
				if(obj.getOffers() !=null){	
					doc.addField("offers", obj.getOffers());
				} 
				if(obj.getBusinessType() !=null){
					Map<String, String> partialNameUpdate = new HashMap<String, String>();
					partialNameUpdate.put("set", obj.getBusinessType().toLowerCase());
					doc.addField("businessType", partialNameUpdate);
				} 
				if(obj.getPlacementAssistance() !=null){
					Map<String, String> partialNameUpdate = new HashMap<String, String>();
					partialNameUpdate.put("set", obj.getPlacementAssistance().toLowerCase());
					doc.addField("placementAssistance", partialNameUpdate);
				} 
				if(obj.getFinancialAssistance() !=null){
					Map<String, String> partialNameUpdate = new HashMap<String, String>();
					partialNameUpdate.put("set", obj.getFinancialAssistance().toLowerCase());
					doc.addField("financialAssistance", partialNameUpdate);
				} 
				if(obj.getCategory() !=null){
					Map<String, String> partialNameUpdate = new HashMap<String, String>();
					partialNameUpdate.put("set", obj.getCategory().toLowerCase());
					doc.addField("pageCategory", partialNameUpdate);
				}  
				if(obj.getAddress() !=null){
			    	String addressString = null;
			    	for (String string : obj.getAddress()) {
			    		addressString = addressString + " " + string;
					}
			    	doc.addField("address", addressString != null ? addressString.toLowerCase() : "");
			    }

				UpdateRequest req = new UpdateRequest(); 
			    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
				req.add(doc); 
				req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
				req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
			}			
		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}		
	}

	@SuppressWarnings("unchecked")
	public List<CompanyEntity> getAllCompanyByUser(String username, int start, int end) {
		List<CompanyEntity> listCompany = new ArrayList<CompanyEntity>();
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("owner:"+username + " OR userList:"+username);
			userQuery.setStart(start);
			userQuery.setRows(end);

			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
			
			if(rsp.getResults() != null){
				for (SolrDocument doc : rsp.getResults()) {
					CompanyEntity companyEntity = new CompanyEntity();
					companyEntity.setPageId(doc.getFieldValue("id").toString());
					companyEntity.setTitle(doc.getFieldValue("pageTitle").toString());
					companyEntity.setDesc(doc.getFieldValue("desc").toString());
					companyEntity.setOwner(doc.getFieldValue("owner").toString());
					companyEntity.setAdmins((List<String>) doc.getFieldValue("userList"));
					companyEntity.setPageUrl(doc.getFieldValue("pageUrl").toString());
					listCompany.add(companyEntity);
				}	
			} 
			return listCompany;
		} catch(Exception e){
			logger.debug("getAllCompanyByUser failure", e);
			return null;
		}		
	}

	public boolean getReturnTypeDefination(String loggedInuser, String requested) {
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("owner:"+loggedInuser + " OR userList:"+loggedInuser);

			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.company.collection")); 
			
			if(rsp.getResults() != null){
				return true;
			} else {
				return false;
			}
		} catch(Exception e){
			logger.debug("getReturnTypeDefination failure", e);
			return false;
		}		
	}

	public List<User> getPendingUsers(String type, int start, int end) {
		List<User> response = new ArrayList<User>();
		try{
			SolrQuery userQuery = new SolrQuery();
			userQuery.setQuery("userType:"+type);
			userQuery.setStart(start);
			userQuery.setRows(end);
			
			QueryRequest req = new QueryRequest(userQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
			if(rsp.getResults() != null){
				for (SolrDocument doc : rsp.getResults()) {
					User user = new User();				
					user.setId(doc.getFieldValue("id").toString());
					user.setDisplayName(doc.getFieldValue("fullname").toString());
					response.add(user);						
				}				
			}
		} catch(Exception e){
			return response;
		}
		return response;
	}

	public void indexOccupationToSolr(UserOccupationEntity obj) {
		try{
			SolrInputDocument doc = new SolrInputDocument();			
			doc.addField("id", obj.getUsername());	
			String orgName = "";
			String industry = "";
			
			for (Occupation occupation : obj.getOccupation()) {
				orgName = orgName + " " + occupation.getOrgName();
				industry = industry + " " + occupation.getIndustry();
			}			
			doc.addField("orgName", orgName);	
			doc.addField("industry", industry);

			UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.user.collection")); 
			
		} catch(Exception e){
			logger.debug("solr error", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}		
	}

	public String getReferral(String emailAddress) {
		try{
			SolrQuery referrerQuery = new SolrQuery();
			referrerQuery.setQuery("invitieesList:"+emailAddress);
			referrerQuery.setStart(0);
			referrerQuery.setRows(1);
			QueryRequest req = new QueryRequest(referrerQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.invited.collection")); 
			if (!rsp.getResults().isEmpty()){
				SolrDocument doc = rsp.getResults().get(0);
				return doc.getFieldValue("username").toString();
			}
			return "rs.ritesh";
		} catch(Exception e){
			return "rs.ritesh";
		}
		
	}

}
