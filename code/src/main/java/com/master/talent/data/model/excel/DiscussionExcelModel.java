/**
 * 
 */
package com.master.talent.data.model.excel;

import com.poiji.internal.annotation.ExcelCell;

/**
 * @author pranjal.sinha
 *
 */
public class DiscussionExcelModel {

	@ExcelCell(0)
	private String owner;
	@ExcelCell(1)
	private String itemType;
	@ExcelCell(2)
	private String created;
	@ExcelCell(3)
	private boolean isDelete;
	@ExcelCell(4)
	private String videoLink;
	@ExcelCell(5)
	private String title;
	@ExcelCell(6)
	private String photoPath;
	@ExcelCell(7)
	private String desc;
	@ExcelCell(8)
	private String updated;
	public String getOwner() {
		return owner;
	}
	public String getItemType() {
		return itemType;
	}
	public String getCreated() {
		return created;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public String getVideoLink() {
		return videoLink;
	}
	public String getTitle() {
		return title;
	}
	public String getPhotoPath() {
		return photoPath;
	}
	public String getDesc() {
		return desc;
	}
	public String getUpdated() {
		return updated;
	}
	@Override
	public String toString() {
		return "DiscussionExcelModel [owner=" + owner + ", itemType=" + itemType + ", created=" + created
				+ ", isDelete=" + isDelete + ", videoLink=" + videoLink + ", title=" + title + ", photoPath="
				+ photoPath + ", desc=" + desc + ", updated=" + updated + "]";
	}
	
	
}
