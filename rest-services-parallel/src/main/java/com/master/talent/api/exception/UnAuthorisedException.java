package com.master.talent.api.exception;

import com.master.talent.api.exception.base.MasterTalentException;

public class UnAuthorisedException extends MasterTalentException {
   
	private static final long serialVersionUID = 1L;
	
	public UnAuthorisedException() {
        super();
    }
    public UnAuthorisedException(String message, Throwable cause) {
        super(message, cause);
    }
    public UnAuthorisedException(String message) {
        super(message);
    }
    public UnAuthorisedException(Throwable cause) {
        super(cause);
    }
	public UnAuthorisedException(String exceptionCode, String message, Throwable cause) {
		super(exceptionCode, message, cause);
	}
	public UnAuthorisedException(String exceptionCode, String message) {
		super(exceptionCode, message);
	}
    
}