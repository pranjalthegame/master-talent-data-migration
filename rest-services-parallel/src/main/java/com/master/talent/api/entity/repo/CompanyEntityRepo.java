package com.master.talent.api.entity.repo;


import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.CompanyEntity;


@Repository
public interface CompanyEntityRepo extends CouchbaseRepository<CompanyEntity, String>
{
}

