/**
 * 
 */
package com.master.talent.data.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.master.talent.data.callabletasks.CallableCompanyTask;
import com.master.talent.data.constants.AppConstants;
import com.master.talent.data.model.CompanyModel;
import com.master.talent.data.model.excel.CompanyExcelModel;
import com.master.talent.data.service.CompanyFeedService;
import com.master.talent.data.service.ReaderService;
import com.master.talent.data.service.RestService;
import com.master.talent.data.util.CommonUtils;
import com.master.talent.data.util.CompanyConverterUtils;

/**
 * @author pranjal.sinha
 *
 */
@Service
public class CompanyFeedServiceImpl implements CompanyFeedService {
	
	private static final Logger LOG = LoggerFactory.getLogger(CompanyFeedServiceImpl.class);
	
	@Autowired
	private ReaderService readerService;
	
	@Autowired
	private RestService restService;
	
	private static final int MAX_NO_OF_BATCHES = 20;
	
	/**
	 * below code runs in single thread mode
	 */
	@Override
	public void processCompanies() {
		List<CompanyExcelModel> companyData = readerService.readCompanyFeedFile(AppConstants.COMPANY_FEED_FILE);
		if(companyData!=null && !companyData.isEmpty()){
			for(CompanyExcelModel inputData: companyData){
				CompanyModel companyModel = CompanyConverterUtils.convertCompany(inputData);
				restService.processCompanyOverHttp(companyModel);
			}
		}
			
	}

	/**
	 * below code template will split the data set into batches and process them concurrently
	 * 
	 */
	@Override
	public void processCompaniesConcurrently() {
		List<CompanyExcelModel> companyData = readerService.readCompanyFeedFile(AppConstants.COMPANY_FEED_FILE);
		if(companyData!=null && !companyData.isEmpty()){
			List<List<CompanyExcelModel>> splitList = CommonUtils.createBatches(companyData, MAX_NO_OF_BATCHES);
			int size = splitList.size();
			ExecutorService executorService = Executors.newFixedThreadPool(size);
			LOG.info("Initializing Thread pool of size: "+splitList.size());
			try{
				ExecutorCompletionService<Map<String,String>> completionService = new ExecutorCompletionService<>(executorService);
				for(List<CompanyExcelModel> companyBatch: splitList){
					LOG.info("Batch size to be procesed is: "+companyBatch.size());
					completionService.submit(new CallableCompanyTask(restService,companyBatch));
				}CommonUtils.pollForResult(completionService, size,AppConstants.TYPE_COMPANY);
			}finally{
				executorService.shutdown();
			}
		}
	}

}
