package com.master.talent.api.models;

import java.util.Date;

public class User {	
	
	private String id;
	private String username;
	private String displayName;
	private boolean pending;
	private Date created;
	
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}	
	public boolean isPending() {
		return pending;
	}
	public void setPending(boolean pending) {
		this.pending = pending;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", created="
				+ created + "]";
	}	
}
