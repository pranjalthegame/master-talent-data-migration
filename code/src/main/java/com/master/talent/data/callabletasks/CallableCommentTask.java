/**
 * 
 */
package com.master.talent.data.callabletasks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.master.talent.data.model.CommentModel;
import com.master.talent.data.model.ResponseObject;
import com.master.talent.data.model.excel.CommentExcelModel;
import com.master.talent.data.service.RestService;
import com.master.talent.data.util.CommentConverterUtils;
import com.master.talent.data.util.JsonUtils;

/**
 * @author pranjal.sinha
 *
 */
public class CallableCommentTask implements Callable<Map<String,String>> {

	
	private RestService restService;
	private List<CommentExcelModel> commentBatch;
	
	public CallableCommentTask(RestService restService,List<CommentExcelModel> commentBatch) {
		this.restService = restService;
		this.commentBatch = commentBatch;
	}
	
	@Override
	public Map<String, String> call() throws Exception {
		Map<String,String> response = new HashMap<>();
		Thread.currentThread().setName(getThreadName());
		for(CommentExcelModel inputData: commentBatch){
			CommentModel commentModel = CommentConverterUtils.convertComment(inputData);
			ResponseObject res = restService.processCommentOverHttp(commentModel);
			String commentId = parseResponseForId(res);
			if(commentId!=null){
				response.put(commentId, JsonUtils.toJson(res));
			}else{
				response.put("Error-"+commentModel.getUsername(), JsonUtils.toJson(res));
			}
		}return response;
	}
	
	private String getThreadName(){
		String threadName = "master-talent-comment-data-migration-thread";
		Long threadId = Thread.currentThread().getId();
		threadName = threadName + "-" + threadId;
		return threadName;
	}
	
	private String parseResponseForId(ResponseObject res){
		try{
			String[] splitArr = res.getResponseBody().split(",");
			String value = splitArr[0];
			String[] idArr = value.split(":");
			return (idArr[1].substring(1, idArr[1].length()-1));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
