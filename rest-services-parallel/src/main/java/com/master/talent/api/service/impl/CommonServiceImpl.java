package com.master.talent.api.service.impl;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.master.talent.api.dao.CommonDao;
import com.master.talent.api.dao.SearchDao;
import com.master.talent.api.dao.SessionDao;
import com.master.talent.api.entity.ReviewEntity;
import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.entity.UserCartEntity;
import com.master.talent.api.entity.UserPurchasedServicesEntity;
import com.master.talent.api.entity.repo.ReviewEntityRepo;
import com.master.talent.api.models.Count;
import com.master.talent.api.models.RequestInvitation;
import com.master.talent.api.service.CommonService;
import com.master.talent.api.models.Purchases;
import com.master.talent.api.utils.EmailUtility;
import com.master.talent.api.utils.FileJsonProcessor;
import com.master.talent.api.utils.SpringPropertiesUtil;

@CrossOriginResourceSharing(
        allowOrigins = {
           "*"
        }, 
        allowCredentials = true, 
        maxAge = 1, 
        allowHeaders = {
           "MTUser"
        }
)
public class CommonServiceImpl implements CommonService {

	private final Logger logger = LoggerFactory.getLogger(CommonServiceImpl.class);
	
	private final Gson serializer = new Gson();
	
	@Autowired
	private  RequestValidator validator;

	@Autowired
	private EmailUtility emailUtility;
	
	@Autowired
	private SessionDao sessionDao;
	
	@Autowired
	private SearchDao searchDao;
	
	@Autowired
	private	CommonDao commonDao;
	
	@Autowired
	private	ReviewEntityRepo reviewEntityRepo;
	
	@Override
	public Response getCountryCodes(HttpHeaders headers) {
		validator.validateBefore(headers);
		Object response = FileJsonProcessor.getJson(SpringPropertiesUtil.getProperty("country.code.fileName"));
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response getLocation(HttpHeaders headers, String country) {
		validator.validateBefore(headers);
		Object response = FileJsonProcessor.getJson(SpringPropertiesUtil.getProperty("location.country.wise")+country+".json");
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response getCurrency(HttpHeaders headers) {
		validator.validateBefore(headers);
		Object response = FileJsonProcessor.getJson(SpringPropertiesUtil.getProperty("currency.list.fileName"));
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response getCart(HttpHeaders headers) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		UserCartEntity response = commonDao.gerCartByUser(sessionEntity.getUsername());
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response addToCart(HttpHeaders headers, Purchases purchases) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		UserCartEntity response = commonDao.addToCart(sessionEntity.getUsername(), purchases);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response removeFromCart(HttpHeaders headers, String purchaseId) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		UserCartEntity response = commonDao.removeFromCart(sessionEntity.getUsername(), purchaseId);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response getPaidServices(HttpHeaders headers) {
		validator.validateBefore(headers);
		Object response = FileJsonProcessor.getJson(SpringPropertiesUtil.getProperty("paid.services.fileName"));
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response getCategory(HttpHeaders headers) {
		validator.validateBefore(headers);
		Object response = FileJsonProcessor.getJson(SpringPropertiesUtil.getProperty("category.list.fileName"));
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	/*@Override
	public Response purchase(HttpHeaders headers) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		UserPurchasedServicesEntity response = commonDao.purchase(sessionEntity.getUsername(), sessionEntity.getReferral(), purchases);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}*/
	
	@Override
	public Response listPurchase(HttpHeaders headers, String type) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		UserPurchasedServicesEntity response = commonDao.listPurchase(sessionEntity.getUsername(), type);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response showContact(HttpHeaders headers, String targetUser) {
		boolean response = false;
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		if(sessionEntity.getUsername() != null){
			response = searchDao.verifyAFirend(sessionEntity.getUsername(), targetUser);
			if(!response){
				response = commonDao.showContact(sessionEntity.getUsername());
			}
			if(response){
				return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
						.header("Access-Control-Allow-Origin", "*")
						.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
			            .header("Access-Control-Allow-Credentials", "true")
			            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			            .header("Access-Control-Max-Age", "1209600")
						.build();
			} else {
				return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
						.header("Access-Control-Allow-Origin", "*")
						.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
			            .header("Access-Control-Allow-Credentials", "true")
			            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			            .header("Access-Control-Max-Age", "1209600")
						.build();
			}
		} else {
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		}		
	}

	@Override
	public Response sendEmail(HttpHeaders headers, String username, String type) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		emailUtility.sendOtp(username, type);
		return Response.ok("{\"success\" : \"mail sent\"}", MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response sendInvitation(HttpHeaders headers, RequestInvitation requestInvitation) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		if(requestInvitation != null){
			emailUtility.sendInvitation(requestInvitation);
			return Response.ok("{\"success\" : \"mail sent\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		} else {
			return Response.ok("{\"success\" : \"mail invitation failed, please try again latter.\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		}
		
	}

	@Override
	public Response getSession(HttpHeaders headers) {
		validator.validateBefore(headers);
		try{
			SessionEntity response  = sessionDao.validateSession(headers.getRequestHeader(SpringPropertiesUtil.getProperty("master.talent.cookiename")).get(0));
			if (response != null){
				return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
						.header("Access-Control-Allow-Origin", "*")
						.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
			            .header("Access-Control-Allow-Credentials", "true")
			            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			            .header("Access-Control-Max-Age", "1209600")
						.build();
			} else {
				return Response.ok("{\"error\" : \"invalid session\"}", MediaType.APPLICATION_JSON)
						.header("Access-Control-Allow-Origin", "*")
						.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
			            .header("Access-Control-Allow-Credentials", "true")
			            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			            .header("Access-Control-Max-Age", "1209600")
						.build();

			}
		}catch(Exception e){
			logger.debug("Error ::" + e);
			return Response.ok("{\"error\" : \"invalid session\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		}
	}

	@Override
	public Response isAFriend(HttpHeaders headers, String username) {
		SessionEntity sess = validator.validateAndReturn(headers);
		boolean response = searchDao.verifyAFirend(sess.getUsername(), username);
		if (response){
			return Response.ok("{\"success\" : \"valid friend\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		} else {
			return Response.ok("{\"error\" : \"not a friend\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();

		}
	}

	@Override
	public Response isAFollower(HttpHeaders headers, String username) {
		SessionEntity sess = validator.validateAndReturn(headers);
		boolean response = searchDao.verifyAFollower(sess.getUsername(), username);
		if (response){
			return Response.ok("{\"success\" : \"valid follower\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		} else {
			return Response.ok("{\"error\" : \"invalid followe\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();

		}
	}

	@Override
	public Response profileView(HttpHeaders headers, String username) {
		SessionEntity sess = validator.validateAndReturn(headers);
		if(sess.getUsername()!=null){
			commonDao.createProfileView(sess.getUsername(), username);
		} else {
			commonDao.createProfileView("UnKnown", username);
		}
			
		return Response.ok("{\"success\" : \"view updated\"}", MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}

	@Override
	public Response profileViews(HttpHeaders headers, int from, int to) {
		SessionEntity sess = validator.validateAndReturn(headers);
		Object response = commonDao.getProfileViews(sess.getUsername(), from, to);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}

	@Override
	public Response chartViews(HttpHeaders headers, String type) {
		SessionEntity sess = validator.validateAndReturn(headers);
		Object response;
		if(type.equalsIgnoreCase("week")){
			response = commonDao.getChart(sess.getUsername(), "week");
		} else if(type.equalsIgnoreCase("month")){
			response = commonDao.getChart(sess.getUsername(), "month");
		} else {
			return Response.ok("{\"error\" : \"invalid request\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		}
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		        .header("Access-Control-Allow-Credentials", "true")
		        .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		        .header("Access-Control-Max-Age", "1209600")
				.build();
	}

	@Override
	public Response createReview(HttpHeaders headers, ReviewEntity reviewEntity) {
		SessionEntity sess = validator.validateAndReturn(headers);
		Object response = commonDao.createReview(reviewEntity, sess.getUsername());
		if (response!=null){
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		} else {
			return Response.ok("{\"error\" : \"review create failed\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		}
	}

	@Override
	public Response updateReview(HttpHeaders headers, ReviewEntity reviewEntity) {
		SessionEntity sess = validator.validateAndReturn(headers);
		ReviewEntity check = reviewEntityRepo.findOne(reviewEntity.getId());
		Object response = null;
		if(check!=null){
			if(check.getReviewOwner().equals(sess.getUsername()) || check.getReviewId().equals(sess.getUsername())){
				response = commonDao.updateReview(reviewEntity, sess.getUsername());
			}else{
				return Response.ok("{\"error\" : \"invalid user update for the review\"}", MediaType.APPLICATION_JSON)
						.header("Access-Control-Allow-Origin", "*")
						.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
			            .header("Access-Control-Allow-Credentials", "true")
			            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			            .header("Access-Control-Max-Age", "1209600")
						.build();
			}
		}		
		if (response!=null){
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		} else {
			return Response.ok("{\"error\" : \"review update failed\"}", MediaType.APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		            .header("Access-Control-Allow-Credentials", "true")
		            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		            .header("Access-Control-Max-Age", "1209600")
					.build();
		}
	}

	@Override
	public Response getReviews(HttpHeaders headers,String type, String username) {
		SessionEntity sess = validator.validateAndReturn(headers);
		Object response = commonDao.getAllReviews(sess.getUsername(), username, type);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}

	@Override
	public Response getItemCounts(HttpHeaders headers, String type) {
		SessionEntity sess = validator.validateAndReturn(headers);
		Count response = searchDao.getCounts(sess.getUsername(), type);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
	}
	
	@Override
	public Response getPaymentOrder(HttpHeaders headers) {
		SessionEntity sess = validator.validateAndReturn(headers);
		UserCartEntity userCartEntity = commonDao.gerCartByUser(sess.getUsername());
		if(userCartEntity !=null){
			Object response = commonDao.getPaymentOrder(userCartEntity);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
		}else{
			return Response.ok("{\"error\" : \"cart is empty, nothing to process\"}", MediaType.APPLICATION_JSON)
			.header("Access-Control-Allow-Origin", "*")
			.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
			.build();
		}		
	}
	
}
