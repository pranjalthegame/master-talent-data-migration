/**
 * 
 */
package com.master.talent.data.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.master.talent.data.callabletasks.CallableUserTask;
import com.master.talent.data.constants.AppConstants;
import com.master.talent.data.model.UserModel;
import com.master.talent.data.model.excel.UserExcelModel;
import com.master.talent.data.service.ReaderService;
import com.master.talent.data.service.RestService;
import com.master.talent.data.service.UserFeedService;
import com.master.talent.data.util.CommonUtils;
import com.master.talent.data.util.UserConverterUtils;

/**
 * @author pranjal.sinha
 *
 */
@Service
public class UserFeedServiceImpl implements UserFeedService {

	private static final Logger LOG = LoggerFactory.getLogger(UserFeedServiceImpl.class);
	
	@Autowired
	private ReaderService readerService;
	
	@Autowired
	private RestService restService;
	
	private static final int MAX_NO_OF_BATCHES = 20;
	
	/**
	 * below code runs in single thread mode
	 */
	@Override
	public void processUsers() {
		List<UserExcelModel> userData = readerService.readUserFeedFile(AppConstants.USER_FEED_FILE);
		if(userData!=null && !userData.isEmpty()){
			for(UserExcelModel inputData: userData){
				UserModel userModel = UserConverterUtils.convertUser(inputData);
				restService.processUserOverHttp(userModel);
			}
		}
	}
	
	/**
	 * below code template will split the data set into batches and process them concurrently
	 * 
	 */
	@Override
	public void processUsersConcurrently() {
		List<UserExcelModel> userData = readerService.readUserFeedFile(AppConstants.USER_FEED_FILE);
		if(userData!=null && !userData.isEmpty()){
			List<List<UserExcelModel>> splitList = CommonUtils.createBatches(userData, MAX_NO_OF_BATCHES);
			int size = splitList.size();
			ExecutorService executorService = Executors.newFixedThreadPool(size);
			LOG.info("Initializing Thread pool of size: "+splitList.size());
			try{
				ExecutorCompletionService<Map<String,String>> completionService = new ExecutorCompletionService<>(executorService);
				for(List<UserExcelModel> userBatch: splitList){
					LOG.info("Batch size to be procesed is: "+userBatch.size());
					completionService.submit(new CallableUserTask(restService,userBatch));
				}CommonUtils.pollForResult(completionService, size,AppConstants.TYPE_USER);
			}finally{
				executorService.shutdown();
			}
		}	
	}
}
