package com.master.talent.api.models;

import org.joda.time.DateTime;


public class ItemShare {

	private String parentItemId;
	private String shareOwner;
	private String shareOwnerDisplayName;
	private DateTime created;
	private String shareMssg;
	
	
	public String getShareOwnerDisplayName() {
		return shareOwnerDisplayName;
	}
	public void setShareOwnerDisplayName(String shareOwnerDisplayName) {
		this.shareOwnerDisplayName = shareOwnerDisplayName;
	}
	public String getShareMssg() {
		return shareMssg;
	}
	public void setShareMssg(String shareMssg) {
		this.shareMssg = shareMssg;
	}
	public String getParentItemId() {
		return parentItemId;
	}
	public void setParentItemId(String parentItemId) {
		this.parentItemId = parentItemId;
	}
	public String getShareOwner() {
		return shareOwner;
	}
	public void setShareOwner(String shareOwner) {
		this.shareOwner = shareOwner;
	}
	public DateTime getCreated() {
		return created;
	}
	public void setCreated(DateTime created) {
		this.created = created;
	}
	@Override
	public String toString() {
		return "ItemShare [=" + ", parentItemId=" + parentItemId
				+ ", shareOwner=" + shareOwner + ", created=" + created +"]";
	}
}
