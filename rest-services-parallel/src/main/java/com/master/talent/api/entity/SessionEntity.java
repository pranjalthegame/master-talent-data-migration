package com.master.talent.api.entity;


import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document(expiry=10800)
public class SessionEntity
{

	@Id
	private String sessionId;
	
	@Field
	private String username;
	
	@Field
	private String referral;
	
	private List<String> jobRecommdations;
	
	private List<String> articleRecommdations;
	
	private List<String> discussionRecommdations;
	
	private List<String> tutorialsRecommdations;
	
	private List<String> userRecommdations;
	
	private boolean priorityApplication;
	private boolean proListing;
	private boolean jobListing;
	private boolean contactViewPack;
	
	public boolean isPriorityApplication() {
		return priorityApplication;
	}


	public void setPriorityApplication(boolean priorityApplication) {
		this.priorityApplication = priorityApplication;
	}


	public boolean isProListing() {
		return proListing;
	}


	public void setProListing(boolean proListing) {
		this.proListing = proListing;
	}


	public boolean isJobListing() {
		return jobListing;
	}


	public void setJobListing(boolean jobListing) {
		this.jobListing = jobListing;
	}


	public boolean isContactViewPack() {
		return contactViewPack;
	}


	public void setContactViewPack(boolean contactViewPack) {
		this.contactViewPack = contactViewPack;
	}


	public String getReferral() {
		return referral;
	}


	public void setReferral(String referral) {
		this.referral = referral;
	}

	public List<String> getUserRecommdations() {
		return userRecommdations;
	}

	public void setUserRecommdations(List<String> userRecommdations) {
		this.userRecommdations = userRecommdations;
	}

	public List<String> getJobRecommdations() {
		return jobRecommdations;
	}

	public void setJobRecommdations(List<String> jobRecommdations) {
		this.jobRecommdations = jobRecommdations;
	}

	public List<String> getArticleRecommdations() {
		return articleRecommdations;
	}

	public void setArticleRecommdations(List<String> articleRecommdations) {
		this.articleRecommdations = articleRecommdations;
	}

	public List<String> getDiscussionRecommdations() {
		return discussionRecommdations;
	}

	public void setDiscussionRecommdations(List<String> discussionRecommdations) {
		this.discussionRecommdations = discussionRecommdations;
	}

	public List<String> getTutorialsRecommdations() {
		return tutorialsRecommdations;
	}

	public void setTutorialsRecommdations(List<String> tutorialsRecommdations) {
		this.tutorialsRecommdations = tutorialsRecommdations;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "SessionEntity [sessionId=" + sessionId + ", username="
				+ username + "]";
	}
}
