package com.master.talent.api.entity.repo;


import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.UserAwardsAndRecogEntity;

@Repository
public interface UserAwarsAndRecogEntityRepo extends CouchbaseRepository<UserAwardsAndRecogEntity, String>
{
}