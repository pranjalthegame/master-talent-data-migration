package com.master.talent.api.models;



public class Events {

	private boolean liked;
	private boolean applied;
	private boolean shortlisted;
	
	private int likedCount;
	private int appliedCount;
	private int shortlistedCount;
	
	private long commentCount;
	
	
	public int getLikedCount() {
		return likedCount;
	}
	public void setLikedCount(int likedCount) {
		this.likedCount = likedCount;
	}
	public int getAppliedCount() {
		return appliedCount;
	}
	public void setAppliedCount(int appliedCount) {
		this.appliedCount = appliedCount;
	}
	public int getShortlistedCount() {
		return shortlistedCount;
	}
	public void setShortlistedCount(int shortlistedCount) {
		this.shortlistedCount = shortlistedCount;
	}
	public boolean isLiked() {
		return liked;
	}
	public void setLiked(boolean liked) {
		this.liked = liked;
	}
	public boolean isApplied() {
		return applied;
	}
	public void setApplied(boolean applied) {
		this.applied = applied;
	}
	public boolean isShortlisted() {
		return shortlisted;
	}
	public void setShortlisted(boolean shortlisted) {
		this.shortlisted = shortlisted;
	}
	public long getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(long commentCount) {
		this.commentCount = commentCount;
	}
	@Override
	public String toString() {
		return "Events [liked=" + liked + ", applied=" + applied
				+ ", shortlisted=" + shortlisted + ", likedCount=" + likedCount
				+ ", appliedCount=" + appliedCount + ", shortlistedCount="
				+ shortlistedCount + "]";
	}	
}
