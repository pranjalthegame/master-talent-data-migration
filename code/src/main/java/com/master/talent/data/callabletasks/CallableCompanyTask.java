/**
 * 
 */
package com.master.talent.data.callabletasks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.master.talent.data.model.CompanyModel;
import com.master.talent.data.model.ResponseObject;
import com.master.talent.data.model.excel.CompanyExcelModel;
import com.master.talent.data.service.RestService;
import com.master.talent.data.util.CompanyConverterUtils;
import com.master.talent.data.util.JsonUtils;

/**
 * @author pranjal.sinha
 *
 */
public class CallableCompanyTask implements Callable<Map<String,String>>{

	private RestService restService;
	private List<CompanyExcelModel> companyBatch;
	
	public CallableCompanyTask(RestService restService,List<CompanyExcelModel> companyBatch) {
		this.restService = restService;
		this.companyBatch = companyBatch;
	}
	
	@Override
	public Map<String, String> call() throws Exception {
		Map<String,String> response = new HashMap<>();
		Thread.currentThread().setName(getThreadName());
		for(CompanyExcelModel inputData: companyBatch){
			CompanyModel companyModel = CompanyConverterUtils.convertCompany(inputData);
			ResponseObject res = restService.processCompanyOverHttp(companyModel);
			String companyId = parseResponseForId(res);
			if(companyId!=null){
				response.put(companyId, JsonUtils.toJson(res));
			}else{
				response.put("Error-"+companyModel.getOwner(), JsonUtils.toJson(res));
			}
		}return response;
	}
	
	private String getThreadName(){
		String threadName = "master-talent-company-data-migration-thread";
		Long threadId = Thread.currentThread().getId();
		threadName = threadName + "-" + threadId;
		return threadName;
	}
	
	private String parseResponseForId(ResponseObject res){
		try{
			String[] splitArr = res.getResponseBody().split(",");
			String value = splitArr[0];
			String[] idArr = value.split(":");
			return (idArr[1].substring(1, idArr[1].length()-1));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
