/**
 * 
 */
package com.master.talent.data.model.excel;

import com.poiji.internal.annotation.ExcelCell;

/**
 * @author pranjal.sinha
 *
 */
public class CommentExcelModel {
	
	@ExcelCell(0)
	private String itemId;
	@ExcelCell(1)
	private String created;
	@ExcelCell(2)
	private boolean isDelete;
	@ExcelCell(3)
	private String updated;
	@ExcelCell(4)
	private String username;
	@ExcelCell(5)
	private String desc;
	
	public String getItemId() {
		return itemId;
	}
	public String getCreated() {
		return created;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public String getUpdated() {
		return updated;
	}
	public String getUsername() {
		return username;
	}
	public String getDesc() {
		return desc;
	}
	@Override
	public String toString() {
		return "CommentExcelModel [itemId=" + itemId + ", created=" + created + ", isDelete=" + isDelete + ", updated="
				+ updated + ", username=" + username + ", desc=" + desc + "]";
	}
	
	
}
