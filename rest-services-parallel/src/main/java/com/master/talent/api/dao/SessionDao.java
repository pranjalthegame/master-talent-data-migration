package com.master.talent.api.dao;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.entity.repo.SessionEntityRepo;

@Component
public class SessionDao {
	
	private final Logger logger = LoggerFactory.getLogger(SessionDao.class);
	
	@Autowired
	private SessionEntityRepo sessionEntityRepo;

	public SessionEntity createSession(String username) {
		return generateSessionObject(username);
	}
	
	private SessionEntity generateSessionObject(String username){
		SessionEntity sessionEntity = new SessionEntity();
		try{
			sessionEntity.setSessionId(username +" "+UUID.randomUUID());
			sessionEntityRepo.save(sessionEntity);
		}catch(Exception e){
			logger.debug("Error ::" + e);
			return null;
		}
		return sessionEntity;		
	}
	
	public SessionEntity validateSession(String sessionId) {
		try{
			SessionEntity sessionEntity = sessionEntityRepo.findOne(sessionId);
			return sessionEntity;
		}catch(Exception e){
			logger.debug("Error ::" + e);
			return null;
		}
	}
	
	public void deleteSession(String sessionId) {
		try{
			sessionEntityRepo.delete(sessionId);
		}catch(Exception e){
			logger.debug("Error ::" + e);
		}
	}
	
}
