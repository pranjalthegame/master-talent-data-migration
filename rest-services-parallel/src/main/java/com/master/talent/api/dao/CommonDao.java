package com.master.talent.api.dao;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.instamojo.wrapper.api.Instamojo;
import com.instamojo.wrapper.api.InstamojoImpl;
import com.instamojo.wrapper.exception.ConnectionException;
import com.instamojo.wrapper.exception.InvalidPaymentOrderException;
import com.instamojo.wrapper.model.PaymentOrder;
import com.instamojo.wrapper.response.CreatePaymentOrderResponse;
import com.master.talent.api.entity.ProfileViewEntity;
import com.master.talent.api.entity.ReviewEntity;
import com.master.talent.api.entity.UserCartEntity;
import com.master.talent.api.entity.UserEarningEntity;
import com.master.talent.api.entity.UserPurchasedServicesEntity;
import com.master.talent.api.entity.mapper.UserDaoMapper;
import com.master.talent.api.entity.repo.CompanyEntityRepo;
import com.master.talent.api.entity.repo.ProfielViewEntityRepo;
import com.master.talent.api.entity.repo.ReviewEntityRepo;
import com.master.talent.api.entity.repo.UserCartEntityRepo;
import com.master.talent.api.entity.repo.UserEarningEntityRepo;
import com.master.talent.api.entity.repo.UserPaidServicesEntityRepo;
import com.master.talent.api.entity.repo.UserPurchasedServicesEntityRepo;
import com.master.talent.api.exception.CouchbaseWriteFailureException;
import com.master.talent.api.exception.InvalidRequestException;
import com.master.talent.api.exception.ResourceNotFoundException;
import com.master.talent.api.exception.SearchException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.ChartViewObject;
import com.master.talent.api.models.Purchases;
import com.master.talent.api.models.Service;
import com.master.talent.api.utils.FileJsonProcessor;
import com.master.talent.api.utils.IndexUtility;
import com.master.talent.api.utils.SpringPropertiesUtil;


@Component
public class CommonDao {
	
	private final Logger logger = LoggerFactory.getLogger(CommonDao.class);
	
	@Autowired
	private	ProfielViewEntityRepo profielViewEntityRepo;
	
	@Autowired
	private	ReviewEntityRepo reviewEntityRepo;
	
	@Autowired
	private	CompanyEntityRepo companyEntityRepo;
	
	@Autowired
	private	UserPaidServicesEntityRepo userPaidServicesEntityRepo;
	
	@Autowired
	private	UserCartEntityRepo userCartEntityRepo;
	
	@Autowired
	private	UserEarningEntityRepo userEarningEntityRepo;
	
	@Autowired
	private	UserPurchasedServicesEntityRepo userPurchasedServicesEntityRepo;
		
	@Autowired
	private IndexUtility indexUtility;
	
	@Autowired
	private UserDaoMapper userDaoMapper;
	
	SolrClient solrClient = new HttpSolrClient.Builder(SpringPropertiesUtil.getProperty("solr.server.url")).build();	
	
	private HttpClient getHttpClient() {
	    CredentialsProvider provider = new BasicCredentialsProvider();
	    UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(
	    		SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
	    provider.setCredentials(AuthScope.ANY, credentials);
	    return HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
	}
	
	public UserCartEntity gerCartByUser(String username) {
		try{
			return userCartEntityRepo.findOne(UserCartEntity.ENTITY_NAME+username);
		}catch(Exception e){
			logger.debug("gerCartByUser", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_DEFAULT,e.getMessage(),e);
		}
	}
	
	public UserCartEntity addToCart(String username, Purchases purchases) {
		try{
			UserCartEntity userCartEntity = userCartEntityRepo.findOne(UserCartEntity.ENTITY_NAME+username);
			List<Purchases> purchasess = new ArrayList<Purchases>();
			if(userCartEntity == null){
				Purchases purchase = new Purchases();
				purchase.setCreated(new DateTime());
				purchase.setPurchaseId(UUID.randomUUID().toString());
				purchase.setServiceId(purchases.getServiceId());
				purchase.setPurchasedPrice(purchases.getPurchasedPrice());				
				purchasess.add(purchase);			
				
				userCartEntity = new UserCartEntity();
				userCartEntity.setUsername(username);
				userCartEntity.setPurchasess(purchasess);
			} else {
				Purchases purchase = new Purchases();
				purchase.setCreated(new DateTime());
				purchase.setPurchaseId(UUID.randomUUID().toString());
				purchase.setServiceId(purchases.getServiceId());
				purchase.setPurchasedPrice(purchases.getPurchasedPrice());	
				purchasess.add(purchase);	
				if(userCartEntity.getPurchasess() !=null){
					userCartEntity.getPurchasess().add(purchase);
				}else{
					purchasess.add(purchase);
					userCartEntity.setPurchasess(purchasess);
				}
			}
			userCartEntityRepo.save(userCartEntity);
			return userCartEntity;
		}catch(Exception e){
			logger.debug("gerCartByUser", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_DEFAULT,e.getMessage(),e);
		}
	}
	
	public UserPurchasedServicesEntity purchase(String username, String referral, List<Purchases> listPurchases) {
		try{
			UserCartEntity userCartEntity = userCartEntityRepo.findOne(UserCartEntity.ENTITY_NAME+username);
			UserPurchasedServicesEntity userPurchasedServicesEntity = userPurchasedServicesEntityRepo.findOne(UserPurchasedServicesEntity.ENTITY_NAME+username);
			List<Purchases> purchasess = new ArrayList<Purchases>();
			Service tempService = new Service();
			
			if(userPurchasedServicesEntity == null){
				userPurchasedServicesEntity = new UserPurchasedServicesEntity();
				userPurchasedServicesEntity.setUsername(username);
				
				Iterator<Purchases> listIterator = userCartEntity.getPurchasess().iterator();
		    	List<Service> list = FileJsonProcessor.getJsonAsJsonObject(SpringPropertiesUtil.getProperty("paid.services.fileName"));
		    	
		    	for (Purchases purchase : listPurchases) {
		    		while (listIterator.hasNext()) {
						Purchases purchases = listIterator.next();
					    if (purchases.getPurchaseId().equals(purchase.getPurchaseId())) {				    	
					    	//process the service 
					    	for (Service service : list) {
					    		if(service.getName().equalsIgnoreCase(purchases.getServiceId())){
									purchases.setCreated(new DateTime());
									Date date = addDays(new Date(), service.getValidDays());
									purchases.setExpireBy(new DateTime(date));
									purchases.setCount(service.getValidCount());
									purchases.setPercentage(service.getReferral());
									
									//process referral commission
									if(referral !=null){
										UserEarningEntity userEarningEntity = new UserEarningEntity();
										userEarningEntity.setId(UUID.randomUUID().toString());
										userEarningEntity.setUsername(referral);
										userEarningEntity.setCreated(DateTime.now());
										userEarningEntity.setReferrer(username);
										userEarningEntity.setServiceId(tempService.getName());
										userEarningEntity.setServiceName(tempService.getTitle());				
										if(tempService.getOfferCost() > 0){
											userEarningEntity.setCommisionAmount(Integer.toString((tempService.getOfferCost()*tempService.getReferral()/100)));					
										} else {
											userEarningEntity.setCommisionAmount(Integer.toString((tempService.getCost()*tempService.getReferral()/100)));	
										}
										userEarningEntityRepo.save(userEarningEntity);
									}
									userPurchasedServicesEntityRepo.save(userPurchasedServicesEntity);
					    		}
							}
					    	purchasess.add(purchases);
					    	listIterator.remove();
					    }
					}
					userPurchasedServicesEntity.setPurchased(purchasess);
		    	}				
			} else {
				purchasess = userPurchasedServicesEntity.getPurchased();
				Iterator<Purchases> listIterator = userCartEntity.getPurchasess().iterator();
		    	List<Service> list = FileJsonProcessor.getJsonAsJsonObject(SpringPropertiesUtil.getProperty("paid.services.fileName"));
				for (Purchases purchase : listPurchases) {
					while (listIterator.hasNext()) {
						Purchases purchases = listIterator.next();
					    if (purchases.getPurchaseId().equals(purchase.getPurchaseId())) {				    	
					    	//process the service 
					    	for (Service service : list) {
					    		if(service.getName().equalsIgnoreCase(purchases.getServiceId())){
									purchases.setCreated(new DateTime());
									Date date = addDays(new Date(), service.getValidDays());
									purchases.setExpireBy(new DateTime(date));
									purchases.setCount(service.getValidCount());

									//process referral commission
									if(referral !=null){
										UserEarningEntity userEarningEntity = new UserEarningEntity();
										userEarningEntity.setId(UUID.randomUUID().toString());
										userEarningEntity.setUsername(referral);
										userEarningEntity.setCreated(DateTime.now());
										userEarningEntity.setReferrer(username);
										userEarningEntity.setServiceId(tempService.getName());
										userEarningEntity.setServiceName(tempService.getTitle());				
										if(tempService.getOfferCost() > 0){
											userEarningEntity.setCommisionAmount(Integer.toString((tempService.getOfferCost()*tempService.getReferral()/100)));					
										} else {
											userEarningEntity.setCommisionAmount(Integer.toString((tempService.getCost()*tempService.getReferral()/100)));	
										}
										userEarningEntityRepo.save(userEarningEntity);
									}
									userPurchasedServicesEntityRepo.save(userPurchasedServicesEntity);
									
					    		}
							}
					    	purchasess.add(purchases);
					    	listIterator.remove();
					    }
					}
				}				
				userPurchasedServicesEntity.setPurchased(purchasess);		
			}
			
			
			userCartEntityRepo.save(userCartEntity);
			return userPurchasedServicesEntity;
		}catch(Exception e){
			logger.debug("gerCartByUser", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_DEFAULT,e.getMessage(),e);
		}
	}
	
	private static Date addDays(Date date, int days)
	{
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.add(Calendar.DATE, days); //minus number would decrement the days
	    return cal.getTime();
	}
	
	public UserPurchasedServicesEntity listPurchase(String username, String type) {
		try{
			UserPurchasedServicesEntity userPurchasedServicesEntity =null;
			if(type.equalsIgnoreCase("all")){
				userPurchasedServicesEntity =
					userPurchasedServicesEntityRepo.findOne(UserPurchasedServicesEntity.ENTITY_NAME+username);
			} else if(type.equalsIgnoreCase("active")){
				userPurchasedServicesEntity =
					userPurchasedServicesEntityRepo.findOne(UserPurchasedServicesEntity.ENTITY_NAME+username);
				
				Iterator<Purchases> listIterator = userPurchasedServicesEntity.getPurchased().iterator();
				while (listIterator.hasNext()) {
					Purchases purchases = listIterator.next();
					DateTime currenDate = new DateTime();		
					if (currenDate.isAfter(purchases.getExpireBy())) {
				    	listIterator.remove();
				    }
				}
			}
			return userPurchasedServicesEntity;
		}catch(Exception e){
			logger.debug("listPurchase", e);			
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_DEFAULT,e.getMessage(),e);
		}
	}
	
	public boolean showContact(String username) {
		try{
			UserPurchasedServicesEntity userPurchasedServicesEntity = userPurchasedServicesEntityRepo.findOne(UserPurchasedServicesEntity.ENTITY_NAME+username);
				
			Iterator<Purchases> listIterator = userPurchasedServicesEntity.getPurchased().iterator();
			while (listIterator.hasNext()) {
				Purchases purchases = listIterator.next();
				DateTime currenDate = new DateTime();		
				if(purchases.getServiceId().equals("SERVICE005")){
					if (currenDate.isAfter(purchases.getExpireBy())) {
				    	listIterator.remove();
				    } else {
				    	return true;
				    }		    		
		    	}				
			}
			return false;
		}catch(Exception e){
			return false;
		}
	}
	
	public UserCartEntity removeFromCart(String username, String purchaseId) {
		try{
			UserCartEntity userCartEntity = userCartEntityRepo.findOne(UserCartEntity.ENTITY_NAME+username);
			Iterator<Purchases> listIterator = userCartEntity.getPurchasess().iterator();
			while (listIterator.hasNext()) {
				Purchases purchases = listIterator.next();
			    if (purchases.getPurchaseId().equals(purchaseId)) {
			    	listIterator.remove();
			    }
			}
			userCartEntityRepo.save(userCartEntity);
			return userCartEntity;
		}catch(Exception e){
			logger.debug("removeFromCart", e);			
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_DEFAULT,e.getMessage(),e);
		}
	}
	
	public void createProfileView(String fromUser, String toUser) {
		try{
			
			if(fromUser.equalsIgnoreCase("UnKnown")){
				ProfileViewEntity profileViewEntity = new ProfileViewEntity();
				profileViewEntity.setId(UUID.randomUUID().toString());
				profileViewEntity.setViewee(toUser);
				profileViewEntity.setViewer(fromUser);
				profileViewEntity.setViewTime(DateTime.now());				
				indexView(profileViewEntity, "CREATE");				
				profielViewEntityRepo.save(profileViewEntity);
			} else {
				SolrDocument doc = validateViewExist(fromUser, toUser);
				if(doc!=null){
					ProfileViewEntity profileViewEntity =profielViewEntityRepo.findOne(doc.getFieldValue("id").toString());
					profileViewEntity.setViewTime(DateTime.now());
					indexView(profileViewEntity, "UPDATE");		
					profielViewEntityRepo.save(profileViewEntity);
				} else {
					ProfileViewEntity profileViewEntity = new ProfileViewEntity();
					profileViewEntity.setId(UUID.randomUUID().toString());
					profileViewEntity.setViewee(toUser);
					profileViewEntity.setViewer(fromUser);
					profileViewEntity.setViewTime(DateTime.now());				
					indexView(profileViewEntity, "CREATE");				
					profielViewEntityRepo.save(profileViewEntity);
				}
			}	
			
		} catch(Exception e){
			logger.debug("createProfileView", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_ITEM_INDEX_FAIL,e.getMessage(),e);
		}	
	}

	private SolrDocument validateViewExist(String fromUser, String toUser) throws SolrServerException, IOException {
		SolrQuery viewQuery = new SolrQuery();
		viewQuery.setQuery("viewee:"+fromUser);
		viewQuery.setFilterQueries("viewed:"+toUser);
		
		QueryRequest req = new QueryRequest(viewQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.view.collection")); 
		
		if (!rsp.getResults().isEmpty()){
			return rsp.getResults().get(0);
		} else {
			return null;
		}
	}

	private void indexView(ProfileViewEntity profileViewEntity, String type) throws SolrServerException, IOException {
		SolrInputDocument doc = new SolrInputDocument();
		doc.addField("id", profileViewEntity.getId());
		if(type.equalsIgnoreCase("CREATE")){
			doc.addField("viewed", profileViewEntity.getViewee());
		    doc.addField("viewee", profileViewEntity.getViewer());
		    doc.addField("time", new Date());
		}
		if(type.equalsIgnoreCase("UPDATE")){
			Map<String, String> partialUpdate = new HashMap<String, String>();
			partialUpdate.put("set", ZonedDateTime.now( ZoneOffset.UTC ).toString());
			doc.addField("time", partialUpdate);
		}
		
		UpdateRequest req = new UpdateRequest(); 
	    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		req.add(doc); 
		req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
		req.process(solrClient, SpringPropertiesUtil.getProperty("solr.view.collection")); 
	}

	public SolrDocumentList getProfileViews(String toUser, int from, int to) {
		try{
			SolrQuery viewQuery = new SolrQuery();
			viewQuery.setQuery("viewed:"+toUser);
			viewQuery.addSort("time", ORDER.desc);
			viewQuery.setStart(from);
			viewQuery.setRows(to);
			
			QueryRequest req = new QueryRequest(viewQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.view.collection")); 
			
			return rsp.getResults();
		}catch(Exception e){
			logger.debug("getProfileViews", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
	}

	public ChartViewObject getChart(String username, String type) {
		ChartViewObject chartViewObject = new ChartViewObject();
		try{
			if(type.equalsIgnoreCase("week")){
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss'Z'");		
		        String fromDate = dateFormat.format(new Date());	        
		        Calendar cal = Calendar.getInstance();
		        cal.add(Calendar.DATE, -7);
		        Date todate1 = cal.getTime();    
		        String toDate = dateFormat.format(todate1);
				
		        SolrDocumentList list = queryViewSolr(username, fromDate, toDate);
		        int unknownCount =	queryViewSolrAndUnknownCount(username, fromDate, toDate);
		        
		        Map<String, Integer> processedList = processDate(list);		        
		        chartViewObject.setList(processedList);
		        chartViewObject.setUserViewCount(list.size());
		        chartViewObject.setUnknownViewCount(unknownCount);
		        
		        return chartViewObject;
			}
			if(type.equalsIgnoreCase("month")){
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss'Z'");		
		        String fromDate = dateFormat.format(new Date());	        
		        Calendar cal = Calendar.getInstance();
		        cal.add(Calendar.DATE, -31);
		        Date todate1 = cal.getTime();    
		        String toDate = dateFormat.format(todate1);
		        
		        SolrDocumentList list = queryViewSolr(username, fromDate, toDate);
		        int unknownCount =	queryViewSolrAndUnknownCount(username, fromDate, toDate);
		        
		        Map<String, Integer> processedList = processDate(list);		        
		        chartViewObject.setList(processedList);
		        chartViewObject.setUserViewCount(list.size());
		        chartViewObject.setUnknownViewCount(unknownCount);
		        
		        return chartViewObject;
			}			
		} catch(Exception e){
			logger.debug("getProfileViews", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_SEARCH_ITEMS_FAIL,e.getMessage(),e);
		}
		return null;		
	}

	private Map<String, Integer> processDate(SolrDocumentList list) {
		Map<String, Integer> chart = new HashMap<String, Integer>();

		Integer count =0;
		String key = null;
		for (SolrDocument doc : list) {	
			if(key == null){
				key = (String) doc.getFieldValue("time").toString().subSequence(0, 10);
				count++;
			} else {
				if(doc.getFieldValue("time").toString().contains(key)){
					count++;
				} else {
					chart.put(key, count);
					key = (String) doc.getFieldValue("time").toString().subSequence(0, 10);
					count =1;
				}				
			}
			chart.put(key, count);
		}	
		return chart;
	}

	private SolrDocumentList queryViewSolr(String username, String fromDate, String toDate) throws SolrServerException, IOException {
		SolrQuery viewQuery = new SolrQuery();
		viewQuery.setQuery("viewed:"+username+" AND -viewee:unknown");
		viewQuery.setFilterQueries("time:["+toDate+" TO " + fromDate + "]");
		viewQuery.addSort("time", ORDER.asc);
		
		QueryRequest req = new QueryRequest(viewQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.view.collection"));
		
		return rsp.getResults();
	}
	
	
	private int queryViewSolrAndUnknownCount(String username, String fromDate, String toDate) throws SolrServerException, IOException {
		SolrQuery viewQuery = new SolrQuery();
		viewQuery.setQuery("viewed:"+username+" AND viewee:unknown");
		viewQuery.setFilterQueries("time:["+toDate+" TO " + fromDate + "]");
		viewQuery.addSort("time", ORDER.asc);
		
		QueryRequest req = new QueryRequest(viewQuery);
		req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.view.collection"));
		
		return rsp.getResults().size();
	}

	public ReviewEntity createReview(ReviewEntity reviewEntity, String username) {
		try{
			reviewEntity.setId(UUID.randomUUID().toString());
			reviewEntity.setReviewOwner(username);
			reviewEntity.setCreated(new DateTime());
			reviewEntity.setPublish(false);			
			reviewEntityRepo.save(reviewEntity);			
			return reviewEntity;
		}catch(Exception e){
			logger.debug("createUserPreSignUpEntity", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_DEFAULT,e.getMessage(),e);
		}
	}
	
	public ReviewEntity updateReview(ReviewEntity reviewEntity, String username) {
		try{
			ReviewEntity dbObj = reviewEntityRepo.findOne(reviewEntity.getId());
			dbObj = userDaoMapper.updateReview(dbObj, reviewEntity);
			reviewEntity = reviewEntityRepo.save(dbObj);	
			return reviewEntity;
		}catch(Exception e){
			logger.debug("createUserPreSignUpEntity", e);
			throw new CouchbaseWriteFailureException(ExceptionCodes.CBECodes.CB_DEFAULT,e.getMessage(),e);
		}
	}

	public List<ReviewEntity> getAllReviews(String loggedInuser, String requested, String type) {
		try{
			if(type.equalsIgnoreCase("company")){
				boolean status = indexUtility.getReturnTypeDefination(loggedInuser, requested);
				if(status)
					return reviewEntityRepo.getByReviewId(requested);	
				else
					return reviewEntityRepo.getByPublish(requested);	
			} else if(type.equalsIgnoreCase("user")){
				if(loggedInuser.equals(requested))
					return reviewEntityRepo.getByReviewId(requested);	
				else
					return reviewEntityRepo.getByPublish(requested);	
			} else{
				throw new InvalidRequestException(ExceptionCodes.IRECodes.IRE_DEFAULT,"InvalidRequestException");
			}			
		}catch(Exception e){
			logger.debug("getAllReviews", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_COMMENT_DELETE_FAIL,e.getMessage(),e);
		}
	}
	
	private CreatePaymentOrderResponse generatePaymentOrder(String loggeinUser, String amount){
		try{			
			PaymentOrder order = new PaymentOrder();
			order.setName(loggeinUser);
			order.setEmail("info.mastertalent@gmail.com");
			order.setPhone("1231231231");
			order.setCurrency("INR");
			order.setAmount(Double.parseDouble(amount));
			order.setDescription("Service purchase at Master Talent.");
			order.setRedirectUrl(SpringPropertiesUtil.getProperty("redirect.url"));
			order.setWebhookUrl("");
			order.setTransactionId(UUID.randomUUID().toString());

			Instamojo api = null;
			try {
				// gets the reference to the instamojo api
				api = InstamojoImpl.getApi(SpringPropertiesUtil.getProperty("instamojo.apiKey"),
						SpringPropertiesUtil.getProperty("instamojo.authToken"),
						SpringPropertiesUtil.getProperty("instamojo.endpoint"),
						SpringPropertiesUtil.getProperty("instamojo.auth.endpoint"));
			} catch (ConnectionException e) {
				logger.debug("getAllReviews", e);
			}
			
			boolean isOrderValid = order.validate();
			CreatePaymentOrderResponse createPaymentOrderResponse = null;
			if (isOrderValid) {
				try {
					createPaymentOrderResponse = api.createNewPaymentOrder(order);
				    // print the status of the payment order.
				} catch (InvalidPaymentOrderException e) {
					logger.debug("getAllReviews", e);
				    if (order.isTransactionIdInvalid()) {
				    	logger.debug("Transaction id is invalid. This is mostly due to duplicate  transaction id.", e);
				    }
				    if (order.isCurrencyInvalid()) {
				    	logger.debug("Currency is invalid.", e);
				    }
				} catch (ConnectionException e) {
					logger.debug("getAllReviews", e);
			    }
			} else {
				// inform validation errors to the user.
				if (order.isTransactionIdInvalid()) {
				    System.out.println("Transaction id is invalid.");
				}
				if (order.isAmountInvalid()) {
				  System.out.println("Amount can not be less than 9.00.");
				}
				if (order.isCurrencyInvalid()) {
				  System.out.println("Please provide the currency.");
				}
				if (order.isDescriptionInvalid()) {
				  System.out.println("Description can not be greater than 255 characters.");
			        }
				if (order.isEmailInvalid()) {
				  System.out.println("Please provide valid Email Address.");
				}
				if (order.isNameInvalid()) {
				  System.out.println("Name can not be greater than 100 characters.");
				}
				if (order.isPhoneInvalid()) {
				  System.out.println("Phone is invalid.");
				}
				if (order.isRedirectUrlInvalid()) {
				  System.out.println("Please provide valid Redirect url.");
				}
				if (order.isWebhookInvalid()) {
			      System.out.println("Provide a valid webhook url");
			    }
			}
		return createPaymentOrderResponse;
		}catch(Exception e){
			logger.debug("getAllReviews", e);
			throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_COMMENT_DELETE_FAIL,e.getMessage(),e);
		}
	}
	
	public CreatePaymentOrderResponse getPaymentOrder(UserCartEntity userCartEntity) {	
		int amount = 0;
		List<Service> list = FileJsonProcessor.getJsonAsJsonObject(SpringPropertiesUtil.getProperty("paid.services.fileName"));
		if(userCartEntity.getPurchasess() != null){
			if(!userCartEntity.getPurchasess().isEmpty()){
				for (Purchases purchase : userCartEntity.getPurchasess()) {
					for (Service service : list) {
			    		if(service.getName().equalsIgnoreCase(purchase.getServiceId())){
			    			amount = amount+service.getOfferCost();
			    		}
					}
				}
			} else {
				throw new ResourceNotFoundException(ExceptionCodes.RECodes.RE_ITEM_NOT_FOUND);
			}
			
		}
		return generatePaymentOrder(userCartEntity.getUsername(), Integer.toString(amount));
	}

}
