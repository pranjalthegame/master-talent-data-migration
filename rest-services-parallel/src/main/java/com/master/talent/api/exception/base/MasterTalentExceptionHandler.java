/**
 * 
 */
package com.master.talent.api.exception.base;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.master.talent.api.ExceptionHandlerMap;
import com.master.talent.api.exception.UnAuthorisedException;
import com.master.talent.api.utils.JsonUtils;

/**
 * @author pranjal.sinha
 *
 */
public class MasterTalentExceptionHandler implements ExceptionMapper<MasterTalentException> {

	private static final Logger LOG = LoggerFactory.getLogger(MasterTalentExceptionHandler.class);
	
	@Override
	public Response toResponse(MasterTalentException exception) {
		logException(exception);
		return Response.ok()
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 	            .header("exception", exception.getMessage())
				.entity(loadMessage(exception))
				.build();
	}
	
	private void logException(MasterTalentException exception){
		LOG.error("Exception Intercepted...Logging trace now");
		LOG.error("Exception: "+exception.getClass().getCanonicalName());
		LOG.error("Exception Trace: "+writeTraceAsString(exception));
//		System.out.println("Exception Intercepted...Logging trace now");
//		System.out.println("Exception: "+exception.getClass().getCanonicalName());
//		System.out.println("Trace: "+writeTraceAsString(exception));
	}
	
	private String writeTraceAsString(MasterTalentException exception){
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		try{
			exception.printStackTrace(printWriter);
			return stringWriter.toString();
		}finally{
			printWriter.close();
			try {
				stringWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private String loadMessage(MasterTalentException baseException){
		String message = null;
		try{
			Object customDecorator = locateCustomDecorator(baseException);
			if(customDecorator!=null){
				LOG.info("CustomDecorator found for exception!!!!");
				LOG.info("CustomDecorator is: ",customDecorator.getClass().getCanonicalName());
				Method m = customDecorator.getClass().getDeclaredMethod("decorate", new Class[] {MasterTalentException.class});
				message = m.invoke(customDecorator, new Object[]{baseException}).toString();
				LOG.info("Custom Exception Message: ",message);
			}
		}catch(Exception e){
			LOG.error("Exception encountered in loading custom message: {}",e);
		}return getJsonMessage(message,baseException);
		
	}
	
	private String getJsonMessage(String message,MasterTalentException baseException){
		ExceptionResponse response = new ExceptionResponse();
		response.setError(message);
		//setting redirection as true if unauthorized exception is received!!
		if(baseException instanceof UnAuthorisedException){
			response.setRedirect(true);
		}return JsonUtils.toJson(response);
	}
	
	private Object locateCustomDecorator(MasterTalentException baseException){
		try{
			String instance = baseException.getClass().getCanonicalName();
			LOG.info("Searching appropriate handler for exception: "+instance);
			Map<String,Object> handlerMap = ExceptionHandlerMap.getHandlerMap();
			if(handlerMap!=null){
				return handlerMap.get(instance);
			}
		}catch(Exception e){
			e.printStackTrace();
		}return null;
	}

	
}
