package com.master.talent.api.models;

import org.joda.time.DateTime;


public class Friends {

	private String friendName;
	private boolean requestor ;
	private DateTime requestedTime;
	private DateTime acceptedTime;
	private boolean isActive;
	public String getFriendName() {
		return friendName;
	}
	public void setFriendName(String friendName) {
		this.friendName = friendName;
	}
	public boolean isRequestor() {
		return requestor;
	}
	public void setRequestor(boolean requestor) {
		this.requestor = requestor;
	}
	public DateTime getRequestedTime() {
		return requestedTime;
	}
	public void setRequestedTime(DateTime requestedTime) {
		this.requestedTime = requestedTime;
	}
	public DateTime getAcceptedTime() {
		return acceptedTime;
	}
	public void setAcceptedTime(DateTime acceptedTime) {
		this.acceptedTime = acceptedTime;
	}
	public boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}
	@Override
	public String toString() {
		return "Friends [friendName=" + friendName + ", requestor=" + requestor
				+ ", requestedTime=" + requestedTime + ", acceptedTime="
				+ acceptedTime + ", isActive=" + isActive + "]";
	}
	
}
