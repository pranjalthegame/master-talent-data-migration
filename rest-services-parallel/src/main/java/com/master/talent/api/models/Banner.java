package com.master.talent.api.models;

public class Banner {	

    private boolean defaultImage;
    private String defaultPath;
    
	public boolean isDefaultImage() {
		return defaultImage;
	}
	public void setDefaultImage(boolean defaultImage) {
		this.defaultImage = defaultImage;
	}
	public String getDefaultPath() {
		return defaultPath;
	}
	public void setDefaultPath(String defaultPath) {
		this.defaultPath = defaultPath;
	}
	@Override
	public String toString() {
		return "Banner [defaultImage=" + defaultImage + ", defaultPath="
				+ defaultPath + "]";
	}
       
}
