/**
 * 
 */
package com.master.talent.data.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.master.talent.data.constants.AppConstants;
import com.master.talent.data.model.PhoneNumber;

/**
 * @author pranjal.sinha
 *
 */
public class CommonUtils {

	private static final Logger LOG = LoggerFactory.getLogger(CommonUtils.class);
	
	public static <T> List<List<T>> createBatches(List<T> records, int threads) {
    	System.out.println(( "Total Records size: " + records.size()));
           int size = (int) Math.ceil((double) records.size() / (double) threads);
           System.out.println(( "Total Batch size: " + size));
           int batchsize = (int) size;
           List<List<T>> batch = new ArrayList<List<T>>();
           if (batchsize == 1) {
                 batch.add(records);
           } else {
                 int remainingElements = records.size();
                 int startIndex = 0;
                 int endIndex = batchsize;
                 do {
                        List<T> subList = records.subList(startIndex, endIndex);
                        batch.add(subList);
                        startIndex = endIndex;
                        if (remainingElements - batchsize >= batchsize) {
                               endIndex = startIndex + batchsize;
                        } else {
					endIndex = startIndex + remainingElements - subList.size();
                        }
                        remainingElements -= subList.size();
                 } while (remainingElements > 0);
           }
           return batch;
    }
	
	public static List<PhoneNumber> getPhones(String source){
		List<PhoneNumber> phones = new ArrayList<>();
		String[] phoneArr = source.split(",");
		for(int i=0;i<phoneArr.length;i++){
			String countryCode = "+"+phoneArr[i].substring(0, 2);
			String number = phoneArr[i].substring(2);
			PhoneNumber phone = new PhoneNumber();
			phone.setCountryCode(countryCode);
			phone.setPhoneNumber(number);
			phones.add(phone);
		}return phones;
	}
	
	public static List<String> getSplitString(String source,String splitBy){
		String[] splitArr = source.split(splitBy);
		return Arrays.asList(splitArr);
	}
	
	public static void pollForResult(ExecutorCompletionService<Map<String,String>> completionService, int loopSize,String type){
		Map<String,String> discussionMap = new ConcurrentHashMap<>();
		for(int i=1;i<=loopSize;i++){
			try{
				Future<Map<String,String>> result = completionService.take();
				if(result.isDone()){
					Map<String,String> output = result.get();
					if(type.equalsIgnoreCase(AppConstants.TYPE_DISCUSSION)){
						discussionMap.putAll(output);
					}
					logOutput(output);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}if(!discussionMap.isEmpty()){
			writeDiscussionOutput(discussionMap);
		}
	}
	
	private static void writeDiscussionOutput(Map<String, String> discussionMap) {
		String outputFilePath = AppConstants.FILE_RELATIVE_PATH + AppConstants.DISCUSSION_OUTPUT_FILE;
		try{
			writeExcel(discussionMap, outputFilePath);
		}catch(IOException e){
			LOG.error("Exception encountered in writing discussion feed output: {}",e);
		}
		
	}
	
	private static void writeExcel(Map<String,String> valueMap, String excelFilePath) throws IOException {
	    Workbook workbook = new HSSFWorkbook();
	    Sheet sheet = workbook.createSheet();
	    int rowCount = 0;
	    Set<Map.Entry<String, String>> entrySet = valueMap.entrySet();
	    Iterator<Map.Entry<String, String>> itr = entrySet.iterator();
	    writeHeaders(sheet);
	    while(itr.hasNext()){
	    	Row row = sheet.createRow(++rowCount);
	    	Map.Entry<String, String> pair = itr.next();
	    	writeValues(pair, row);
	    }
	    try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
	        workbook.write(outputStream);
	    }
	}
	
	private static void writeValues(Map.Entry<String,String> pair, Row row) {
	    Cell cell = row.createCell(0);
	    cell.setCellValue(pair.getKey());
	    cell = row.createCell(1);
	    cell.setCellValue(pair.getValue());
	}
	
	private static void writeHeaders(Sheet sheet){
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("ItemId");
		cell = row.createCell(1);
		cell.setCellValue("Owner");
	}

	private static void logOutput(Map<String,String> output){
		Set<Map.Entry<String,String>> entrySet = output.entrySet();
		Iterator<Map.Entry<String,String>> itr = entrySet.iterator();
		while(itr.hasNext()){
			Map.Entry<String,String> pair = itr.next();
			LOG.info("["+pair.getKey()+","+pair.getValue()+"]");
		}
	}
}
