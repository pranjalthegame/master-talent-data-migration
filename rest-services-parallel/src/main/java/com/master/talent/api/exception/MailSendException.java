package com.master.talent.api.exception;

import com.master.talent.api.exception.base.MasterTalentException;

public class MailSendException extends MasterTalentException {
   
	private static final long serialVersionUID = 1L;
	
	public MailSendException() {
        super();
    }
    public MailSendException(String message, Throwable cause) {
        super(message, cause);
    }
    public MailSendException(String message) {
        super(message);
    }
    public MailSendException(Throwable cause) {
        super(cause);
    }
	public MailSendException(String exceptionCode, String message, Throwable cause) {
		super(exceptionCode, message, cause);
	}
	public MailSendException(String exceptionCode, String message) {
		super(exceptionCode, message);
	}
    
    
}