package com.master.talent.api.service.impl;

import java.io.File;

import javax.activation.DataHandler;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.master.talent.api.service.ImageProcessor;
import com.master.talent.api.utils.ProfileImageProcessor;
import com.master.talent.api.utils.SpringPropertiesUtil;

@CrossOriginResourceSharing(
        allowOrigins = {
           "*"
        }, 
        allowCredentials = true, 
        maxAge = 1, 
        allowHeaders = {
           "MTUser"
        }
)
public class ImageProcessorImpl implements ImageProcessor {

	private final Gson serializer = new Gson();

	private final Logger logger = LoggerFactory.getLogger(ImageProcessorImpl.class);

	@Autowired
	private ProfileImageProcessor profileImageProcessor;
	
	@Autowired
	private  RequestValidator validator;
	
	public void setRepo(ProfileImageProcessor profileImageProcessor) {
		this.profileImageProcessor = profileImageProcessor;
	}
	
	@Override
	public Response uploadProfile(HttpHeaders headers, Attachment attachments, HttpServletRequest request, String username) {
		 validator.validateBefore(headers);
		 DataHandler dataHandler = attachments.getDataHandler();         
         
		 // process rendition's
         profileImageProcessor.generateProfileImage(dataHandler, username);
         return Response.ok("{ \"success\" : \"true\"}")
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}
	
	@Override
	public Response uploadBanner(HttpHeaders headers, Attachment attachments, HttpServletRequest request, String username) {
		 validator.validateBefore(headers);
		 DataHandler dataHandler = attachments.getDataHandler();
		 
         // process rendition's
         profileImageProcessor.generateBannerImage(dataHandler, username);
         return Response.ok("{ \"success\" : \"true\"}")
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}
	
	@Override
	public Response uploadItem(HttpHeaders headers, Attachment attachments) {
		 validator.validateBefore(headers);
		 DataHandler dataHandler = attachments.getDataHandler();
         // get filename to be uploaded
         MultivaluedMap<String, String> multivaluedMap = attachments.getHeaders();
         String fileName = getFileName(multivaluedMap);
         
         // process rendition's
         String path =profileImageProcessor.generateItem(dataHandler);
         return Response.ok("{ \"success\" :\""+path+"\"}")
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}
	
	/**
    *
    * @param multivaluedMap
    * @return
    */
   private String getFileName(MultivaluedMap<String, String> multivaluedMap) {

       String[] contentDisposition = multivaluedMap.getFirst("Content-Disposition").split(";");
       for (String filename : contentDisposition) {

           if ((filename.trim().startsWith("filename"))) {
               String[] name = filename.split("=");
               String exactFileName = name[1].trim().replaceAll("\"", "");
               return exactFileName;
           }
       }
       return "unknownFile";
   }

	@Override
	public Response deleteItemImage(HttpHeaders headers, String itemPath) {
		try{
			FileUtils.forceDelete(new File(SpringPropertiesUtil.getProperty("image.path")+itemPath));
			return Response.ok("{ \"success\" :\"Files deleted\"}")
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		} catch(Exception e){
			logger.debug("Could not delete the folder");
		}
		return null;
	}
	
}
