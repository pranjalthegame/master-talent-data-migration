package com.master.talent.api;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;
import org.springframework.data.couchbase.repository.config.RepositoryOperationsMapping;

import com.couchbase.client.java.Bucket;
import com.master.talent.api.entity.ApplyEntity;
import com.master.talent.api.entity.ChatEntity;
import com.master.talent.api.entity.ChatGroupEntity;
import com.master.talent.api.entity.CommentEntity;
import com.master.talent.api.entity.ItemEntity;
import com.master.talent.api.entity.LikeEntity;
import com.master.talent.api.entity.ProfileViewEntity;
import com.master.talent.api.entity.ShortListEntity;
import com.master.talent.api.entity.SessionEntity;

@Configuration
@EnableCouchbaseRepositories(basePackages={"com.master.talent.api.entity.repo"})
public class MyCouchbaseConfig extends AbstractCouchbaseConfiguration {
 
    @Override
    protected List<String> getBootstrapHosts() {
        return Arrays.asList("couchdb-pranjal.cloudapp.net");
    }
 
    @Override
    protected String getBucketName() {
        return "user";
    }
 
    @Override
    protected String getBucketPassword() {
        return "";
    }

    
    @Bean
    public Bucket itemBucket() throws Exception {
        return couchbaseCluster().openBucket("item", "");
    }
    
    @Bean
    public Bucket sessionBucket() throws Exception {
        return couchbaseCluster().openBucket("session", "");
    }
    
    @Bean
    public Bucket chatBucket() throws Exception {
        return couchbaseCluster().openBucket("chat", "");
    }
    
    @Bean
    public Bucket viewBucket() throws Exception {
        return couchbaseCluster().openBucket("view", "");
    }
    
    @Bean
    public CouchbaseTemplate sessionTemplate() throws Exception {
        CouchbaseTemplate template = new CouchbaseTemplate(
          couchbaseClusterInfo(), sessionBucket(),
          mappingCouchbaseConverter(), translationService());
        template.setDefaultConsistency(getDefaultConsistency());
        return template;
    }
    
    @Bean
    public CouchbaseTemplate itemTemplate() throws Exception {
        CouchbaseTemplate template = new CouchbaseTemplate(
          couchbaseClusterInfo(), itemBucket(),
          mappingCouchbaseConverter(), translationService());
        template.setDefaultConsistency(getDefaultConsistency());
        return template;
    }
    
    @Bean
    public CouchbaseTemplate chatTemplate() throws Exception {
        CouchbaseTemplate template = new CouchbaseTemplate(
          couchbaseClusterInfo(), chatBucket(),
          mappingCouchbaseConverter(), translationService());
        template.setDefaultConsistency(getDefaultConsistency());
        return template;
    }
    
    @Bean
    public CouchbaseTemplate viewTemplate() throws Exception {
        CouchbaseTemplate template = new CouchbaseTemplate(
          couchbaseClusterInfo(), viewBucket(),
          mappingCouchbaseConverter(), translationService());
        template.setDefaultConsistency(getDefaultConsistency());
        return template;
    }
    
    @Override
    public void configureRepositoryOperationsMapping(
      RepositoryOperationsMapping baseMapping) {
        try {
            baseMapping.mapEntity(ItemEntity.class, itemTemplate());
            baseMapping.mapEntity(CommentEntity.class, itemTemplate());
            baseMapping.mapEntity(LikeEntity.class, itemTemplate());
            baseMapping.mapEntity(ApplyEntity.class, itemTemplate());
            baseMapping.mapEntity(ShortListEntity.class, itemTemplate());
            baseMapping.mapEntity(ChatEntity.class, chatTemplate());
            baseMapping.mapEntity(ChatGroupEntity.class, chatTemplate());
            baseMapping.mapEntity(ProfileViewEntity.class, viewTemplate());
            baseMapping.mapEntity(SessionEntity.class, sessionTemplate());
        } catch (Exception e) {
            //custom Exception handling
        }
    }
}
