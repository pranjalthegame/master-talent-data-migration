package com.master.talent.api.exception;

import com.master.talent.api.exception.base.MasterTalentException;

public class CouchbaseWriteFailureException extends MasterTalentException {
   
	private static final long serialVersionUID = -2859292084622222403L;
	
	public CouchbaseWriteFailureException() {
        super();
    }
    public CouchbaseWriteFailureException(String message, Throwable cause) {
        super(message, cause);
    }
    public CouchbaseWriteFailureException(String message) {
        super(message);
    }
    public CouchbaseWriteFailureException(Throwable cause) {
        super(cause);
    }
    
    public CouchbaseWriteFailureException(String exceptionCode, String message, Throwable cause) {
		super(exceptionCode, message, cause);
	}
	public CouchbaseWriteFailureException(String exceptionCode, String message) {
		super(exceptionCode, message);
	}
}