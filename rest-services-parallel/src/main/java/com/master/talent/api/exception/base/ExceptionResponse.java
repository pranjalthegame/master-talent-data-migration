/**
 * 
 */
package com.master.talent.api.exception.base;

/**
 * @author pranjal.sinha
 *
 */
public class ExceptionResponse {

	private String error;
	private boolean redirect;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isRedirect() {
		return redirect;
	}

	public void setRedirect(boolean redirect) {
		this.redirect = redirect;
	}
	
	
	
	
}
