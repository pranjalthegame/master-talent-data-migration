/**
 * 
 */
package com.master.talent.api;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import com.master.talent.api.annotation.Handles;

/**
 * @author pranjal.sinha
 *
 */
@Configuration
public class ExceptionConfig {

	
	@Bean
	public ExceptionHandlerMap getExceptionHandlerMap() throws Exception{
		ExceptionHandlerMap handlerMap = new ExceptionHandlerMap();
		handlerMap.setHandlerMap(init());
		return handlerMap;
	}
	
	private Map<String,Object> init() throws Exception {
		System.out.println("Initializing Exception handler map!!");
		Map<String,Object> map = new HashMap<>();
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
		scanner.addIncludeFilter(new AnnotationTypeFilter(Handles.class));
		Set<BeanDefinition> set = scanner.findCandidateComponents("com.master.talent.api.exception.decorator");
		System.out.println("Package set size is: "+set.size());
		for(BeanDefinition bd: set){
			System.out.println("Class name: "+bd.getBeanClassName());
			Object obj = Class.forName(bd.getBeanClassName()).newInstance();
			String value = obj.getClass().getAnnotation(Handles.class).value();
			System.out.println("Annotation Value for this class: "+value);
			map.put(value, obj);
		}return map;
	}
}
