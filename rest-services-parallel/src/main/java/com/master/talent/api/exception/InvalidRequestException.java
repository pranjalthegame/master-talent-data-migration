package com.master.talent.api.exception;

import com.master.talent.api.exception.base.MasterTalentException;

public class InvalidRequestException extends MasterTalentException {
   
	private static final long serialVersionUID = 1L;
	
	public InvalidRequestException() {
        super();
    }
    public InvalidRequestException(String message, Throwable cause) {
        super(message, cause);
    }
    public InvalidRequestException(String message) {
        super(message);
    }
    public InvalidRequestException(Throwable cause) {
        super(cause);
    }
	public InvalidRequestException(String exceptionCode, String message, Throwable cause) {
		super(exceptionCode, message, cause);
	}
	public InvalidRequestException(String exceptionCode, String message) {
		super(exceptionCode, message);
	}
    
}