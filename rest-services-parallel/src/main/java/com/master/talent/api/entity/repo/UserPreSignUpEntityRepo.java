package com.master.talent.api.entity.repo;


import java.util.List;

import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.UserPreSignUpEntity;

@Repository
public interface UserPreSignUpEntityRepo extends CouchbaseRepository<UserPreSignUpEntity, String>
{
	@View(designDocument = "userEntity", viewName = "byEmailAddress")
	List<UserPreSignUpEntity> findByEmailAddress(String emailAddress);
	
	@View(designDocument = "userEntity", viewName = "byUserType")
	List<UserPreSignUpEntity> findByUserType(String userType);
}