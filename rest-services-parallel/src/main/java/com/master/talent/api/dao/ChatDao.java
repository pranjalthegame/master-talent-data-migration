package com.master.talent.api.dao;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.master.talent.api.entity.ChatEntity;
import com.master.talent.api.entity.ChatGroupEntity;
import com.master.talent.api.entity.repo.ChatEntityRepo;
import com.master.talent.api.entity.repo.ChatGroupEntityRepo;
import com.master.talent.api.exception.InvalidRequestException;
import com.master.talent.api.exception.SearchException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.Chat;
import com.master.talent.api.models.ChatListResponse;
import com.master.talent.api.models.ChatRequest;
import com.master.talent.api.models.ChatUser;
import com.master.talent.api.utils.SpringPropertiesUtil;

@Component
public class ChatDao {
	
	private final Logger logger = LoggerFactory.getLogger(ChatDao.class);
	
	SolrClient solrClient = new HttpSolrClient.Builder(SpringPropertiesUtil.getProperty("solr.server.url")).build();	
	
	@Autowired
	private ChatEntityRepo chatEntityRepo;
	
	@Autowired
	private ChatGroupEntityRepo chatGroupEntityRepo;
	
	@Autowired
	private SearchDao searchDao;

	public Chat createMessage(String fromUser, String toUsername, String message){
		boolean flag = false;
		ChatEntity chatEntity = chatEntityRepo.findOne(fromUser+"%~%"+toUsername);
		if(chatEntity ==null){
			chatEntity = chatEntityRepo.findOne(toUsername+"%~%"+fromUser);
			if(chatEntity==null){
				chatEntity = new ChatEntity();
				flag = true;
				chatEntity.setId(fromUser+"%~%"+toUsername);
				chatEntity.setCreated(DateTime.now());
				chatEntity.setUpdated(DateTime.now());
				chatEntity.setChat(new ArrayList<Chat>());
			}
		}		
		if(chatEntity!=null){	
			Chat chat = new Chat();
			chat.setChatId(UUID.randomUUID().toString());
			chat.setChatOwner(fromUser);
			chat.setMesage(message);
			chat.setCreated(DateTime.now());
			chatEntity.setUpdated(DateTime.now());
			chatEntity.getChat().add(chat);
			//update chat indexes
			if(flag)
				indexChatToSolr(chatEntity, fromUser, toUsername, "CREATE");
			else 
				indexChatToSolr(chatEntity, fromUser, toUsername, "CREATEUPDATE");
			chatEntityRepo.save(chatEntity);
			return chat;
		}
		return null;
	}
	
	private void indexChatToSolr(ChatEntity chatEntity, String fromUser, String toUsername, String type) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", chatEntity.getId());
			if(type.equalsIgnoreCase("CREATE")){
				doc.addField("created", new Date());
			    doc.addField("type", "single");
			    doc.addField("read", fromUser);
			    doc.addField("updated", new Date());
			    List<String> users = new ArrayList<String>();
			    users.add(fromUser);
			    users.add(toUsername);
			    doc.addField("userList", users);
			}
			if(type.equalsIgnoreCase("CREATEUPDATE")){
				Map<String, String> partialDateUpdate = new HashMap<String, String>();
		    	partialDateUpdate.put("set", ZonedDateTime.now( ZoneOffset.UTC ).toString());
		    	doc.addField("updated", partialDateUpdate);
		    	Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("set", fromUser);
			    doc.addField("read", partialUpdate);
			}
		    if(type.equalsIgnoreCase("UPDATE")){
		    	Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("add", fromUser);
		    	doc.addField("read", partialUpdate);
		    }
		    
		    UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		    req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.chat.collection")); 
			
		} catch(Exception e){
			System.out.println("ERROR" + e);
			throw new SearchException(e.getMessage(),e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ChatListResponse> getAllChats(String username) {
		try{
			SolrQuery chatQuery = new SolrQuery();
			chatQuery.setQuery("id:*");
			chatQuery.setFilterQueries("userList:"+username);
			chatQuery.addSort("updated", ORDER.desc);
			
			QueryRequest req = new QueryRequest(chatQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.chat.collection")); 
			
			List<ChatListResponse> chatListResponse =null;
			if(rsp != null){
				chatListResponse = new ArrayList<ChatListResponse>();
				for (SolrDocument doc : rsp.getResults()) {
					ChatListResponse chatResponse = new ChatListResponse();
					chatResponse.setChatId(doc.getFieldValue("id").toString());
					chatResponse.setDate(doc.getFieldValue("updated").toString());
					chatResponse.setType(doc.getFieldValue("type").toString());		
					chatResponse.setUsers(new ArrayList<ChatUser>());
					chatResponse.setRead((List<String>)doc.getFieldValue("read"));
					List<String> users = (List<String>) doc.getFieldValue("userList");
					for (String user : users) {
						ChatUser chatUser = new ChatUser();
						chatUser.setUsername(user);
						chatUser.setDisplayName(searchDao.getDisplayName(user));
						chatResponse.getUsers().add(chatUser);
					}
					chatListResponse.add(chatResponse);
				}
			}
			return chatListResponse;
		} catch(Exception e){
			throw new SearchException(e.getMessage(),e);
		}
	}

	public Object getIndiviualChats(String chatId, String username) {
		if(chatId.contains("%~%")){
			ChatEntity chatEntity = chatEntityRepo.findOne(chatId);
			if(checkReadStatus(chatId, username))
				indexChatToSolr(chatEntity,username,"", "UPDATE");
			return chatEntity;
		} else {
			ChatGroupEntity chatEntity = chatGroupEntityRepo.findOne(chatId);
			if(checkReadStatus(chatId, username))
				indexChatGroupToSolr(chatEntity,username,"READ");
			return chatEntity;
		}
	}

	private boolean checkReadStatus(String chatId, String username) {
		try{
			SolrQuery chatQuery = new SolrQuery();
			chatQuery.setQuery("id:"+chatId);
			chatQuery.setFilterQueries("read:"+username);
			
			QueryRequest req = new QueryRequest(chatQuery);
			req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
			QueryResponse rsp = req.process(solrClient, SpringPropertiesUtil.getProperty("solr.chat.collection")); 

			if(rsp!=null){
				if(rsp.getResults().isEmpty())
					return true;
				else
					return false;		
			}
			return true;
		}catch(Exception e){
			return true;
		}	
	}

	public Chat createGroupMessage(String username, ChatRequest chat) {
		try{
			ChatGroupEntity chatGroup = new ChatGroupEntity();
			chatGroup.setMessageList(new ArrayList<Chat>());
			chatGroup.setId(UUID.randomUUID().toString());
			chatGroup.setOwner(username);
			chatGroup.setUserList(chat.getUsers());
			chatGroup.setCreated(DateTime.now());
			chatGroup.setUpdated(DateTime.now());
			
			Chat chatObj = new Chat();
			chatObj.setChatId(UUID.randomUUID().toString());
			chatObj.setChatOwner(username);
			chatObj.setMesage(chat.getMessage());
			chatObj.setCreated(DateTime.now());
			
			chatGroup.getMessageList().add(chatObj);	
			indexChatGroupToSolr(chatGroup, username, "CREATE");
			chatGroupEntityRepo.save(chatGroup);
			return chatObj;
		} catch(Exception e){
			throw new SearchException(e.getMessage(),e);
		}
	}
	
	private void indexChatGroupToSolr(ChatGroupEntity chatGroup, String username, String type) {
		try{
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", chatGroup.getId());
			if(type.equalsIgnoreCase("CREATE")){
				chatGroup.getUserList().add(username);
				doc.addField("created", new Date());
			    doc.addField("updated", new Date());
			    doc.addField("type", "group");
			    doc.addField("read", username);
			    doc.addField("userList", chatGroup.getUserList());
			}
			if(type.equalsIgnoreCase("READ")){
				Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("add", username);
		    	doc.addField("read", partialUpdate);
		    }
		    if(type.equalsIgnoreCase("UPDATEUSER-ADD")){
		    	Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("add", username);
		    	doc.addField("userList", partialUpdate);
		    }
		    if(type.equalsIgnoreCase("UPDATEUSER-REMOVE")){
		    	Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("remove", username);
		    	doc.addField("userList", partialUpdate);
		    }
		    if(type.equalsIgnoreCase("UPDATEMSSG")){
		    	Map<String, String> partialDateUpdate = new HashMap<String, String>();
		    	partialDateUpdate.put("set", ZonedDateTime.now( ZoneOffset.UTC ).toString());
		    	doc.addField("updated", partialDateUpdate);
		    	Map<String, String> partialUpdate = new HashMap<String, String>();
		    	partialUpdate.put("set", null);
			    doc.addField("read", partialUpdate);
		    }
		    
		    UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		    req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.chat.collection")); 
			
		} catch(Exception e){
			System.out.println(e);
			throw new SearchException(e.getMessage(),e);
		}
	}

	public Chat updateGroupMessage(String username, String chatId, String message) {
		try{
			ChatGroupEntity chatGroup = chatGroupEntityRepo.findOne(chatId);
			
			Chat chatObj = new Chat();
			chatObj.setChatId(UUID.randomUUID().toString());
			chatObj.setChatOwner(username);
			chatObj.setMesage(message);
			chatObj.setCreated(DateTime.now());
			
			chatGroup.getMessageList().add(chatObj);	
			indexChatGroupToSolr(chatGroup, username, "UPDATEMSSG");
			chatGroupEntityRepo.save(chatGroup);
			return chatObj;
		} catch(Exception e){
			throw new SearchException(e.getMessage(),e);
		}
	}

	public void updateGroup(String username, String adduser, String action, ChatRequest chat) {
		try{
			ChatGroupEntity chatGroup = chatGroupEntityRepo.findOne(chat.getChatId());
			if(action.equalsIgnoreCase("add")){
				List<String> userList = chatGroup.getUserList();
				userList.add(adduser);
				chatGroup.setUserList(userList);
				indexChatGroupToSolr(chatGroup, adduser, "UPDATEUSER-ADD");
			}else if(action.equalsIgnoreCase("remove")){
				List<String> userList = chatGroup.getUserList();
				Iterator<String> Iterator = userList.iterator();
				while (Iterator.hasNext()) {
					String user = Iterator.next();
				    if (user.equals(adduser)) {
				    	Iterator.remove();
				    }
				}
				indexChatGroupToSolr(chatGroup, adduser, "UPDATEUSER-REMOVE");
			} else {
				throw new InvalidRequestException(ExceptionCodes.IRECodes.IRE_DEFAULT,"InvalidRequestException");
			}
			System.out.println("chatGroup" + chatGroup);
			chatGroupEntityRepo.save(chatGroup);
		} catch(Exception e){
			System.out.println("ERRRR" + e);
			throw new SearchException(e.getMessage(),e);
		}
		
	}
}
