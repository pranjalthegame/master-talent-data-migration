/**
 * 
 */
package com.master.talent.data.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.master.talent.data.model.Banner;
import com.master.talent.data.model.Employment;
import com.master.talent.data.model.UserModel;
import com.master.talent.data.model.excel.UserExcelModel;

/**
 * @author pranjal.sinha
 *
 */
public class UserConverterUtils {

	//This method will convert all specific data types for userModel
			//For ex, all comma separated string values from excel model to be converted into List or 
			//other suited values based on structure of userModel
	public static UserModel convertUser(UserExcelModel userExcelModel){

		//plain fields
		UserModel userModel = new UserModel();
		userModel.setActive(userExcelModel.isActive());
		userModel.setBreif(userExcelModel.getBrief());
		userModel.setDob(userExcelModel.getDob());
		userModel.setEmailAddress(userExcelModel.getEmailAddress());
		userModel.setFname(userExcelModel.getFname());
		userModel.setLname(userExcelModel.getLname());
		userModel.setGender(userExcelModel.getGender());
		userModel.setOtp(userExcelModel.getOtp());
		userModel.setPassword(userExcelModel.getPassword());
		userModel.setReferral(userExcelModel.getReferral());
		userModel.setRegisterLevel(userExcelModel.getRegisterLevel());
		userModel.setRelationship(userExcelModel.getRelationship());
		userModel.setUsername(userExcelModel.getUsername());
		userModel.setFollower(userExcelModel.isFollower());
		
		//List and other fields
		userModel.setBanner(getBanner(userExcelModel));
		userModel.setEmployment(getEmploymentList(userExcelModel));
		userModel.setPhone(CommonUtils.getPhones(userExcelModel.getPhone()));
		userModel.setProInterest(CommonUtils.getSplitString(userExcelModel.getProInterest(),","));
		userModel.setUserType(CommonUtils.getSplitString(userExcelModel.getUserType(),","));
		userModel.setCreatedDate(getDate(userExcelModel.getCreatedDate()));
		
		return userModel;
		
	
	}
	
		private static Date getDate(String source){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
			try {
				return sdf.parse(source);
			} catch (ParseException e) {
				e.printStackTrace();
			}return null;
		}
		
		
		private static Banner getBanner(UserExcelModel userExcelModel){
			Banner banner = new Banner();
			banner.setDefaultImage(userExcelModel.isBannerDefultImage());
			banner.setDefaultPath(userExcelModel.getBannerDefaultPath());
			return banner;
		}
		
		private static List<Employment> getEmploymentList(UserExcelModel userExcelModel){
			List<Employment> employments = new ArrayList<>();
			String[] type = userExcelModel.getEmploymentType().split(",");
			String[] workStatus = userExcelModel.getEmploymentWorkStatus().split(",");
			String[] category = userExcelModel.getEmploymentCategory().split(",");
			String[] expertise = userExcelModel.getEmploymentExpertise().split(",");
			for(int i=0;i<type.length;i++){
				Employment emp = new Employment();
				emp.setType(type[i]);
				emp.setWorkStatus(workStatus[i]);
				emp.setCategory(category[i]);
				emp.setExpertise(expertise[i]);
				employments.add(emp);
			}return employments;
		}
}
