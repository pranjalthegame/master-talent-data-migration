/**
 * 
 */
package com.master.talent.api.utils;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author pranjal.sinha
 *
 */
public class JsonUtils {

	private static final Logger LOGGER = (Logger) LoggerFactory
			.getLogger(JsonUtils.class);
	
	public static String toJson(Object object){
		String json = null;
		ObjectMapper mapper = new ObjectMapper();
		if (object !=null){
			try{
				json = mapper.writeValueAsString(object);
			}catch (JsonGenerationException e) {
				LOGGER.debug(e.getMessage(), e);
			} catch (JsonMappingException e) {
				LOGGER.debug(e.getMessage(), e);
			}catch (IOException e) {
				LOGGER.debug(e.getMessage(), e);
			}
		}
		return json;
	}
	@SuppressWarnings("unchecked")
	public static Object toObject(String json,@SuppressWarnings("rawtypes") Class name ){
		ObjectMapper mapper = new ObjectMapper();
		Object output=null;
		 
		try {
			output = (Object) mapper.readValue(json, name);
		} catch (IOException e) {
			LOGGER.debug(e.getMessage(), e);
		}
		return output;
	}
}
