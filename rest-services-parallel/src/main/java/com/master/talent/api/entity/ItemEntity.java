package com.master.talent.api.entity;


import org.joda.time.DateTime;
import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Article;
import com.master.talent.api.models.Discussion;
import com.master.talent.api.models.Events;
import com.master.talent.api.models.ItemShare;
import com.master.talent.api.models.Job;
import com.master.talent.api.models.Tutorial;
import com.master.talent.api.models.Media;

@Document(expiry=0)
public class ItemEntity
{

	@Id
	@Field
	private String itemId;
	
	@Field
	private String owner;
	
	@Field
	private String pageId;
	
	private String displayName;
	private String pageName;
	
	private String pageUrl;
	
	@Field
	private String itemType;
	
	@Field
	private Job job;
	
	@Field
	private Article article;
	
	@Field
	private Discussion discussion;
	
	@Field
	private Media media;
	
	@Field
	private Tutorial tutorial;
	
	@Field
    private DateTime created;
	
	@Field
    private DateTime updated;
	
	@Field
	private boolean isDelete;
	
	@Field 
	private ItemShare share;
	
	private Events events;
	
		
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public Tutorial getTutorial() {
		return tutorial;
	}

	public void setTutorial(Tutorial tutorial) {
		this.tutorial = tutorial;
	}

	public Events getEvents() {
		return events;
	}

	public void setEvents(Events events) {
		this.events = events;
	}

	// to use only in the case of share
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemId() {
		return itemId;
	}

	public ItemShare getShare() {
		return share;
	}

	public void setShare(ItemShare share) {
		this.share = share;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Discussion getDiscussion() {
		return discussion;
	}

	public void setDiscussion(Discussion discussion) {
		this.discussion = discussion;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Media getMedia() {
		return media;
	}

	public void setMedia(Media media) {
		this.media = media;
	}
	
	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public DateTime getUpdated() {
		return updated;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "ItemEntity [itemId=" + itemId + ", itemType=" + itemType
				+ ", job=" + job + ", article=" + article
				+ ", discussion=" + discussion + ", media=" + media
				+ ", created=" + created + ", updated=" + updated
				+ "]";
	}
	
}
