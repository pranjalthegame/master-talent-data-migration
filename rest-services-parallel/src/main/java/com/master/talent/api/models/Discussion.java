package com.master.talent.api.models;

import java.util.List;


public class Discussion {

	private String title;
	private String desc;
	private String videoLink;
	private List<String> photoPath;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	public List<String> getPhotoPath() {
		return photoPath;
	}
	public void setPhotoPath(List<String> photoPath) {
		this.photoPath = photoPath;
	}
	@Override
	public String toString() {
		return "Discussion [title=" + title + ", desc=" + desc + ", videoLink="
				+ videoLink + ", photoPath=" + photoPath + "]";
	}
}
