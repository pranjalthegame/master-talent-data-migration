package com.master.talent.api.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.master.talent.api.exception.FileException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.Service;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

public class FileJsonProcessor {
	
	private final static Logger logger = LoggerFactory.getLogger(FileJsonProcessor.class);

    static Gson gson = new Gson();
    
    public static Object getJson(String filePath){
    	try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
	        Object json = gson.fromJson(bufferedReader, Object.class);
			return json;
    	} catch (IOException e) {
			logger.debug("Failed to read file json from classpath",e);
			throw new FileException(ExceptionCodes.FECodes.FE_FILE_READ_FAIL,"Failed to read file");
		}
    }
    
    public static List<Service> getJsonAsJsonObject(String filePath){
    	try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
			Type listType = new TypeToken<ArrayList<Service>>(){}.getType();
		    List<Service> list = gson.fromJson(bufferedReader, listType);
			return list;
    	} catch (IOException e) {
			logger.debug("Failed to read file json from classpath",e);
			throw new FileException(ExceptionCodes.FECodes.FE_FILE_READ_FAIL,"Failed to read file");
		}
    }
}
