package com.master.talent.api.models;

import org.joda.time.DateTime;


public class Followers {

	private String followerName;
	private DateTime requestedTime;
	private String isDelete;
	
	public String getFollowerName() {
		return followerName;
	}
	public void setFollowerName(String followerName) {
		this.followerName = followerName;
	}
	public DateTime getRequestedTime() {
		return requestedTime;
	}
	public void setRequestedTime(DateTime requestedTime) {
		this.requestedTime = requestedTime;
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	@Override
	public String toString() {
		return "Friends [followerName=" + followerName 
				+ ", requestedTime=" + requestedTime  + ", isDelete=" + isDelete + "]";
	}
	
}
