/**
 * 
 */
package com.master.talent.data.service;

/**
 * @author pranjal.sinha
 *
 */
public interface CompanyFeedService {

	void processCompanies();
	
	void processCompaniesConcurrently();
}
