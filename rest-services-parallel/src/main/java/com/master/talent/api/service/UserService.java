/**
 * Created by Apache CXF WadlToJava code generator
**/
package com.master.talent.api.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.master.talent.api.entity.CompanyEntity;
import com.master.talent.api.entity.UserAwardsAndRecogEntity;
import com.master.talent.api.entity.UserEducationEntity;
import com.master.talent.api.entity.UserLocationEntity;
import com.master.talent.api.entity.UserOccupationEntity;
import com.master.talent.api.entity.UserPreSignUpEntity;
import com.master.talent.api.entity.UserSkillsEntity;
import com.master.talent.api.entity.UserBankDetailsEntity;
import com.master.talent.api.models.Login;

@Path("/")
public interface UserService {
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userPreSignup")
	Response createUserPreSignup(@Context HttpHeaders headers, UserPreSignUpEntity obj);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/login")
	Response loginUser(@Context HttpHeaders headers, Login obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userPreSignup/byEmail/{emailId}")
	Response getUserPresignByEmail(@Context HttpHeaders headers, @PathParam("emailId") String emailId);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userPreSignup")
	Response updatePreSignup(@Context HttpHeaders headers, UserPreSignUpEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userPreSignup/{username}")
	Response getUserPresignUp(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userPreSignup/userType/{userType}/{start}/{end}")
	Response getUserByUserType(@Context HttpHeaders headers, @PathParam("userType") String userType, @PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/exist/{username}")
	Response verifyUserName(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/verify/{username}/{otp}")
	Response verifyOtp(@Context HttpHeaders headers, @PathParam("username") String username,  @PathParam("otp") String otp);
	
	@DELETE
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userPreSignup/{username}")
	//note this should be a soft delete change attribute delete=true
	Response deleteUserPresignUp(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userSkills")
	Response createUserSkills(@Context HttpHeaders headers, UserSkillsEntity obj);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userSkills")
	Response updateUserSkills(@Context HttpHeaders headers, UserSkillsEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userSkills/{username}")
	Response getUserUserSkills(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@DELETE
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userSkills/{username}")
	//note this should be a soft delete change attribute delete=true
	Response deleteUserSkills(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userOccupation")
	Response createUserOccupation(@Context HttpHeaders headers, UserOccupationEntity obj);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userOccupation")
	Response updateUserOccupation(@Context HttpHeaders headers, UserOccupationEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userOccupation/{username}")
	Response getUserUserOccupation(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@DELETE
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userOccupation/{username}")
	//note this should be a soft delete change attribute delete=true
	Response deleteUserOccupation(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userEducation")
	Response createUserEducation(@Context HttpHeaders headers, UserEducationEntity obj);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userEducation")
	Response updateUserEducation(@Context HttpHeaders headers, UserEducationEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userEducation/{username}")
	Response getUserEducation(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@DELETE
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userEducation/{username}")
	//note this should be a soft delete change attribute delete=true
	Response deleteUserEducation(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userAwardsAndRecog")
	Response createUserAwardsAndRecog(@Context HttpHeaders headers, UserAwardsAndRecogEntity obj);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userAwardsAndRecog")
	Response updateUserAwardsAndRecog(@Context HttpHeaders headers, UserAwardsAndRecogEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userAwardsAndRecog/{username}")
	Response getUserAwardsAndRecog(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@DELETE
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userAwardsAndRecog/{username}")
	Response deleteUserAwardsAndRecog(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/friend/request/{requested}")
	Response requestFriend(@Context HttpHeaders headers, @PathParam("requested") String requested);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/friend/request/accept/{requestor}")
	Response acceptFriendRequest(@Context HttpHeaders headers, @PathParam("requestor") String requestor);
	
	@DELETE
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/friend/{friendName}")
	Response deleteFriendRequest(@Context HttpHeaders headers, @PathParam("friendName") String deleteUsername);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/friend/{username}/{start}/{end}")
	Response getAllFriends(@Context HttpHeaders headers, @PathParam("username") String username, @PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/follow/request/{followed}")
	Response requestFollow(@Context HttpHeaders headers, @PathParam("followed") String followed);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/follow/request/company/{compId}")
	Response requestCompanyFollow(@Context HttpHeaders headers, @PathParam("compId") String compId);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/follow/{username}/{start}/{end}")
	Response getAllFollowers(@Context HttpHeaders headers, @PathParam("username") String username, @PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/follow/company/{compId}/{start}/{end}")
	Response getCompanyFollowers(@Context HttpHeaders headers, @PathParam("compId") String compId, @PathParam("start") int start, @PathParam("end") int end);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userLocation")
	Response createUserLocation(@Context HttpHeaders headers, UserLocationEntity obj);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userLocation")
	Response updateUserLocation(@Context HttpHeaders headers, UserLocationEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/userLocation/{username}")
	Response getUserLocation(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/company")
	Response createCompany(@Context HttpHeaders headers, CompanyEntity obj);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/company")
	Response updateCompany(@Context HttpHeaders headers, CompanyEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/company/{companyId}")
	Response getCompany(@Context HttpHeaders headers, @PathParam("companyId") String companyId);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/company/friendlyId/{friendlyId}")
	Response getCompanyByFriendly(@Context HttpHeaders headers, @PathParam("friendlyId") String friendlyId);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/company/verify/{friendlyId}")
	Response verifyCompanyFriendlyId(@Context HttpHeaders headers, @PathParam("friendlyId") String friendlyId);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/getAllCompany/{start}/{end}")
	Response getAllCompany(@Context HttpHeaders headers, @PathParam("start") int start, @PathParam("end") int end);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/getAllEarnings/{type}")
	Response getAllEarnings(@Context HttpHeaders headers, @PathParam("type") String type);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/bankDetails")
	Response createBankDetails(@Context HttpHeaders headers, UserBankDetailsEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/bankDetails")
	Response getBankDetails(@Context HttpHeaders headers);
}