package com.master.talent.api.utils;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.master.talent.api.dao.SearchDao;
import com.master.talent.api.dao.UserDao;
import com.master.talent.api.entity.UserPreSignUpEntity;
import com.master.talent.api.entity.repo.UserPreSignUpEntityRepo;
import com.master.talent.api.exception.MailSendException;
import com.master.talent.api.exception.SearchException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.RequestInvitation;

@Service
public class EmailUtility {
	
	private final Logger logger = LoggerFactory.getLogger(EmailUtility.class);
	
	@Autowired
	private JavaMailSenderImpl javaMailSender;
	
	@Autowired
	private UserDao dao;
	
	@Autowired
	private SearchDao searchDao;
	
	@Autowired
	private UserPreSignUpEntityRepo userPreSignUpEntityRepo;
	
	SolrClient solrClient = new HttpSolrClient.Builder(SpringPropertiesUtil.getProperty("solr.server.url")).build();	
	
	public void setJavaMailSender(JavaMailSenderImpl javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
		
	public void trainerNotification(String email, String status) {
		try {
			if(status.equalsIgnoreCase("trainer")){
				MimeMessage mimeMessage = javaMailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
				helper.setTo(email);
				helper.setSubject("Master Talent Trainer Request Approved");
				helper.setText("<html><head><title>Mastertalent</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><table cellspacing='0' cellpadding='0' style='background:#f2f2f2;font-family:sans-serif, airal, roboto;' width='600px' align='center'><tbody> <tr> "
						+ "<td align='center'> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;'> <tbody> <tr> <td align='center' valign='top' style=''> <div style='width:570px;margin:15px 0 0 0;'> <table width='570' align='center' border='0' cellspacing='18' cellpadding='0' style='border-collapse:collapse;border-spacing:18px;width:570px;padding-right:0;padding-left:0;background:#9e359e;'> "
						+ "<tbody> <tr> <td align='center' valign='top' style=''> <p style='padding:20px;background:url(http://mastertalent.com/assets/images/popup_bg.png) repeat center center;'> "
						+ "<img style='border-radius:100%;border:2px solid #fff;' src='http://www.mastertalent.com/assets/images/logos/logo.png' /> </p> </td> </tr> </tbody> </table> </div> </td> "
						+ "</tr> </tbody> </table> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;margin-top:0;'> <tbody> <tr> <td align='center' valign='top'> "
						+ "<table width='100%' border='0' cellpadding='0' cellspacing='0'> <tr> <td> <p style='padding:20px;font-size:13px;'> Did you know that Mastertalent World's Professional &amp;"
						+ " Learning network gives lifetime earning for your referrals. Learn more about <a href='http://www.mastertalent.com' target='_blank'>Mastertalent lifetime referral program</a> </p> </td> </tr> <tr> <td style='background:#fff;'> <div style='padding:20px;'> <p style='margin:0 0 20px 0;'></p> <p> Your requst for Trainer has been Approved."
								+ " </strong> </p> </div> <div style='padding:20px;font-size:12px;'> <span style='color:#a0a0a0;'> "
								+ "Best Regards <br>Mastertalent </span> <p>Simplifying Professional &amp; Learning journey</p> </div> </td> </tr> </table> </td> </tr> </tbody> </table>"
								+ " <table width='100%' border='0' cellspacing='0' cellpadding='0' style='width:100%;'> <tbody> <tr> <td align='center' valign='top' style='text-align:center;font-size:12px;'>"
								+ " <div style='width:570px;padding:20px;color:#a0a0a0;'> &copy; 2000-2017 Mastertalent. All rights reserved </div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>"
								+ " </body></html>",true);
				javaMailSender.send(mimeMessage);
			} else if(status.equalsIgnoreCase("rejectTrainer")){
				MimeMessage mimeMessage = javaMailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
				helper.setTo(email);
				helper.setSubject("Master Talent Trainer Request Rejected");
				helper.setText("<html><head><title>Mastertalent</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><table cellspacing='0' cellpadding='0' style='background:#f2f2f2;font-family:sans-serif, airal, roboto;' width='600px' align='center'><tbody> <tr> "
						+ "<td align='center'> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;'> <tbody> <tr> <td align='center' valign='top' style=''> <div style='width:570px;margin:15px 0 0 0;'> <table width='570' align='center' border='0' cellspacing='18' cellpadding='0' style='border-collapse:collapse;border-spacing:18px;width:570px;padding-right:0;padding-left:0;background:#9e359e;'> "
						+ "<tbody> <tr> <td align='center' valign='top' style=''> <p style='padding:20px;background:url(http://mastertalent.com/assets/images/popup_bg.png) repeat center center;'> "
						+ "<img style='border-radius:100%;border:2px solid #fff;' src='http://www.mastertalent.com/assets/images/logos/logo.png' /> </p> </td> </tr> </tbody> </table> </div> </td> "
						+ "</tr> </tbody> </table> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;margin-top:0;'> <tbody> <tr> <td align='center' valign='top'> "
						+ "<table width='100%' border='0' cellpadding='0' cellspacing='0'> <tr> <td> <p style='padding:20px;font-size:13px;'> Did you know that Mastertalent World's Professional &amp;"
						+ " Learning network gives lifetime earning for your referrals. Learn more about <a href='http://www.mastertalent.com' target='_blank'>Mastertalent lifetime referral program</a> </p> </td> </tr> <tr> <td style='background:#fff;'> <div style='padding:20px;'> <p style='margin:0 0 20px 0;'></p> <p> Your requst for Trainer has been Rejected."
								+ " </strong> </p> </div> <div style='padding:20px;font-size:12px;'> <span style='color:#a0a0a0;'> "
								+ "Best Regards <br>Mastertalent </span> <p>Simplifying Professional &amp; Learning journey</p> </div> </td> </tr> </table> </td> </tr> </tbody> </table>"
								+ " <table width='100%' border='0' cellspacing='0' cellpadding='0' style='width:100%;'> <tbody> <tr> <td align='center' valign='top' style='text-align:center;font-size:12px;'>"
								+ " <div style='width:570px;padding:20px;color:#a0a0a0;'> &copy; 2000-2017 Mastertalent. All rights reserved </div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>"
								+ " </body></html>",true);
				javaMailSender.send(mimeMessage);
			}
		} catch (MessagingException e) {
			logger.debug("Messaging exception: {}",e);
			throw new MailSendException(ExceptionCodes.MECodes.ME_MAIL_SEND_FAIL,e.getMessage(),e);
		}
	}
	
	public boolean sendOtp(String username, String type) {
		try {
			UserPreSignUpEntity userPreSignUpEntity = dao.getUserPreSignUpEntity(username);
			userPreSignUpEntity.setOtp(generateOTP(4));
			userPreSignUpEntityRepo.save(userPreSignUpEntity);
		
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			helper.setTo(userPreSignUpEntity.getEmailAddress());
			if(type.equalsIgnoreCase("otp")){
				helper.setSubject("Master Talent OTP");
				
				helper.setText("<html><head><title>Mastertalent</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><table cellspacing='0' cellpadding='0' style='background:#f2f2f2;font-family:sans-serif, airal, roboto;' width='600px' align='center'><tbody> <tr> "
						+ "<td align='center'> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;'> <tbody> <tr> <td align='center' valign='top' style=''> <div style='width:570px;margin:15px 0 0 0;'> <table width='570' align='center' border='0' cellspacing='18' cellpadding='0' style='border-collapse:collapse;border-spacing:18px;width:570px;padding-right:0;padding-left:0;background:#9e359e;'> "
						+ "<tbody> <tr> <td align='center' valign='top' style=''> <p style='padding:20px;background:url(http://mastertalent.com/assets/images/popup_bg.png) repeat center center;'> "
						+ "<img style='border-radius:100%;border:2px solid #fff;' src='http://www.mastertalent.com/assets/images/logos/logo.png' /> </p> </td> </tr> </tbody> </table> </div> </td> "
						+ "</tr> </tbody> </table> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;margin-top:0;'> <tbody> <tr> <td align='center' valign='top'> "
						+ "<table width='100%' border='0' cellpadding='0' cellspacing='0'> <tr> <td> <p style='padding:20px;font-size:13px;'> Did you know that Mastertalent World's Professional &amp;"
						+ " Learning network gives lifetime earning for your referrals. Learn more about <a href='http://www.mastertalent.com' target='_blank'>Mastertalent lifetime referral program</a> </p> </td> </tr> <tr> <td style='background:#fff;'> <div style='padding:20px;'> <p style='margin:0 0 20px 0;'>Hi </p> <p> Your email verification OTP is <strong> "+ userPreSignUpEntity.getOtp()+""
								+ " </strong> </p> </div> <div style='padding:20px;font-size:12px;'> <span style='color:#a0a0a0;'> "
								+ "Best Regards <br>Mastertalent </span> <p>Simplifying Professional &amp; Learning journey</p> </div> </td> </tr> </table> </td> </tr> </tbody> </table>"
								+ " <table width='100%' border='0' cellspacing='0' cellpadding='0' style='width:100%;'> <tbody> <tr> <td align='center' valign='top' style='text-align:center;font-size:12px;'>"
								+ " <div style='width:570px;padding:20px;color:#a0a0a0;'> &copy; 2000-2017 Mastertalent. All rights reserved </div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>"
								+ " </body></html>",true);
					
			}else{
				throw new MailSendException(ExceptionCodes.MECodes.ME_WRONG_TYPE,"Mail send error to ::"+userPreSignUpEntity.getEmailAddress());
			}
			javaMailSender.send(mimeMessage);
			return true;
		} catch (MessagingException e) {
			logger.debug("Messaging exception: {}",e);
			throw new MailSendException(ExceptionCodes.MECodes.ME_MAIL_SEND_FAIL,e.getMessage(),e);
		}
	}
	
	
	public boolean sendWelcome(String email, String fullname) {
		try {	
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			helper.setTo(email);
			helper.setSubject("Welcome to Master Talent");				
			helper.setText("<html><head><title>Mastertalent</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><table cellspacing='0' cellpadding='0' style='background:#f2f2f2;font-family:sans-serif, airal, roboto;' width='600px' align='center'><tbody> <tr> "
					+ "<td align='center'> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;'> <tbody> <tr> <td align='center' valign='top' style=''> <div style='width:570px;margin:15px 0 0 0;'> <table width='570' align='center' border='0' cellspacing='18' cellpadding='0' style='border-collapse:collapse;border-spacing:18px;width:570px;padding-right:0;padding-left:0;background:#9e359e;'> "
					+ "<tbody> <tr> <td align='center' valign='top' style=''> <p style='padding:20px;background:url(http://mastertalent.com/assets/images/popup_bg.png) repeat center center;'> "
					+ "<img style='border-radius:100%;border:2px solid #fff;' src='http://www.mastertalent.com/assets/images/logos/logo.png' /> </p> </td> </tr> </tbody> </table> </div> </td> "
					+ "</tr> </tbody> </table> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;margin-top:0;'> <tbody> <tr> <td align='center' valign='top'> "
					+ "<table width='100%' border='0' cellpadding='0' cellspacing='0'> <tr> <td> <p style='padding:20px;font-size:13px;'> Did you know that Mastertalent World's Professional &amp;"
					+ " Learning network gives lifetime earning for your referrals. Learn more about <a href='http://www.mastertalent.com' target='_blank'>Mastertalent lifetime referral program</a> </p> </td> </tr> <tr> <td style='background:#fff;'> <div style='padding:20px;'> <p style='margin:0 0 20px 0;'>Hi </p> <p> Welcome to Master Talent <strong> "+fullname
					+ " </strong> </p> </div> <div style='padding:20px;font-size:12px;'> <span style='color:#a0a0a0;'> "
					+ "Best Regards <br>Mastertalent </span> <p>Simplifying Professional &amp; Learning journey</p> </div> </td> </tr> </table> </td> </tr> </tbody> </table>"
					+ " <table width='100%' border='0' cellspacing='0' cellpadding='0' style='width:100%;'> <tbody> <tr> <td align='center' valign='top' style='text-align:center;font-size:12px;'>"
					+ " <div style='width:570px;padding:20px;color:#a0a0a0;'> &copy; 2000-2017 Mastertalent. All rights reserved </div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>"
					+ " </body></html>",true);

			javaMailSender.send(mimeMessage);
			return true;
		} catch (MessagingException e) {
			logger.debug("Messaging exception: {}",e);
			throw new MailSendException(ExceptionCodes.MECodes.ME_MAIL_SEND_FAIL,e.getMessage(),e);
		}
	}
	
	static String generateOTP(int len)
    {
        String numbers = "0123456789";
        Random rndm_method = new Random();
        char[] otp = new char[len];
 
        for (int i = 0; i < len; i++) {
            // Use of charAt() method : to get character value
        	
            // Use of nextInt() as it is scanning the value as int
            otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return String.valueOf(otp);
    }
	
	
	public boolean sendInvitation(RequestInvitation requestInvitation) {
		try {
			String fullname = searchDao.getDisplayName(requestInvitation.getUsername());
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			String href = SpringPropertiesUtil.getProperty("master.invite.link")+requestInvitation.getUsername();
			
			List<String> list = requestInvitation.getInviteEmailId();			
			for (String string : list) {
				helper.setTo(string);
				helper.setSubject("You're invited to Master Talent");
				helper.setText("<html><head><title>Mastertalent</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><table cellspacing='0' cellpadding='0' style='background:#f2f2f2;font-family:sans-serif, airal, roboto;' width='600px' align='center'><tbody> <tr> "
						+ "<td align='center'> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;'> <tbody> <tr> <td align='center' valign='top' style=''> <div style='width:570px;margin:15px 0 0 0;'> <table width='570' align='center' border='0' cellspacing='18' cellpadding='0' style='border-collapse:collapse;border-spacing:18px;width:570px;padding-right:0;padding-left:0;background:#9e359e;'> "
						+ "<tbody> <tr> <td align='center' valign='top' style=''> <p style='padding:20px;background:url(http://mastertalent.com/assets/images/popup_bg.png) repeat center center;'> "
						+ "<img style='border-radius:100%;border:2px solid #fff;' src='http://www.mastertalent.com/assets/images/logos/logo.png' /> </p> </td> </tr> </tbody> </table> </div> </td> "
						+ "</tr> </tbody> </table> <table width='570' border='0' cellspacing='0' cellpadding='0' style='width:570px;margin-top:0;'> <tbody> <tr> <td align='center' valign='top'> "
						+ "<table width='100%' border='0' cellpadding='0' cellspacing='0'> <tr> <td> <p style='padding:20px;font-size:13px;'> Did you know that Mastertalent World's Professional &amp;"
						+ " Learning network gives lifetime earning for your referrals. Learn more about <a href='http://www.mastertalent.com' target='_blank'>Mastertalent lifetime referral program</a> </p> </td> </tr> <tr> <td style='background:#fff;'> <div style='padding:20px;'> <p style='margin:0 0 20px 0;'></p> You have been invited to Master Talent by "+fullname +".\n"
								+ "<a href=\""+ href +"\">Visit Maaster Talent</a></p> </div> <div style='padding:20px;font-size:12px;'> <span style='color:#a0a0a0;'> "
								+ "Best Regards <br>Mastertalent </span> <p>Simplifying Professional &amp; Learning journey</p> </div> </td> </tr> </table> </td> </tr> </tbody> </table>"
								+ " <table width='100%' border='0' cellspacing='0' cellpadding='0' style='width:100%;'> <tbody> <tr> <td align='center' valign='top' style='text-align:center;font-size:12px;'>"
								+ " <div style='width:570px;padding:20px;color:#a0a0a0;'> &copy; 2000-2017 Mastertalent. All rights reserved </div> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>"
								+ " </body></html>",true);
				javaMailSender.send(mimeMessage);
			}
			indexInvitation(requestInvitation);				
			return true;
		} catch (MessagingException e) {
			logger.debug("Messaging exception: {}",e);
			throw new MailSendException(ExceptionCodes.MECodes.ME_MAIL_SEND_FAIL,e.getMessage(),e);
		} catch (SolrServerException | IOException e) {
			logger.debug("Messaging exception: {}",e);
			throw new MailSendException(ExceptionCodes.MECodes.ME_MAIL_SEND_FAIL,e.getMessage(),e);
		}
	}

	private void indexInvitation(RequestInvitation requestInvitation) {
		try{
			
			SolrInputDocument doc = new SolrInputDocument();
			doc.addField("id", UUID.randomUUID().toString());
			doc.addField("username", requestInvitation.getUsername());
		    doc.addField("created", new Date());
		    doc.addField("invitieesList", requestInvitation.getInviteEmailId());
		    
		    UpdateRequest req = new UpdateRequest(); 
		    req.setBasicAuthCredentials(SpringPropertiesUtil.getProperty("solr.username"), SpringPropertiesUtil.getProperty("solr.password"));
		    req.add(doc); 
			req.setCommitWithin(Integer.parseInt(SpringPropertiesUtil.getProperty("solr.timeout"))); 
			req.process(solrClient, SpringPropertiesUtil.getProperty("solr.invited.collection")); 
			
		} catch(Exception e){
			logger.debug("indexInvitation failure", e);
			throw new SearchException(ExceptionCodes.SECodes.SE_USER_INDEX_FAIL,e.getMessage(),e);
		}		
		
	}

}
