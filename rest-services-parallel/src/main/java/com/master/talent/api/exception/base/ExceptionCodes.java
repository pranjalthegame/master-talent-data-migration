/**
 * 
 */
package com.master.talent.api.exception.base;

/**
 * @author pranjal.sinha
 *
 */
public class ExceptionCodes {

	//inner class for CouchbaseWriteFailureException Codes
	public static class CBECodes{
		public static final String CB_DEFAULT = "CBE001";
		public static final String CB_EXISTS_USER = "CBE002";
		public static final String CB_USER_UPDATE_FAIL = "CBE003";
		public static final String CB_USER_CREATE_FAIL = "CBE004";
		public static final String CB_USER_DELETE_FAIL = "CBE005";
		public static final String CB_EXISTS_USERSKILLS = "CBE006";
		public static final String CB_USERSKILLS_CREATE_FAIL = "CBE007";
		public static final String CB_USERSKILLS_UPDATE_FAIL = "CBE008";
		public static final String CB_USERSKILLS_DELETE_FAIL = "CBE009";
		public static final String CB_EXISTS_USER_OCCUPATION = "CB010";
		public static final String CB_USER_OCC_CREATE_FAIL = "CB011";
		public static final String CB_USER_OCC_UPDATE_FAIL = "CB012";
		public static final String CB_USER_OCC_DELETE_FAIL = "CB013";
		public static final String CB_EXISTS_USER_EDUCATION = "CB014";
		public static final String CB_USER_EDU_CREATE_FAIL = "CB015";
		public static final String CB_USER_EDU_UPDATE_FAIL = "CB016";
		public static final String CB_USER_EDU_DELETE_FAIL = "CB017";
		public static final String CB_EXISTS_USER_AWARDS = "CB018";
		public static final String CB_USER_AWARDS_CREATE_FAIL = "CB019";
		public static final String CB_USER_AWARDS_UPDATE_FAIL = "CB020";
		public static final String CB_USER_AWARDS_DELETE_FAIL = "CB021";
		public static final String CB_ADD_FRIEND_AND_INDEX_FAIL = "CB022";
		public static final String CB_INVALID_TYPE = "CB023";
		public static final String CB_ITEM_CREATE_FAIL = "CB024";
		public static final String CB_ITEM_UPDATE_FAIL = "CB025";
		public static final String CB_ITEM_DELETE_FAIL = "CB026";
		public static final String CB_EXISTS_ITEM = "CB027";
		public static final String CB_EXISTS_COMMENT = "CB028";
		public static final String CB_COMMENT_CREATE_FAIL = "CB029";
		public static final String CB_COMMENT_UPDATE_FAIL = "CB030";
		public static final String CB_COMMENT_DELETE_FAIL = "CB031";
		public static final String CB_USER_LOCATION_FAIL = "CBE032";
		
		
	}
	
	//inner class for FileException Codes
	public static class FECodes{
		public static final String FE_DEFAULT = "FE001";
		public static final String FE_FILE_NOT_FOUND = "FE002";
		public static final String FE_FILE_READ_FAIL = "FE003";
	}
	
	//inner class for ImageUploadException Codes
	public static class IUECodes{
		public static final String IUE_DEFAULT = "IUE001";
		public static final String IUE_UPLOAD_FAIL = "IUE002";
		public static final String IUE_USER_IMAGE_RENDER_FAIL = "IUE003";
		public static final String IUE_ITEM_IMAGE_RENDER_FAIL = "IUE004";
		public static final String IUE_BANNER_IMAGE_RENDER_FAIL = "IUE005";
		public static final String IUE_PROFILE_IMAGE_RENDER_FAIL = "IUE006";
		
	}
	
	//inner class for MailSendException Codes
	public static class MECodes{
		public static final String ME_DEFAULT = "ME001";
		public static final String ME_CONNECTION_FAIL = "ME002";
		public static final String ME_WRONG_TYPE = "ME003";
		public static final String ME_MAIL_SEND_FAIL = "ME004";
	}
	
	//inner class for ResourceNotFoundException Codes
	public static class RECodes{
		public static final String RE_DEFAULT = "RE001";
		public static final String RE_USER_NOT_FOUND = "RE002";
		public static final String RE_USER_SKILLS_NOT_FOUND = "RE003";
		public static final String RE_USER_DELETE_FAIL = "RE004";
		public static final String RE_USER_SKILLS_DELETE_FAIL = "RE005";
		public static final String RE_USER_OCC_DELETE_FAIL = "RE006";
		public static final String RE_USER_OCC_NOT_FOUND = "RE007";
		public static final String RE_USER_EDU_DELETE_FAIL = "RE008";
		public static final String RE_USER_EDU_NOT_FOUND = "RE009";
		public static final String RE_USER_AWARDS_DELETE_FAIL = "RE010";
		public static final String RE_USER_AWARDS_NOT_FOUND = "RE011";
		public static final String RE_VALIDATE_OTP_FAIL = "RE012";
		public static final String RE_ITEM_NOT_FOUND = "RE013";
		public static final String RE_ITEM_DELETE_FAIL = "RE014";
		public static final String RE_COMMENT_NOT_FOUND = "RE015";
		public static final String RE_COMMENT_DELETE_FAIL = "RE016";
		
	}
	
	//inner class for SearchException Codes
	public static class SECodes{
		public static final String SE_DEFAULT = "SE001";
		public static final String SE_USER_INDEX_FAIL = "SE002";
		public static final String SE_ITEM_INDEX_FAIL = "SE003";
		public static final String SE_GET_USER_FREINDS_FAIL = "SE004";
		public static final String SE_GET_USER_FOLLOWERS_FAIL = "SE005";
		public static final String SE_VERIFY_FRIEND_FAIL = "SE006";
		public static final String SE_VERIFY_FOLLOWER_FAIL = "SE007";
		public static final String SE_SEARCH_AUTO_COMPLETE_FAIL = "SE008";
		public static final String SE_SEARCH_GET_ITEMS_FAIL = "SE009";
		public static final String SE_SEARCH_GET_DASHBOARD_ITEMS_FAIL = "SE010";
		public static final String SE_SEARCH_ITEMS_FAIL = "SE011";
		
	}
	
	//inner class for UnauthorisedException Codes
	public static class UECodes{
		public static final String UE_DEFAULT = "UE001";
		public static final String UE_NOT_ALLOWED = "UE002";
		public static final String UE_HEADER_LIST_NULL = "UE003";
		public static final String UE_COOKIE_NOT_FOUND = "UE004";
		public static final String UE_SESSION_NULL = "UE005";
	}
	
	//inner class for InvalidRequestException codes
	public static class IRECodes{
		public static final String IRE_DEFAULT = "IRE001";
	}
}
