/**
 * 
 */
package com.master.talent.data.service;

/**
 * @author pranjal.sinha
 *
 */
public interface MigrationService {

	void initiate(String type);

}
