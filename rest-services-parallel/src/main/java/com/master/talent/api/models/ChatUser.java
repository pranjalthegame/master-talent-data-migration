package com.master.talent.api.models;

public class ChatUser {	
	
	private String username;
	private String displayName;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@Override
	public String toString() {
		return "ChatUser [username=" + username + ", displayName="
				+ displayName + "]";
	}
	
}
