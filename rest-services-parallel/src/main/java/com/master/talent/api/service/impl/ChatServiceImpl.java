package com.master.talent.api.service.impl;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.master.talent.api.dao.ChatDao;
import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.exception.InvalidRequestException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.models.Chat;
import com.master.talent.api.models.ChatRequest;
import com.master.talent.api.models.GetChat;
import com.master.talent.api.service.ChatService;

@CrossOriginResourceSharing(
        allowOrigins = {
           "*"
        }, 
        allowCredentials = true, 
        maxAge = 1, 
        allowHeaders = {
           "MTUser"
        }
)
public class ChatServiceImpl implements ChatService {

	private final Gson serializer = new Gson();

	private final Logger logger = LoggerFactory.getLogger(ChatServiceImpl.class);
	
	@Autowired
	private ChatDao dao;
	
	@Autowired
	private  RequestValidator validator;
	

	@Override
	public Response createMessage(HttpHeaders headers, ChatRequest chat) {
		try{
			SessionEntity sessionEntity = validator.validateAndReturn(headers);
			Chat response =null;
			
			if((chat.getUsers()==null) && (chat.getChatId() !=null)){	
				response = dao.updateGroupMessage(sessionEntity.getUsername(), chat.getChatId(),  chat.getMessage());
			} else{
				if(chat.getUsers().size() ==1){
					response = dao.createMessage(sessionEntity.getUsername(), chat.getUsers().get(0), chat.getMessage());
				} else {
					if(chat.getChatId() == null){
						response = dao.createGroupMessage(sessionEntity.getUsername(), chat);
					}
				}
			}
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		} catch(Exception e){
			throw new InvalidRequestException(ExceptionCodes.IRECodes.IRE_DEFAULT,e.getMessage(),e);
		}
	}

	@Override
	public Response getAllChats(HttpHeaders headers) {
		SessionEntity sessionEntity = validator.validateAndReturn(headers);
		Object response = dao.getAllChats(sessionEntity.getUsername());
		if(response != null){
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		} else {
			return Response.ok("{\"success\" : \"No chats Found for the user\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
		
	}

	@Override
	public Response getIndiviualChats(HttpHeaders headers, GetChat chat) {
		SessionEntity session = validator.validateAndReturn(headers);
		Object response =dao.getIndiviualChats(chat.getChatId(), session.getUsername());
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response deleteMessage(HttpHeaders headers, GetChat chat) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response updateGroup(HttpHeaders headers, String action, String username, ChatRequest chat) {
		SessionEntity session = validator.validateAndReturn(headers);
		dao.updateGroup(session.getUsername(), username, action, chat);
		return Response.ok("{\"success\" : \"User List updated\"}", MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();

	}	
}
