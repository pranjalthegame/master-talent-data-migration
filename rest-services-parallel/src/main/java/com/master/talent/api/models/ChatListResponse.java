package com.master.talent.api.models;

import java.util.Date;
import java.util.List;

public class ChatListResponse {
	
	private String chatId;
	private String type;
	private List<ChatUser> users;
	private String date;
	private List<String> read;
	
	public List<String> getRead() {
		return read;
	}
	public void setRead(List<String> read) {
		this.read = read;
	}
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<ChatUser> getUsers() {
		return users;
	}
	public void setUsers(List<ChatUser> users) {
		this.users = users;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "ChatListResponse [chatId=" + chatId + ", type=" + type
				+ ", users=" + users + ", date=" + date + "]";
	}	
}
