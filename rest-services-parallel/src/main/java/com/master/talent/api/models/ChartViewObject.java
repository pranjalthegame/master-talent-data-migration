package com.master.talent.api.models;

import java.util.Map;


public class ChartViewObject {

	private Map<String, Integer> list;
	private int unknownViewCount;
	private int userViewCount;
	
	public Map<String, Integer> getList() {
		return list;
	}
	public void setList(Map<String, Integer> list) {
		this.list = list;
	}
	public int getUnknownViewCount() {
		return unknownViewCount;
	}
	public void setUnknownViewCount(int unknownViewCount) {
		this.unknownViewCount = unknownViewCount;
	}
	public int getUserViewCount() {
		return userViewCount;
	}
	public void setUserViewCount(int userViewCount) {
		this.userViewCount = userViewCount;
	}
	
	@Override
	public String toString() {
		return "ChartViewObject [list=" + list + ", unknownViewCount=" + unknownViewCount + ", userViewCount="
				+ userViewCount + "]";
	}
	
}
