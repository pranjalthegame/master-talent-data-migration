package com.master.talent.api.exception.decorator;

import com.master.talent.api.annotation.Handles;
import com.master.talent.api.exception.base.ExceptionDecorator;
import com.master.talent.api.exception.base.MasterTalentException;
import com.master.talent.api.utils.SpringPropertiesUtil;

@Handles(value="com.master.talent.api.exception.SearchException")
public class SearchExceptionDecorator implements ExceptionDecorator {
	
    @Override
    public String decorate(MasterTalentException exception){
		String returnVal = SpringPropertiesUtil.getProperty(exception.getExceptionCode());
		if(returnVal==null || returnVal.equalsIgnoreCase("")){
			returnVal = SpringPropertiesUtil.getProperty("search.exception.json.message");
		}return returnVal;
    }
}
