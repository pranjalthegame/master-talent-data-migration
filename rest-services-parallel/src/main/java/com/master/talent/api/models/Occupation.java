package com.master.talent.api.models;

public class Occupation {
	
	private String orgName;
	private String title;
	private String industry;
	private String jobFundtionalArea;
	private String jobLevel;
	private String incomeLevel;
	private boolean jobApplicableFlag;
	private String jobApplicable;
	private String startMonth;
	private String startYear;
	private String endMonth;
	private boolean endDateFlag;
	private String endyear;
	private String breif;
	
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getJobTitle() {
		return title;
	}
	public void setJobTitle(String title) {
		this.title = title;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getJobFundtionalArea() {
		return jobFundtionalArea;
	}
	public void setJobFundtionalArea(String jobFundtionalArea) {
		this.jobFundtionalArea = jobFundtionalArea;
	}
	public String getJobLevel() {
		return jobLevel;
	}
	public void setJobLevel(String jobLevel) {
		this.jobLevel = jobLevel;
	}
	public String getIncomeLevel() {
		return incomeLevel;
	}
	public void setIncomeLevel(String incomeLevel) {
		this.incomeLevel = incomeLevel;
	}
	public boolean isJobApplicableFlag() {
		return jobApplicableFlag;
	}
	public void setJobApplicableFlag(boolean jobApplicableFlag) {
		this.jobApplicableFlag = jobApplicableFlag;
	}
	public String getJobApplicable() {
		return jobApplicable;
	}
	public void setJobApplicable(String jobApplicable) {
		this.jobApplicable = jobApplicable;
	}
	public boolean isEndDateFlag() {
		return endDateFlag;
	}
	public void setEndDateFlag(boolean endDateFlag) {
		this.endDateFlag = endDateFlag;
	}
	public String getBreif() {
		return breif;
	}
	public void setBreif(String breif) {
		this.breif = breif;
	}
	public String getStartMonth() {
		return startMonth;
	}
	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}
	public String getStartYear() {
		return startYear;
	}
	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}
	public String getEndMonth() {
		return endMonth;
	}
	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}
	public String getEndyear() {
		return endyear;
	}
	public void setEndyear(String endyear) {
		this.endyear = endyear;
	}
	
	@Override
	public String toString() {
		return "Occupation [orgName=" + orgName + ", title=" + title
				+ ", industry=" + industry + ", jobFundtionalArea="
				+ jobFundtionalArea + ", jobLevel=" + jobLevel
				+ ", incomeLevel=" + incomeLevel + ", jobApplicableFlag="
				+ jobApplicableFlag + ", jobApplicable=" + jobApplicable
				+ ", startMonth=" + startMonth + ", startYear=" + startYear
				+ ", endMonth=" + endMonth + ", endDateFlag=" + endDateFlag
				+ ", endyear=" + endyear + ", breif=" + breif + "]";
	}
}
