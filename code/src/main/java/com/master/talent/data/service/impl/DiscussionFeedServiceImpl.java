/**
 * 
 */
package com.master.talent.data.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.master.talent.data.callabletasks.CallableDiscussionTask;
import com.master.talent.data.constants.AppConstants;
import com.master.talent.data.model.DiscussionModel;
import com.master.talent.data.model.excel.DiscussionExcelModel;
import com.master.talent.data.service.DiscussionFeedService;
import com.master.talent.data.service.ReaderService;
import com.master.talent.data.service.RestService;
import com.master.talent.data.util.CommonUtils;
import com.master.talent.data.util.DiscussionConverterUtils;

/**
 * @author pranjal.sinha
 *
 */
@Service
public class DiscussionFeedServiceImpl  implements DiscussionFeedService{

	private static final Logger LOG = LoggerFactory.getLogger(DiscussionFeedServiceImpl.class);
	
	@Autowired
	private ReaderService readerService;
	
	@Autowired
	private RestService restService;
	
	private static final int MAX_NO_OF_BATCHES = 20;
	
	/**
	 * below code runs in single thread mode
	 */
	@Override
	public void processDiscussions() {
		List<DiscussionExcelModel> discussionData = readerService.readDiscussionFeedFile(AppConstants.DISCUSSION_FEED_FILE);
		if(discussionData!=null && !discussionData.isEmpty()){
			//START
			for(DiscussionExcelModel inputData: discussionData){
				DiscussionModel discussionModel = DiscussionConverterUtils.convertDiscussion(inputData);
				restService.processDiscussionOverHttp(discussionModel);
			} 
		}//END
			
	}
	
	/**
	 * below code template will split the data set into batches and process them concurrently
	 * 
	 */
	@Override
	public void processDiscussionsConcurrently() {
		List<DiscussionExcelModel> discussionData = readerService.readDiscussionFeedFile(AppConstants.DISCUSSION_FEED_FILE);
		if(discussionData!=null && !discussionData.isEmpty()){
		List<List<DiscussionExcelModel>> splitList = CommonUtils.createBatches(discussionData, MAX_NO_OF_BATCHES);
		int size = splitList.size();
		ExecutorService executorService = Executors.newFixedThreadPool(size);
		LOG.info("Initializing Thread pool of size: "+splitList.size());
		try{
			ExecutorCompletionService<Map<String,String>> completionService = new ExecutorCompletionService<>(executorService);
			for(List<DiscussionExcelModel> discussionBatch: splitList){
				LOG.info("Batch size to be procesed is: "+discussionBatch.size());
				completionService.submit(new CallableDiscussionTask(restService,discussionBatch));
			}CommonUtils.pollForResult(completionService, size,AppConstants.TYPE_DISCUSSION);
		}finally{
			executorService.shutdown();
		}
	}
	}
}
