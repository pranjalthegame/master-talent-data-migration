/**
 * 
 */
package com.master.talent.data.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.master.talent.data.model.Discussion;
import com.master.talent.data.model.DiscussionModel;
import com.master.talent.data.model.excel.DiscussionExcelModel;

/**
 * @author pranjal.sinha
 *
 */
public class DiscussionConverterUtils {

	private static final String SPLIT_BY_COMMA = ",";
	
	public static DiscussionModel convertDiscussion(DiscussionExcelModel discussionExcelModel){
		DiscussionModel discussionModel = new DiscussionModel();
		discussionModel.setOwner(discussionExcelModel.getOwner());
		discussionModel.setItemType(discussionExcelModel.getItemType());
		discussionModel.setCreated(getDate(discussionExcelModel.getCreated()));
		discussionModel.setDelete(discussionExcelModel.isDelete());
		discussionModel.setDiscussion(getDiscussion(discussionExcelModel));
		discussionModel.setUpdated(getDate(discussionExcelModel.getUpdated()));
		return discussionModel;
	}

	private static Discussion getDiscussion(DiscussionExcelModel discussionExcelModel) {
		Discussion discussion = new Discussion();
		discussion.setTitle(discussionExcelModel.getTitle());
		discussion.setDesc(discussionExcelModel.getDesc());
		discussion.setVideoLink(discussionExcelModel.getVideoLink());
		discussion.setPhotoPath(CommonUtils.getSplitString(discussionExcelModel.getPhotoPath(), SPLIT_BY_COMMA));
		return discussion;
	}

	private static DateTime getDate(String date) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime dt = formatter.parseDateTime(date);
		return dt;
	}
}
