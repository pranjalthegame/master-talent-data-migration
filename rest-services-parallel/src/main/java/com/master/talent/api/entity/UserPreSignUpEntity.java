package com.master.talent.api.entity;


import java.util.Date;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Banner;
import com.master.talent.api.models.Employment;
import com.master.talent.api.models.PhoneNumber;

@Document(expiry=0)
public class UserPreSignUpEntity
{
	
	public static final String ENTITY_NAME = "PRESIGNUP-";
	
	@Id
    private String key;
	@Field
	@NotEmpty
	private String username;
	@Field
	@NotEmpty
	private String password;
	@Field
	@NotEmpty
	private String fname;
	@Field
	@NotEmpty
	private String lname;
	@Field
	private String fullname;
	@Field
	private String emailAddress;
	@Field
	private List<PhoneNumber> phone;
	@Field
	private String dob;
	@Field
	private String gender;
	@Field
	private String relationship;
	@Field
	private String breif;
	@Field
	private List<String> proInterest;
	@Field
	private List<Employment> employment;
	@Field
	private String registerLevel;
	@Field
	private String otp;
	@Field
	private boolean isDeleted;
	@Field
	private boolean active;
	@Field
	private Banner banner;
	@Field
	private List<String> userType;
	@Field
	private String referral;
	
	private boolean isFriend;
	private boolean isFollower;
	private boolean isPendingFriend;
	private boolean isRequestedPendingFriend;
	
	private Date createdDate;
	
	
	
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getReferral() {
		return referral;
	}
	public void setReferral(String referral) {
		this.referral = referral;
	}
	public boolean isRequestedPendingFriend() {
		return isRequestedPendingFriend;
	}
	public void setRequestedPendingFriend(boolean isRequestedPendingFriend) {
		this.isRequestedPendingFriend = isRequestedPendingFriend;
	}
	public boolean isPendingFriend() {
		return isPendingFriend;
	}
	public void setPendingFriend(boolean isPendingFriend) {
		this.isPendingFriend = isPendingFriend;
	}
	public boolean isFriend() {
		return isFriend;
	}
	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}
	public boolean isFollower() {
		return isFollower;
	}
	public void setFollower(boolean isFollower) {
		this.isFollower = isFollower;
	}
	public List<String> getUserType() {
		return userType;
	}
	public void setUserType(List<String> userType) {
		this.userType = userType;
	}
	public String getKey() {
		return key;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
		this.key = ENTITY_NAME+username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getFullname() {
		return this.fname +" " +this.lname;
	}
	public void setFullname(String fullname) {
		this.fullname = this.fname +" " +this.lname;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public List<PhoneNumber> getPhone() {
		return phone;
	}
	public void setPhone(List<PhoneNumber> phone) {
		this.phone = phone;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getBreif() {
		return breif;
	}
	public void setBreif(String breif) {
		this.breif = breif;
	}
	public List<String> getProInterest() {
		return proInterest;
	}
	public void setProInterest(List<String> proInterest) {
		this.proInterest = proInterest;
	}
	public List<Employment> getEmployment() {
		return employment;
	}
	public void setEmployment(List<Employment> employment) {
		this.employment = employment;
	}
	public String getRegisterLevel() {
		return registerLevel;
	}
	public void setRegisterLevel(String registerLevel) {
		this.registerLevel = registerLevel;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}	
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}	
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Banner getBanner() {
		return banner;
	}
	public void setBanner(Banner banner) {
		this.banner = banner;
	}
	
	@Override
	public String toString() {
		return "UserPreSignUp [username=" + username + ", password=" + password
				+ ", fname=" + fname + ", lname=" + lname + ", fullname="
				+ fullname + ", emailAddress=" + emailAddress + ", phone="
				+ phone + ", dob=" + dob + ", gender=" + gender
				+ ", relationship=" + relationship + ", breif=" + breif
				+ ", proInterest=" + proInterest + ", employment=" + employment
				+ ", registerLevel=" + registerLevel + "]";
	}	
}
