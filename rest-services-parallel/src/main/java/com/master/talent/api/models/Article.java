package com.master.talent.api.models;

import java.util.List;


public class Article {

	private String articleType;
	private String title;
	private String desc;
	private List<String> photoPath;
	
	public String getArticleType() {
		return articleType;
	}
	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}	
	public List<String> getPhotoPath() {
		return photoPath;
	}
	public void setPhotoPath(List<String> photoPath) {
		this.photoPath = photoPath;
	}
	@Override
	public String toString() {
		return "Article [articleType=" + articleType + ", title=" + title
				+ ", desc=" + desc + ", photoPath=" + photoPath + "]";
	}
}
