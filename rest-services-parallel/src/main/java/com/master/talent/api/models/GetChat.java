package com.master.talent.api.models;

public class GetChat {

	private String chatId;
	private String messageId;
	
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	@Override
	public String toString() {
		return "GetChat [chatId=" + chatId + ", messageId=" + messageId + "]";
	}
	
	
}
