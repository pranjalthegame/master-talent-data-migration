/**
 * 
 */
package com.master.talent.data.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.master.talent.data.model.Banner;
import com.master.talent.data.model.CompanyModel;
import com.master.talent.data.model.excel.CompanyExcelModel;

/**
 * @author pranjal.sinha
 *
 */
public class CompanyConverterUtils {

	private static final String SPLIT_BY_COMMA = ",";
	private static final String SPLIT_BY_STOP = ".";
	
	public static CompanyModel convertCompany(CompanyExcelModel companyExcelModel){
		CompanyModel companyModel = new CompanyModel();
		companyModel.setOwner(companyExcelModel.getOwner());
		companyModel.setOwnershipType(companyExcelModel.getOwnershipType());
		companyModel.setOffers(CommonUtils.getSplitString(companyExcelModel.getOffers(), SPLIT_BY_COMMA));
		companyModel.setAddress(CommonUtils.getSplitString(companyExcelModel.getAddress(), SPLIT_BY_STOP));
		companyModel.setTurnOverCurrency(companyExcelModel.getTurnOverCurrency());
		companyModel.setFoundedOn(companyExcelModel.getFoundedOn());
		companyModel.setCreated(getDateTime(companyExcelModel.getCreated()));
		companyModel.setTurnOverValue(companyExcelModel.getTurnOverValue());
		companyModel.setProcessAuditStatus(companyExcelModel.getProcessAuditStatus());
		companyModel.setType(companyExcelModel.getType());
		companyModel.setTitle(companyExcelModel.getTitle());
		companyModel.setDeleted(companyExcelModel.isDeleted());
		companyModel.setWebsiteUrl(CommonUtils.getSplitString(companyExcelModel.getWebsiteUrl(), SPLIT_BY_COMMA));
		companyModel.setPhone(CommonUtils.getPhones(companyExcelModel.getPhone()));
		companyModel.setPageUrl(companyExcelModel.getPageUrl());
		companyModel.setFinancialAuditStatus(companyExcelModel.getFinancialAuditStatus());
		companyModel.setCategory(companyExcelModel.getCategory());
		companyModel.setStaffMembers(companyExcelModel.getStaffMembers());
		companyModel.setBusinessType(companyExcelModel.getBusinessType());
		companyModel.setFollower(companyExcelModel.isFollower());
		companyModel.setAdmins(CommonUtils.getSplitString(companyExcelModel.getAdmins(), SPLIT_BY_COMMA));
		companyModel.setDesc(companyExcelModel.getDesc());
		companyModel.setAddressCords(companyExcelModel.getAddressCords());
		companyModel.setRating(companyExcelModel.getRating());
		companyModel.setBoardingFacility(companyExcelModel.getBoardingFacility());
		companyModel.setPlacementAssistance(companyExcelModel.getPlacementAssistance());
		companyModel.setFinancialAssistance(companyExcelModel.getFinancialAssistance());
		companyModel.setInfraFacility(companyExcelModel.getInfraFacility());
		companyModel.setBanner(getBanner(companyExcelModel));
		
		return companyModel;
	}

	private static Banner getBanner(CompanyExcelModel companyExcelModel) {
		Banner banner = new Banner();
		banner.setDefaultImage(companyExcelModel.isBannerDefultImage());
		banner.setDefaultPath(companyExcelModel.getBannerDefaultPath());
		return banner;
	}

	private static DateTime getDateTime(String created) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime dt = formatter.parseDateTime(created);
		return dt;
	}
}
