package com.master.talent.api.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.master.talent.api.models.ChatRequest;
import com.master.talent.api.models.GetChat;

@Path("/")
public interface ChatService {

	@POST
    @Path("/")
    @Consumes("application/json")
    @Produces("application/json")
    public Response createMessage(@Context HttpHeaders headers, ChatRequest chat);
	
	@POST
    @Path("/group/{action}/{username}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateGroup(@Context HttpHeaders headers, @PathParam("action") String action, @PathParam("username") String username, ChatRequest chat);
	
	@GET
    @Path("/")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getAllChats(@Context HttpHeaders headers);
	
	@POST
    @Path("/single")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getIndiviualChats(@Context HttpHeaders headers, GetChat getChat);
	
	@POST
    @Path("/delete")
	@Consumes("application/json")
    @Produces("application/json")
    public Response deleteMessage(@Context HttpHeaders headers, GetChat getChat);
	

}