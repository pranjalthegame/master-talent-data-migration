package com.master.talent.api.entity.repo;


import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.ShortListEntity;


@Repository
public interface ShortlistEntityRepo extends CouchbaseRepository<ShortListEntity, String>
{
}

