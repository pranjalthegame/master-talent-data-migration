package com.master.talent.data.model;

public class Employment {
	
	private String type;
	private String category;
	private String expertise;
	private String workStatus;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getExpertise() {
		return expertise;
	}
	public void setExpertise(String expertise) {
		this.expertise = expertise;
	}
	public String getWorkStatus() {
		return workStatus;
	}
	public void setWorkStatus(String workStatus) {
		this.workStatus = workStatus;
	}
	@Override
	public String toString() {
		return "Employment [type=" + type + ", category=" + category
				+ ", expertise=" + expertise + ", workStatus=" + workStatus
				+ "]";
	}
}
