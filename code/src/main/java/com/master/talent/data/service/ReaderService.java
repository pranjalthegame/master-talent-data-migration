/**
 * 
 */
package com.master.talent.data.service;

import java.util.List;

import com.master.talent.data.model.excel.CommentExcelModel;
import com.master.talent.data.model.excel.CompanyExcelModel;
import com.master.talent.data.model.excel.DiscussionExcelModel;
import com.master.talent.data.model.excel.UserExcelModel;

/**
 * @author pranjal.sinha
 *
 */
public interface ReaderService {
	
	List<UserExcelModel> readUserFeedFile(String fileName);
	
	List<DiscussionExcelModel> readDiscussionFeedFile(String fileName);
	
	List<CompanyExcelModel> readCompanyFeedFile(String fileName);
	
	List<CommentExcelModel> readCommentFeedFile(String fileName);
}
