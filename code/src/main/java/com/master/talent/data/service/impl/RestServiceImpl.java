/**
 * 
 */
package com.master.talent.data.service.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.master.talent.data.model.CommentModel;
import com.master.talent.data.model.CompanyModel;
import com.master.talent.data.model.DiscussionModel;
import com.master.talent.data.model.ResponseObject;
import com.master.talent.data.model.UserModel;
import com.master.talent.data.service.RestService;
import com.master.talent.data.util.JsonUtils;

/**
 * @author pranjal.sinha
 *
 */
public class RestServiceImpl implements RestService {
	
	private static final Logger LOG = LoggerFactory.getLogger(RestServiceImpl.class);
	private Map<String,String> serviceUrlMap;
	
	public RestServiceImpl(Map<String,String> serviceUrlMap){
		this.serviceUrlMap = serviceUrlMap;
	}

	@Override
	public ResponseObject processUserOverHttp(UserModel data) {
		return processRequest(serviceUrlMap.get("user"), data);
	}

	@Override
	public ResponseObject processCompanyOverHttp(CompanyModel data) {
		return processRequest(serviceUrlMap.get("company"), data);
	}

	@Override
	public ResponseObject processDiscussionOverHttp(DiscussionModel data) {
		return processRequest(serviceUrlMap.get("discussion"), data);
	}
	

	@Override
	public ResponseObject processCommentOverHttp(CommentModel data) {
		return processRequest(serviceUrlMap.get("comment"), data);
	}
	
	private ResponseObject processRequest(String url,Object data){
		try{
			HttpPost postRequest = new HttpPost(url);
			String dataString = JsonUtils.toJson(data);
			postRequest.setEntity(new StringEntity(dataString));
			LOG.info("Initiating data push request, url is:{"+url+"}, request body: "+dataString);
			HttpResponse response = hit(postRequest);
			return logResponse(response);
		}catch(Exception e){
			return new ResponseObject(500,writeExceptionAsString(e));
		}
	}

	private ResponseObject logResponse(HttpResponse response) throws ParseException, IOException {
		int responseCode = response.getStatusLine().getStatusCode();
		String responseBody = EntityUtils.toString(response.getEntity());
		return new ResponseObject(responseCode,responseBody);
	}

	private HttpResponse hit(HttpPost postRequest) throws ClientProtocolException, IOException{
		postRequest.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		return getHttpClient().execute(postRequest);
	}

	private HttpClient getHttpClient(){
		return HttpClientBuilder.create().build();
	}
	
	private String writeExceptionAsString(Exception e){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		try{
			e.printStackTrace(pw);
			return sw.toString(); // stack trace as a string
		}finally{
			pw.close();
			try {
				sw.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				LOG.error("Unexpected Exception: ",e);
			}
		}
	
	}

	
}
