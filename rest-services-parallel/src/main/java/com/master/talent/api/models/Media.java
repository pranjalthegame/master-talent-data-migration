package com.master.talent.api.models;

import java.util.List;


public class Media {

	private String desc;
	private String videoLink;
	private List<String> photoPath;

	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	public List<String> getPhotoPath() {
		return photoPath;
	}
	public void setPhotoPath(List<String> photoPath) {
		this.photoPath = photoPath;
	}
	@Override
	public String toString() {
		return "Discussion [desc=" + desc + ", videoLink="
				+ videoLink + ", photoPath=" + photoPath + "]";
	}
}
