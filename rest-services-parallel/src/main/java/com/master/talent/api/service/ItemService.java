/**
 * Created by Apache CXF WadlToJava code generator
**/
package com.master.talent.api.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.master.talent.api.entity.CommentEntity;
import com.master.talent.api.entity.ItemEntity;
import com.master.talent.api.models.ItemShare;

@Path("/")
public interface ItemService {
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/")
	Response createItem(@Context HttpHeaders headers, ItemEntity obj);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/")
	Response updateItem(@Context HttpHeaders headers, ItemEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/{itemId}")
	Response getItem(@Context HttpHeaders headers, @PathParam("itemId") String itemId);
		
	@DELETE
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/{itemId}")
	Response deleteItem(@Context HttpHeaders headers, @PathParam("itemId") String itemId);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/comment")
	Response createComment(@Context HttpHeaders headers, CommentEntity obj);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/comment")
	Response updateComment(@Context HttpHeaders headers, CommentEntity obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/comment/{itemId}")
	Response getComment(@Context HttpHeaders headers, @PathParam("itemId") String itemId, @QueryParam("from") int from, @QueryParam("to") int to);
	
	@DELETE
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/comment/{commentId}")
	Response deleteComment(@Context HttpHeaders headers, @PathParam("commentId") String commentId);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/share")
	Response shareItem(@Context HttpHeaders headers, ItemShare obj);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/like/{itemId}")
	Response likeItem(@Context HttpHeaders headers, @PathParam("itemId") String itemId);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/shortlist/{itemId}")
	Response shortlistItem(@Context HttpHeaders headers, @PathParam("itemId") String itemId);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/apply/{itemId}")
	Response applyItem(@Context HttpHeaders headers, @PathParam("itemId") String itemId);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/itemView/{itemId}")
	Response itemViewed(@Context HttpHeaders headers, @PathParam("itemId") String itemId);
}