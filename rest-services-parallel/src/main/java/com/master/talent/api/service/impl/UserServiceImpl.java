package com.master.talent.api.service.impl;

import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.master.talent.api.dao.SearchDao;
import com.master.talent.api.dao.UserDao;
import com.master.talent.api.entity.CompanyEntity;
import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.entity.UserAwardsAndRecogEntity;
import com.master.talent.api.entity.UserBankDetailsEntity;
import com.master.talent.api.entity.UserEarningEntity;
import com.master.talent.api.entity.UserEducationEntity;
import com.master.talent.api.entity.UserLocationEntity;
import com.master.talent.api.entity.UserOccupationEntity;
import com.master.talent.api.entity.UserPreSignUpEntity;
import com.master.talent.api.entity.UserSkillsEntity;
import com.master.talent.api.entity.repo.CompanyEntityRepo;
import com.master.talent.api.models.Login;
import com.master.talent.api.models.User;
import com.master.talent.api.service.UserService;
import com.master.talent.api.utils.EmailUtility;
import com.master.talent.api.utils.IndexUtility;
import com.master.talent.api.utils.SpringPropertiesUtil;

@CrossOriginResourceSharing(
        allowOrigins = {
           "*"
        }, 
        allowCredentials = true, 
        maxAge = 1, 
        allowHeaders = {
           "MTUser"
        }
)
public class UserServiceImpl implements UserService {
	
	private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	private final Gson serializer = new Gson();
	
	@Autowired
	private UserDao dao;
	
	@Autowired
	private SearchDao searchDao;
	
	@Autowired
	private CompanyEntityRepo companyEntityRepo;
	
	@Autowired
	private IndexUtility indexUtility;
	
	@Autowired
	private EmailUtility emailUtility;

	@Autowired
	private  RequestValidator validator;

	@Override
	public Response createUserPreSignup(HttpHeaders headers, UserPreSignUpEntity obj) {
//		validator.validateBefore(headers);
//		
//		if(obj.getReferral() ==null){
//			if(obj.getReferral().isEmpty()){
//				//get the reffereer if any
//				obj.setReferral(indexUtility.getReferral(obj.getEmailAddress()));
//			}
//		}
		
		obj.setEmailAddress(obj.getEmailAddress().toLowerCase());
		//appending random number in username to denote it as test user
		obj.setUsername(obj.getUsername().toLowerCase()+"11111");
		UserPreSignUpEntity temp =dao.getUserByEmail(obj.getEmailAddress());
		if(temp == null){
			UserPreSignUpEntity response = dao.createUserPreSignUpEntity(obj);
			response.setPassword("xxxx");
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		} else {
			return Response.ok("{\"error\" : \"Email Already Exists\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
	}
	
	@Override
	public Response loginUser(HttpHeaders headers, Login obj) {
		validator.validateBefore(headers);
		UserPreSignUpEntity userPreSignUpEntity;
		String temp;
		
		if(obj.getUsername().contains("@")){
			userPreSignUpEntity =dao.getUserByEmail(obj.getUsername());
			if(userPreSignUpEntity!=null){
				byte[] decoded = Base64.decodeBase64(userPreSignUpEntity.getPassword());  
				temp = new String(decoded); 
			}else{
				temp = "";
			}
		} else {
			userPreSignUpEntity =dao.getUserPreSignUpEntityForLogin(obj.getUsername());
			if(userPreSignUpEntity!=null){
				byte[] decoded = Base64.decodeBase64(userPreSignUpEntity.getPassword());  
				temp = new String(decoded); 
			}else{
				temp = "";
			}
		}
		if (temp.equals(obj.getPassword())){
			return Response.ok("{\"success\" : \"valid login\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}else{
			return Response.ok("{\"error\" : \"invalid login\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
		
	}
	
	@Override
	public Response getUserPresignByEmail(HttpHeaders headers, String emailId){
		validator.validateBefore(headers);
		UserPreSignUpEntity response =dao.getUserByEmail(emailId);
		response.setPassword("xxxx");
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response updatePreSignup(HttpHeaders headers, UserPreSignUpEntity obj) {
		validator.validateBefore(headers);
		if(obj.getPassword()!=null){
			byte[] encoded = Base64.encodeBase64(obj.getPassword().getBytes());  
			obj.setPassword(new String(encoded));	
		}
		if(obj.getEmailAddress()!=null){
			UserPreSignUpEntity userPreSignUpEntity =dao.getUserByEmail(obj.getEmailAddress());
			if(userPreSignUpEntity!=null){
				return Response.ok("{\"error\" : \"Email Already Exist\"}", MediaType.APPLICATION_JSON)
		        		.header("Access-Control-Allow-Origin", "*")
		 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		 	            .header("Access-Control-Allow-Credentials", "true")
		 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		 	            .header("Access-Control-Max-Age", "1209600")
		 				.build();
			}
		}
		UserPreSignUpEntity response = dao.updateUserPreSignUpEntity(obj);
		response.setPassword("xxxx");
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response getUserPresignUp(HttpHeaders headers, String username) {
		SessionEntity sessionEntity =validator.validateAndReturn(headers);
		UserPreSignUpEntity response = dao.getUserPreSignUpEntity(username);
		if(!sessionEntity.getUsername().equalsIgnoreCase(username)){
			response.setFriend(searchDao.verifyAFirend(sessionEntity.getUsername(), username));
			response.setFollower(searchDao.verifyAFollower(sessionEntity.getUsername(), username));
			if(!response.isFriend()){
				response.setPendingFriend(searchDao.verifyAPendingFirend(sessionEntity.getUsername(), username));
				response.setRequestedPendingFriend(searchDao.verifyARequestedPendingFirend(sessionEntity.getUsername(), username));
		}	}
		response.setPassword("xxxx");
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}
	
	@Override
	public Response getUserByUserType(HttpHeaders headers, String userType, int start, int end) {
		SessionEntity sessionEntity =validator.validateAndReturn(headers);
		List<User> response = dao.getUserPreSignUpByType(userType, start, end);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response deleteUserPresignUp(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		dao.deleteUserPreSignUpEntity(username);
		return Response.ok(SpringPropertiesUtil.getProperty("delete.success.json.message"), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response createUserSkills(HttpHeaders headers, UserSkillsEntity obj) {
		validator.validateBefore(headers);
		UserSkillsEntity response = dao.createUserSkills(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response updateUserSkills(HttpHeaders headers, UserSkillsEntity obj) {
		validator.validateBefore(headers);
		UserSkillsEntity response = dao.updateUserSkills(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response getUserUserSkills(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		UserSkillsEntity response = dao.getUserUserSkills(username);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response deleteUserSkills(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		dao.deleteUserSkills(username);
		return Response.ok(SpringPropertiesUtil.getProperty("delete.success.json.message"), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response createUserOccupation(HttpHeaders headers, UserOccupationEntity obj) {
		validator.validateBefore(headers);
		UserOccupationEntity response = dao.createUserOccupation(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response updateUserOccupation(HttpHeaders headers, UserOccupationEntity obj) {
		validator.validateBefore(headers);
		UserOccupationEntity response = dao.updateUserOccupation(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response getUserUserOccupation(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		UserOccupationEntity response = dao.getUserUserOccupation(username);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response deleteUserOccupation(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		dao.deleteUserOccupation(username);
		return Response.ok(SpringPropertiesUtil.getProperty("delete.success.json.message"), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response createUserEducation(HttpHeaders headers, UserEducationEntity obj) {
		validator.validateBefore(headers);
		UserEducationEntity response = dao.createUserEducation(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response updateUserEducation(HttpHeaders headers, UserEducationEntity obj) {
		validator.validateBefore(headers);
		UserEducationEntity response = dao.updateUserEducation(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response getUserEducation(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		UserEducationEntity response = dao.getUserEducation(username);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response deleteUserEducation(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		dao.deleteUserEducation(username);
		return Response.ok(SpringPropertiesUtil.getProperty("delete.success.json.message"), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response createUserAwardsAndRecog(HttpHeaders headers, UserAwardsAndRecogEntity obj) {
		validator.validateBefore(headers);
		UserAwardsAndRecogEntity response = dao.createUserAwardsAndRecog(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response updateUserAwardsAndRecog(HttpHeaders headers, UserAwardsAndRecogEntity obj) {
		validator.validateBefore(headers);
		UserAwardsAndRecogEntity response = dao.updateUserAwardsAndRecog(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response getUserAwardsAndRecog(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		UserAwardsAndRecogEntity response = dao.getUserAwardsAndRecog(username);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response deleteUserAwardsAndRecog(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		dao.deleteUserAwardsAndRecog(username);
		return Response.ok(SpringPropertiesUtil.getProperty("delete.success.json.message"), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response verifyOtp(HttpHeaders headers, String username, String otp) {
		validator.validateBefore(headers);
		boolean status = dao.validateOTP(username, otp);
		if(status){
			UserPreSignUpEntity response = dao.getUserPreSignUpEntity(username);
			response.setActive(true);
			response.setRegisterLevel("DONE");
			emailUtility.sendWelcome(response.getEmailAddress(), response.getFullname());
			dao.updateUserPreSignUpEntity(response);
			return Response.ok(SpringPropertiesUtil.getProperty("otp.success.json.message"), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}else{
			return Response.ok(SpringPropertiesUtil.getProperty("otp.failed.json.message"), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
	}
	
	@Override
	public Response verifyUserName(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		UserPreSignUpEntity response = dao.getUserPreSignUpEntity(username);
		if(response!=null){
			return Response.ok(SpringPropertiesUtil.getProperty("username.valid.json.message"), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}else {
			return Response.ok(SpringPropertiesUtil.getProperty("username.invalid.json.message"), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
	}

	@Override
	public Response requestFriend(HttpHeaders headers, String requested) {
		SessionEntity session = validator.validateAndReturn(headers);
		boolean verify = searchDao.verifyAFirend(session.getUsername(), requested);
		boolean verifyFriend = searchDao.verifyAPendingFirend(session.getUsername(), requested);	
		boolean verifyOtherFriend = searchDao.verifyAPendingFirend(requested, session.getUsername());	
		boolean verifyRequestFriend = searchDao.verifyARequestedPendingFirend(requested, session.getUsername());	
		boolean verifyOtherRequestFriend = searchDao.verifyARequestedPendingFirend(session.getUsername(), requested);	
		
		if(!verify){
			if(!verifyFriend && !verifyOtherFriend){
				if(!verifyRequestFriend && !verifyOtherRequestFriend){
					boolean response = dao.addFriend(session.getUsername(), requested);
					if(response){
						return Response.ok("{\"success\" : \"pending friend list updated\"}", MediaType.APPLICATION_JSON)
				        		.header("Access-Control-Allow-Origin", "*")
				 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				 	            .header("Access-Control-Allow-Credentials", "true")
				 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				 	            .header("Access-Control-Max-Age", "1209600")
				 				.build();
					} else {
						return Response.ok("{\"error\" : \"pending friend list update failed\"}", MediaType.APPLICATION_JSON)
				        		.header("Access-Control-Allow-Origin", "*")
				 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				 	            .header("Access-Control-Allow-Credentials", "true")
				 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				 	            .header("Access-Control-Max-Age", "1209600")
				 				.build();
					}	
				}				
			} 
		}
		return Response.ok("{\"error\" : \"already friends\"}", MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response acceptFriendRequest(HttpHeaders headers, String requestor) {
		SessionEntity session = validator.validateAndReturn(headers);
		boolean response = dao.acceptFriend(session.getUsername(), requestor);
		if(response){
			return Response.ok("{\"success\" : \"friend list updated\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		} else {
			return Response.ok("{\"error\" : \"friend list update failed\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}			
	}

	@Override
	public Response deleteFriendRequest(HttpHeaders headers, String deleteUsername) {
		SessionEntity session = validator.validateAndReturn(headers);
		boolean verify = searchDao.verifyAFirend(session.getUsername(), deleteUsername);
		if(verify){
			boolean response = dao.deleteFriend(session.getUsername(), deleteUsername);
			if(response){
				return Response.ok("{\"success\" : \"friend list updated\"}", MediaType.APPLICATION_JSON)
		        		.header("Access-Control-Allow-Origin", "*")
		 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		 	            .header("Access-Control-Allow-Credentials", "true")
		 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		 	            .header("Access-Control-Max-Age", "1209600")
		 				.build();
			} else {
				return Response.ok("{\"error\" : \"friend list update failed\"}", MediaType.APPLICATION_JSON)
		        		.header("Access-Control-Allow-Origin", "*")
		 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		 	            .header("Access-Control-Allow-Credentials", "true")
		 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		 	            .header("Access-Control-Max-Age", "1209600")
		 				.build();
			}	
		}else{
			return Response.ok("{\"error\" : \"invalid request not a valid friend\"}", MediaType.APPLICATION_JSON)
    		.header("Access-Control-Allow-Origin", "*")
			.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
			.build();
		}
	}

	@Override
	public Response getAllFriends(HttpHeaders headers, String username, int start, int end) {
		validator.validateBefore(headers);
		Object response = searchDao.getAllFriendsForUser(username ,start, end);
		if(response!=null){
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		} else {
			return Response.ok("{\"success\" : \"no friends for user found\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}	
	}

	@Override
	public Response requestFollow(HttpHeaders headers, String followed) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{
			boolean verify = searchDao.verifyAFollower(followed, session.getUsername());
			if(!verify){
				dao.addFollower(session.getUsername(), followed);
				return Response.ok("{\"success\" : \"added to followers list\"}", MediaType.APPLICATION_JSON)
		        		.header("Access-Control-Allow-Origin", "*")
		 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		 	            .header("Access-Control-Allow-Credentials", "true")
		 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		 	            .header("Access-Control-Max-Age", "1209600")
		 				.build();
			} else {
				dao.unFollow(session.getUsername(), followed);
				return Response.ok("{\"success\" : \"removed from followers list\"}", MediaType.APPLICATION_JSON)
		        		.header("Access-Control-Allow-Origin", "*")
		 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		 	            .header("Access-Control-Allow-Credentials", "true")
		 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		 	            .header("Access-Control-Max-Age", "1209600")
		 				.build();
			}
		} catch(Exception e){
			return Response.ok("{\"error\" : \"update failed\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
	}
	
	@Override
	public Response requestCompanyFollow(HttpHeaders headers, String compId) {
		SessionEntity session = validator.validateAndReturn(headers);
		try{
			boolean verify = searchDao.verifyACompanyFollower(compId, session.getUsername());
			if(!verify){
				dao.addCompanyFollower(session.getUsername(), compId);
				return Response.ok("{\"success\" : \"added to followers list\"}", MediaType.APPLICATION_JSON)
		        		.header("Access-Control-Allow-Origin", "*")
		 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		 	            .header("Access-Control-Allow-Credentials", "true")
		 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		 	            .header("Access-Control-Max-Age", "1209600")
		 				.build();
			} else {
				dao.unCompanyFollow(session.getUsername(), compId);
				return Response.ok("{\"success\" : \"removed from followers list\"}", MediaType.APPLICATION_JSON)
		        		.header("Access-Control-Allow-Origin", "*")
		 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		 	            .header("Access-Control-Allow-Credentials", "true")
		 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		 	            .header("Access-Control-Max-Age", "1209600")
		 				.build();
			}
		} catch(Exception e){
			return Response.ok("{\"error\" : \"update failed\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
	}

	@Override
	public Response getAllFollowers(HttpHeaders headers, String username, int start, int end) {
		validator.validateBefore(headers);
		Object response = searchDao.getAllFollowersForUser(username, start, end);
		if(response!=null){
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		} else {
			return Response.ok("{\"success\" : \"no folowers for user found\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}	
	}
	
	@Override
	public Response getCompanyFollowers(HttpHeaders headers, String compId, int start, int end) {
		validator.validateBefore(headers);
		Object response = searchDao.getAllFollowersForCompany(compId, start, end);
		if(response!=null){
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		} else {
			return Response.ok("{\"success\" : \"no folowers for user found\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}	
	}
	
	@Override
	public Response createUserLocation(HttpHeaders headers, UserLocationEntity obj) {
		validator.validateBefore(headers);
		UserLocationEntity temp =dao.getUserLocation(obj.getUsername());
		if(temp == null){
			UserLocationEntity response = dao.createUserLocation(obj);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		} else {
			return Response.ok("{\"error\" : \"Location for the user exists\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
	}

	@Override
	public Response updateUserLocation(HttpHeaders headers, UserLocationEntity obj) {
		validator.validateBefore(headers);
		UserLocationEntity response = dao.updateUserLocation(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response getUserLocation(HttpHeaders headers, String username) {
		validator.validateBefore(headers);
		UserLocationEntity response =dao.getUserLocation(username);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}
	
	@Override
	public Response verifyCompanyFriendlyId(HttpHeaders headers, String friendlyId) {
		SessionEntity sess = validator.validateAndReturn(headers);
		
		if(searchDao.verifyFriendlyId(friendlyId)){
			return Response.ok("{\"success\" : \"Url Already Exist\"}", MediaType.APPLICATION_JSON)
    		.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
		}
		return Response.ok("{\"error\" : \"Url Doesnot Exist\"}", MediaType.APPLICATION_JSON)
		.header("Access-Control-Allow-Origin", "*")
			.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
			.build();		
	}

	@Override
	public Response createCompany(HttpHeaders headers, CompanyEntity obj) {
/*		SessionEntity sess = validator.validateAndReturn(headers);
		
		if(searchDao.verifyFriendlyId(obj.getPageUrl())){
			return Response.ok("{\"error\" : \"Url Already Exist\"}", MediaType.APPLICATION_JSON)
    		.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	            .header("Access-Control-Max-Age", "1209600")
				.build();
		}*/
		
		CompanyEntity response =dao.createCompany(obj.getOwner(), obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
	}

	@Override
	public Response updateCompany(HttpHeaders headers, CompanyEntity obj) {
		SessionEntity sess = validator.validateAndReturn(headers);		
		CompanyEntity companyEntity = companyEntityRepo.findOne(obj.getPageId());					
		if(companyEntity != null){
			if(obj.getPageUrl()!=null){
				if(!companyEntity.getPageUrl().equals(obj.getPageUrl())) {
					if(searchDao.verifyFriendlyId(obj.getPageUrl())){
						Response.ok("{\"error\" : \"Url Already Exist\"}", MediaType.APPLICATION_JSON)
			    		.header("Access-Control-Allow-Origin", "*")
							.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				            .header("Access-Control-Allow-Credentials", "true")
				            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				            .header("Access-Control-Max-Age", "1209600")
							.build();
					}
				}				
			}
			
			CompanyEntity response =dao.updateCompany(sess.getUsername(), companyEntity, obj);
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
		}else{
			return Response.ok("{\"error\" : \"Invalid Company update, Does not exist\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
	}

	@Override
	public Response getCompany(HttpHeaders headers, String companyId) {
		SessionEntity sess = validator.validateAndReturn(headers);
		CompanyEntity response = companyEntityRepo.findOne(companyId);
		response.setFollower(searchDao.verifyACompanyFollower(companyId, sess.getUsername()));
		if(response != null){
			String[] temp = indexUtility.getCompanyReview(companyId).split("/");
			response.setRating(temp[0]);
			
			return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
		}else{
			return Response.ok("{\"error\" : \"Company does not exist\"}", MediaType.APPLICATION_JSON)
	        		.header("Access-Control-Allow-Origin", "*")
	 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	 	            .header("Access-Control-Allow-Credentials", "true")
	 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	 	            .header("Access-Control-Max-Age", "1209600")
	 				.build();
		}
	}
	
	@Override
	public Response getCompanyByFriendly(HttpHeaders headers, String friendlyId) {
		SessionEntity sess = validator.validateAndReturn(headers);
		CompanyEntity response = searchDao.getCompanyFriendlyId(friendlyId);
		if(response !=null ){
			String[] temp = indexUtility.getCompanyReview(response.getPageId()).split("/");
			response.setRating(temp[0]);
		}
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
			.header("Access-Control-Allow-Origin", "*")
			.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	        .header("Access-Control-Allow-Credentials", "true")
	        .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	        .header("Access-Control-Max-Age", "1209600")
			.build();
	}
	
	@Override
	public Response getAllCompany(HttpHeaders headers, int start, int end) {
		SessionEntity sess = validator.validateAndReturn(headers);
		List<CompanyEntity> response = dao.getAllCompanyByUser(sess.getUsername(), start, end);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
    		.header("Access-Control-Allow-Origin", "*")
			.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
			.build();
	}
	
	@Override
	public Response getAllEarnings(HttpHeaders headers, String type) {
		SessionEntity sess = validator.validateAndReturn(headers);
		List<UserEarningEntity> response = dao.getAllEarnings(sess.getUsername(), type);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
    		.header("Access-Control-Allow-Origin", "*")
			.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .header("Access-Control-Max-Age", "1209600")
			.build();
	}
	
	@Override
	public Response createBankDetails(HttpHeaders headers, UserBankDetailsEntity obj) {
		SessionEntity session = validator.validateAndReturn(headers);
		
		obj.setUsername(session.getUsername());
		UserBankDetailsEntity response = dao.createBankDetails(obj);
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
        		.header("Access-Control-Allow-Origin", "*")
 				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
 	            .header("Access-Control-Allow-Credentials", "true")
 	            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
 	            .header("Access-Control-Max-Age", "1209600")
 				.build();
		
	}
	
	@Override
	public Response getBankDetails(HttpHeaders headers) {
		SessionEntity session = validator.validateAndReturn(headers);
		UserBankDetailsEntity response = dao.getBankDetails(session.getUsername());
		return Response.ok(serializer.toJson(response), MediaType.APPLICATION_JSON)
			.header("Access-Control-Allow-Origin", "*")
			.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
			.header("Access-Control-Allow-Credentials", "true")
			.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			.header("Access-Control-Max-Age", "1209600")
			.build();
	}
	
}
