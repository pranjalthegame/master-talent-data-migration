package com.master.talent.api.entity;


import java.util.List;

import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;
import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Chat;

@Document(expiry=0)
public class ChatEntity
{

	@Id
	@Field
	private String Id;
	
	@Field
    @NotNull
    private DateTime created;
	
	@Field
    @NotNull
    private DateTime updated;
	
	@Field
	private List<Chat> chat;
	
	@Field
    private boolean read;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public DateTime getUpdated() {
		return updated;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}	

	public List<Chat> getChat() {
		return chat;
	}

	public void setChat(List<Chat> chat) {
		this.chat = chat;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	@Override
	public String toString() {
		return "ChatEntity [Id=" + Id + ", created=" + created + ", updated="
				+ updated + ", chat=" + chat + "]";
	}	
}
