/**
 * 
 */
package com.master.talent.data.service;

import com.master.talent.data.model.CommentModel;
import com.master.talent.data.model.CompanyModel;
import com.master.talent.data.model.DiscussionModel;
import com.master.talent.data.model.ResponseObject;
import com.master.talent.data.model.UserModel;

/**
 * @author pranjal.sinha
 *
 */
public interface RestService{


	ResponseObject processUserOverHttp(UserModel data);

	ResponseObject processCompanyOverHttp(CompanyModel data);

	ResponseObject processDiscussionOverHttp(DiscussionModel data);
	
	ResponseObject processCommentOverHttp(CommentModel data);
}
