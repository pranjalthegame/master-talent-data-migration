package com.master.talent.api.entity;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Followers;

@Document(expiry=0)
public class UserFollowersEntity {
	
	public static final String ENTITY_NAME = "FOLLOWERS-";
	
	@Id
	private String key;
	
	@Field
	private String username;
	
	@Field
	private List<Followers> followers;
	
	@Field
	private List<Followers> followee;
	
	@Field
	private List<Followers> companies;
	

	String getKey() {
		return key;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
		this.key = ENTITY_NAME+username;
	}

	
	public List<Followers> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Followers> companies) {
		this.companies = companies;
	}

	public List<Followers> getFollowers() {
		return followers;
	}

	public void setFollowers(List<Followers> followers) {
		this.followers = followers;
	}

	public List<Followers> getFollowee() {
		return followee;
	}

	public void setFollowee(List<Followers> followee) {
		this.followee = followee;
	}

	@Override
	public String toString() {
		return "UserOccupationEntity [username=" + username + ", followers="
				+ followers + "]";
	}

}
