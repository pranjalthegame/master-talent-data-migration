package com.master.talent.api.entity;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.AwardAndRecog;

@Document(expiry=0)
public class UserAwardsAndRecogEntity {
	
	public static final String ENTITY_NAME = "AWARDRECOG-";
	
	@Id
	private String key;
	
	@Field
	private String username;
	
	@Field
	private List<AwardAndRecog> awards;
	
	@Field
	private boolean isDeleted;

	public String getKey() {
		return key;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		this.key = ENTITY_NAME+username;
	}

	public List<AwardAndRecog> getAwards() {
		return awards;
	}

	public void setAwards(List<AwardAndRecog> awards) {
		this.awards = awards;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "UserAwarsAndRecogEntity [username=" + username
				+ ", awards=" + awards + "]";
	}

}
