package com.master.talent.data.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.master.talent.data.constants.AppConstants;
import com.master.talent.data.service.CommentFeedService;
import com.master.talent.data.service.CompanyFeedService;
import com.master.talent.data.service.DiscussionFeedService;
import com.master.talent.data.service.MigrationService;
import com.master.talent.data.service.ReaderService;
import com.master.talent.data.service.RestService;
import com.master.talent.data.service.UserFeedService;
import com.master.talent.data.service.impl.CommentFeedServiceImpl;
import com.master.talent.data.service.impl.CompanyFeedServiceImpl;
import com.master.talent.data.service.impl.DiscussionFeedServiceImpl;
import com.master.talent.data.service.impl.MigrationServiceImpl;
import com.master.talent.data.service.impl.ReaderServiceImpl;
import com.master.talent.data.service.impl.RestServiceImpl;
import com.master.talent.data.service.impl.UserFeedServiceImpl;

@Configuration
@ComponentScan(basePackages="com.master.talent.data.*")
public class AppConfig {
	
	@Bean(name="migrationService")
    public MigrationService migrationService() {
        return new MigrationServiceImpl();
    }
	
	@Bean(name="readerService")
    public ReaderService readerService() {
        return new ReaderServiceImpl();
    }
	
	@Bean(name="restService")
	public RestService restService(){
		return new RestServiceImpl(initServiceUrlMap());
	}
	
	@Bean(name="userFeedService")
	public UserFeedService userFeedService(){
		return new UserFeedServiceImpl();
	}
	
	@Bean(name="companyFeedService")
	public CompanyFeedService companyFeedService(){
		return new CompanyFeedServiceImpl();
	}
	
	@Bean(name="discussionFeedService")
	public DiscussionFeedService discussionFeedService(){
		return new DiscussionFeedServiceImpl();
	}
	
	@Bean(name="commentFeedService")
	public CommentFeedService commentFeedService(){
		return new CommentFeedServiceImpl();
	}
	
	private Map<String,String> initServiceUrlMap(){
		Map<String,String> serviceUrlMap = new HashMap<>();
		serviceUrlMap.put("user", formUserUrl());
		serviceUrlMap.put("discussion", formDiscussionUrl());
		serviceUrlMap.put("company", formCompanyUrl());
		serviceUrlMap.put("comment", formCommentUrl());
		return serviceUrlMap;
	}
	
	private String formUserUrl(){
		return baseUrl() + AppConstants.USER_SERVICE_PORT + AppConstants.USER_SERVICE_ENDPOINT;
	}
	
	private String formDiscussionUrl(){
		return baseUrl() + AppConstants.DISCUSSION_SERVICE_PORT + AppConstants.DISCUSSION_SERVICE_ENDPOINT;
	}
	
	private String formCompanyUrl(){
		return baseUrl() + AppConstants.COMPANY_SERVICE_PORT + AppConstants.COMPANY_SERVICE_ENDPOINT;
	}
	
	private String formCommentUrl(){
		return baseUrl() + AppConstants.COMMENT_SERVICE_PORT + AppConstants.COMMENT_SERVICE_ENDPOINT;
	}
	
	private String baseUrl(){
		return AppConstants.PROTOCOL + AppConstants.HOSTNAME + ":";
	}
}