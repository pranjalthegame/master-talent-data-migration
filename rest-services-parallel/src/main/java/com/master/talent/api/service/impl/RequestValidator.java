package com.master.talent.api.service.impl;

import javax.ws.rs.core.HttpHeaders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.master.talent.api.entity.SessionEntity;
import com.master.talent.api.exception.UnAuthorisedException;
import com.master.talent.api.exception.base.ExceptionCodes;
import com.master.talent.api.service.SessionService;
import com.master.talent.api.utils.SpringPropertiesUtil;

@Component
public class RequestValidator {
	
	@Autowired
	private SessionService sessionService;
	
	private final Logger logger = LoggerFactory.getLogger(RequestValidator.class);
	
	public void validateBefore(HttpHeaders headers) throws UnAuthorisedException {		
		if((headers.getRequestHeader(SpringPropertiesUtil.getProperty("master.talent.cookiename")) != null)){
			String userCookie = headers.getRequestHeader(SpringPropertiesUtil.getProperty("master.talent.cookiename")).get(0);
			if(userCookie !=null){
			    SessionEntity sess = sessionService.validateSession(userCookie);
		   		if(sess==null){
		   			throw new UnAuthorisedException(ExceptionCodes.UECodes.UE_SESSION_NULL,SpringPropertiesUtil.getProperty("unauthorised.exception.message"));
		   		}
			} else {
				throw new UnAuthorisedException(ExceptionCodes.UECodes.UE_COOKIE_NOT_FOUND,SpringPropertiesUtil.getProperty("unauthorised.exception.message"));
			}
		}else {
			throw new UnAuthorisedException(ExceptionCodes.UECodes.UE_HEADER_LIST_NULL,SpringPropertiesUtil.getProperty("unauthorised.exception.message"));
		}
		
	}
	
	public SessionEntity validateAndReturn(HttpHeaders headers) throws UnAuthorisedException {		
		if((headers.getRequestHeader(SpringPropertiesUtil.getProperty("master.talent.cookiename")) != null)){
			String userCookie = headers.getRequestHeader(SpringPropertiesUtil.getProperty("master.talent.cookiename")).get(0);
			if(userCookie !=null){
			    SessionEntity sess = sessionService.validateSession(userCookie);
		   		if(sess==null){
		   			throw new UnAuthorisedException(ExceptionCodes.UECodes.UE_SESSION_NULL,SpringPropertiesUtil.getProperty("unauthorised.exception.message"));
		   		} else {
				    return sess;
		   		}
			} else {
				throw new UnAuthorisedException(ExceptionCodes.UECodes.UE_COOKIE_NOT_FOUND,SpringPropertiesUtil.getProperty("unauthorised.exception.message"));
			}
		}else {
			throw new UnAuthorisedException(ExceptionCodes.UECodes.UE_HEADER_LIST_NULL,SpringPropertiesUtil.getProperty("unauthorised.exception.message"));
		}
		
	}
}
