package com.master.talent.api.models;

import java.util.Date;

import com.couchbase.client.java.repository.annotation.Field;



public class SearchUser {	
	
	private String username;
	private String brief;
	private String displayName;
	private String created;
	private Long profileViews;
	private int followCount;
	private int friendCount;
	private String phone;
	private String activeTime;
	
	private String location;
	private String country;
	private String workType;
	private String workExp;
	private String workStatus;
	private String rating;
	
	private boolean isFriend;
	private boolean isFollower;
	private boolean isPendingFriend;	
		
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Long getProfileViews() {
		return profileViews;
	}
	public void setProfileViews(Long profileViews) {
		this.profileViews = profileViews;
	}
	public int getFollowCount() {
		return followCount;
	}
	public void setFollowCount(int followCount) {
		this.followCount = followCount;
	}
	public int getFriendCount() {
		return friendCount;
	}
	public void setFriendCount(int friendCount) {
		this.friendCount = friendCount;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public boolean isFriend() {
		return isFriend;
	}
	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}
	public boolean isFollower() {
		return isFollower;
	}
	public void setFollower(boolean isFollower) {
		this.isFollower = isFollower;
	}
	public boolean isPendingFriend() {
		return isPendingFriend;
	}
	public void setPendingFriend(boolean isPendingFriend) {
		this.isPendingFriend = isPendingFriend;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getActiveTime() {
		return activeTime;
	}
	public void setActiveTime(String activeTime) {
		this.activeTime = activeTime;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getWorkType() {
		return workType;
	}
	public void setWorkType(String workType) {
		this.workType = workType;
	}
	public String getWorkExp() {
		return workExp;
	}
	public void setWorkExp(String workExp) {
		this.workExp = workExp;
	}
	public String getWorkStatus() {
		return workStatus;
	}
	public void setWorkStatus(String workStatus) {
		this.workStatus = workStatus;
	}
	@Override
	public String toString() {
		return "SearchUser [username=" + username + ", brief=" + brief
				+ ", displayName=" + displayName + ", created=" + created
				+ ", profileViews=" + profileViews + ", followCount="
				+ followCount + ", phone=" + phone + ", activeTime="
				+ activeTime + ", location=" + location + ", workType="
				+ workType + ", workExp=" + workExp + ", workStatus="
				+ workStatus + "]";
	}
}
