package com.master.talent.api.entity;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Purchases;

@Document(expiry=0)
public class UserPaidServicesEntity {
	
	public static final String ENTITY_NAME = "SERVICES-";
	
	@Id
	private String key;
	
	@Field
	private String username;
	
	@Field
	private List<Purchases> purchasess;
	
	public String getKey() {
		return key;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		this.key = ENTITY_NAME+username;
	}

	public List<Purchases> getPurchasess() {
		return purchasess;
	}

	public void setPurchasess(List<Purchases> purchasess) {
		this.purchasess = purchasess;
	}

	@Override
	public String toString() {
		return "UserPaidServicesEntity [key=" + key + ", purchasess="
				+ purchasess + ", username=" + username + "]";
	}

}
