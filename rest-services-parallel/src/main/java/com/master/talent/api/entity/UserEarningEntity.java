package com.master.talent.api.entity;

import java.util.List;
import org.joda.time.DateTime;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Location;

@Document(expiry=0)
public class UserEarningEntity {
	
	public static final String ENTITY_NAME = "EARNING-";
	
	@Id
	@Field
	private String id;
	
	@Field
	private String username;
	
	@Field
	private String commisionAmount;
	
	@Field
	private String referrer;
	
	@Field
	private String serviceId;
	
	@Field
	private String serviceName;
	
	@Field
	private DateTime created;
	
	@Field
	private boolean redeemed;
	
	@Field
	private DateTime redeemDate;
	
	@Field
	private String transactionId;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCommisionAmount() {
		return commisionAmount;
	}

	public void setCommisionAmount(String commisionAmount) {
		this.commisionAmount = commisionAmount;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public boolean isRedeemed() {
		return redeemed;
	}

	public void setRedeemed(boolean redeemed) {
		this.redeemed = redeemed;
	}

	public DateTime getRedeemDate() {
		return redeemDate;
	}

	public void setRedeemDate(DateTime redeemDate) {
		this.redeemDate = redeemDate;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "UserEarningEntity [commisionAmount=" + commisionAmount
				+ ", created=" + created + ", id=" + id + ", redeemDate="
				+ redeemDate + ", redeemed=" + redeemed + ", referrer="
				+ referrer + ", serviceId=" + serviceId + ", serviceName="
				+ serviceName + ", transactionId=" + transactionId
				+ ", username=" + username + "]";
	}

}
