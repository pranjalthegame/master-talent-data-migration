package com.master.talent.api.models;

public class Tutorial {

	private String title;
	private String desc;
	private String videoLink;
	private String category;
	private String subCategory;
	private String type;
	private String otherTrainer;
	private String otherTrainerFname;
	private String otherTrainerLname;
	
	
	public String getOtherTrainer() {
		return otherTrainer;
	}
	public void setOtherTrainer(String otherTrainer) {
		this.otherTrainer = otherTrainer;
	}
	public String getOtherTrainerFname() {
		return otherTrainerFname;
	}
	public void setOtherTrainerFname(String otherTrainerFname) {
		this.otherTrainerFname = otherTrainerFname;
	}
	public String getOtherTrainerLname() {
		return otherTrainerLname;
	}
	public void setOtherTrainerLname(String otherTrainerLname) {
		this.otherTrainerLname = otherTrainerLname;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	@Override
	public String toString() {
		return "Tutorial [title=" + title + ", desc=" + desc + ", videoLink="
				+ videoLink + ", category=" + category + ", subCategory="
				+ subCategory + "]";
	}	
}
