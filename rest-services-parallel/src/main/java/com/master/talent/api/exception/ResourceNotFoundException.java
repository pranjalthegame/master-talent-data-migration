package com.master.talent.api.exception;

import com.master.talent.api.exception.base.MasterTalentException;

public class ResourceNotFoundException extends MasterTalentException {
   
	private static final long serialVersionUID = -2859292084622222403L;
	
	public ResourceNotFoundException() {
        super();
    }
    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    public ResourceNotFoundException(String message) {
        super(message);
    }
    public ResourceNotFoundException(Throwable cause) {
        super(cause);
    }
	public ResourceNotFoundException(String exceptionCode, String message, Throwable cause) {
		super(exceptionCode, message, cause);
	}
	public ResourceNotFoundException(String exceptionCode, String message) {
		super(exceptionCode, message);
	}
    
    
}