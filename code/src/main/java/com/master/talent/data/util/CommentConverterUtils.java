/**
 * 
 */
package com.master.talent.data.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.master.talent.data.model.CommentModel;
import com.master.talent.data.model.excel.CommentExcelModel;

/**
 * @author pranjal.sinha
 *
 */
public class CommentConverterUtils {

	public static CommentModel convertComment(CommentExcelModel commentExcelModel){
		CommentModel commentModel = new CommentModel();
		commentModel.setItemId(commentExcelModel.getItemId());
		commentModel.setCreated(getDate(commentExcelModel.getCreated()));
		commentModel.setDelete(commentExcelModel.isDelete());
		commentModel.setUpdated(getDate(commentExcelModel.getUpdated()));
		commentModel.setUsername(commentExcelModel.getUsername());
		commentModel.setDesc(commentExcelModel.getDesc());
		return commentModel;
	}
	
	private static DateTime getDate(String date) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		DateTime dt = formatter.parseDateTime(date);
		return dt;
	}
}
