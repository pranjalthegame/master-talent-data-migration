package com.master.talent.api.entity.repo;


import java.util.List;

import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.ReviewEntity;


@Repository
public interface ReviewEntityRepo extends CouchbaseRepository<ReviewEntity, String>
{
	
	@View(designDocument = "reviewEntity", viewName = "getPublishedList")
	List<ReviewEntity> getByPublish(String reviewId);
	
	@View(designDocument = "reviewEntity", viewName = "getAllList")
	List<ReviewEntity> getByReviewId(String reviewId);
}

