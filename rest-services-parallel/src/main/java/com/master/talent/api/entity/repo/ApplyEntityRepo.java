package com.master.talent.api.entity.repo;


import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.ApplyEntity;


@Repository
public interface ApplyEntityRepo extends CouchbaseRepository<ApplyEntity, String>
{
}

