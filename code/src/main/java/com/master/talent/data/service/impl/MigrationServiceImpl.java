/**
 * 
 */
package com.master.talent.data.service.impl;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.master.talent.data.constants.AppConstants;
import com.master.talent.data.service.CommentFeedService;
import com.master.talent.data.service.CompanyFeedService;
import com.master.talent.data.service.DiscussionFeedService;
import com.master.talent.data.service.MigrationService;
import com.master.talent.data.service.UserFeedService;

/**
 * @author pranjal.sinha
 *
 */
public class MigrationServiceImpl implements MigrationService{
	
	@Autowired
	private UserFeedService userFeedService;
	
	@Autowired
	private CompanyFeedService companyFeedService;
	
	@Autowired
	private DiscussionFeedService discussionFeedService;
	
	@Autowired
	private CommentFeedService commentFeedService;
	
	private static final Logger LOG = LoggerFactory.getLogger(MigrationServiceImpl.class);
	
	public void initiate(String type) {
		LOG.info("Config setup done and working!!!");
		 File directory = new File("./");
		   LOG.info(directory.getAbsolutePath());
		   if(AppConstants.IS_PARALLEL_PROCESSING_ENABLED){
			   LOG.info("Will run in multi thread mode.!!");
			   if(type.equalsIgnoreCase(AppConstants.TYPE_USER)){
					 userFeedService.processUsersConcurrently();
				}else if(type.equalsIgnoreCase(AppConstants.TYPE_DISCUSSION)){
					discussionFeedService.processDiscussionsConcurrently();
				}else if(type.equalsIgnoreCase(AppConstants.TYPE_COMPANY)){
					companyFeedService.processCompaniesConcurrently();
				}else if(type.equalsIgnoreCase(AppConstants.TYPE_COMMENT)){
					commentFeedService.processCommentsConcurrently();
				}
		   }else{
			   LOG.info("Will run in single thread mode.!!");
			   if(type.equalsIgnoreCase(AppConstants.TYPE_USER)){
				   userFeedService.processUsers();
				}else if(type.equalsIgnoreCase(AppConstants.TYPE_DISCUSSION)){
					discussionFeedService.processDiscussions();
				}else if(type.equalsIgnoreCase(AppConstants.TYPE_COMPANY)){
					companyFeedService.processCompanies();
				}else if(type.equalsIgnoreCase(AppConstants.TYPE_COMMENT)){
					commentFeedService.processComments();
				}
		   }
	}
}
