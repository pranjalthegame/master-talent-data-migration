package com.master.talent.api.entity;

import org.joda.time.DateTime;
import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document(expiry=0)
public class ReviewEntity {
	
	@Id
	@Field
	private String id;
	
	@Field
	private String reviewId;
	
	@Field
	private String type;
	
	@Field
	private String reviewOwner;
	
	@Field
	private DateTime created;
	
	@Field
	private String reviewMessage;
	
	@Field
	private String reviewRating;
	
	@Field
	private boolean publish;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReviewId() {
		return reviewId;
	}

	public void setReviewId(String reviewId) {
		this.reviewId = reviewId;
	}

	public String getReviewOwner() {
		return reviewOwner;
	}

	public void setReviewOwner(String reviewOwner) {
		this.reviewOwner = reviewOwner;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public String getReviewMessage() {
		return reviewMessage;
	}

	public void setReviewMessage(String reviewMessage) {
		this.reviewMessage = reviewMessage;
	}

	public String getReviewRating() {
		return reviewRating;
	}

	public void setReviewRating(String reviewRating) {
		this.reviewRating = reviewRating;
	}

	public boolean isPublish() {
		return publish;
	}

	public void setPublish(boolean publish) {
		this.publish = publish;
	}

	@Override
	public String toString() {
		return "CompanyReviewEntity [id=" + id + ", reviewId=" + reviewId
				+ ", reviewOwner=" + reviewOwner + ", created=" + created
				+ ", reviewMessage=" + reviewMessage + ", reviewRating="
				+ reviewRating + ", publish=" + publish + "]";
	}

}
