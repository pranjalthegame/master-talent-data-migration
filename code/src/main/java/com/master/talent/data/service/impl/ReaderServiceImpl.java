/**
 * 
 */
package com.master.talent.data.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.master.talent.data.constants.AppConstants;
import com.master.talent.data.model.excel.CommentExcelModel;
import com.master.talent.data.model.excel.CompanyExcelModel;
import com.master.talent.data.model.excel.DiscussionExcelModel;
import com.master.talent.data.model.excel.UserExcelModel;
import com.master.talent.data.service.ReaderService;
import com.poiji.internal.Poiji;

/**
 * @author pranjal.sinha
 *
 */
@Service
public class ReaderServiceImpl implements ReaderService {
	
	private static final Logger LOG = LoggerFactory.getLogger(ReaderServiceImpl.class);
	
	@Override
	public List<UserExcelModel> readUserFeedFile(String fileName) {
		List<UserExcelModel> userInput = null;
		try {
			 userInput = Poiji.fromExcel(new File(getFilePath(fileName)), UserExcelModel.class);
			LOG.info("User Feed File read!!. Data size is: "+userInput.size());
//			LOG.info("first row is:{} ",userInput.get(0));
		} catch (FileNotFoundException e) {
			LOG.error("Exception in reading feed file:{} ",e);
		}return userInput;
	}

	@Override
	public List<DiscussionExcelModel> readDiscussionFeedFile(String fileName) {
		List<DiscussionExcelModel> discussionInput = null;
		try {
			discussionInput = Poiji.fromExcel(new File(getFilePath(fileName)), DiscussionExcelModel.class);
			LOG.info("Discussion Feed File read!!. Data size is: "+discussionInput.size());
			//LOG.info(discussionInput.get(0));
		} catch (FileNotFoundException e) {
			LOG.error("Exception in reading feed file: {}",e);
		}return discussionInput;
	}

	@Override
	public List<CompanyExcelModel> readCompanyFeedFile(String fileName) {
		List<CompanyExcelModel> companyInput = null;
		try {
			companyInput = Poiji.fromExcel(new File(getFilePath(fileName)), CompanyExcelModel.class);
			LOG.info("Company Feed File read!!. Data size is: "+companyInput.size());
			//LOG.info(companyInput.get(0));
		} catch (FileNotFoundException e) {
			LOG.error("Exception in reading feed file:{} ",e);
		}return companyInput;
	}
	
	@Override
	public List<CommentExcelModel> readCommentFeedFile(String fileName) {
		List<CommentExcelModel> commentInput = null;
		try{
			commentInput = Poiji.fromExcel(new File(getFilePath(fileName)), CommentExcelModel.class);
			LOG.info("Comment Feed File read!!. Data size is: "+commentInput.size());
		}catch(FileNotFoundException e){
			LOG.error("Exception in reading feed file:{} ",e);
		}
		return commentInput;
	}
	
	private String getFilePath(String fileName){
		return AppConstants.FILE_RELATIVE_PATH + fileName;
	}


}
