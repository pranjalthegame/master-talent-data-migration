/**
 * 
 */
package com.master.talent.api.exception.base;

/**
 * @author pranjal.sinha
 *
 */
public interface ExceptionDecorator {

	String decorate(MasterTalentException exception);
}
