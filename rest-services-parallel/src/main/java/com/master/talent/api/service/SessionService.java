/**
 * Created by Apache CXF WadlToJava code generator
**/
package com.master.talent.api.service;

import com.master.talent.api.entity.SessionEntity;

public interface SessionService {
    
	SessionEntity createSession(String username);
    
    SessionEntity validateSession(String sessionId);

    void deleteSession(String sessionId);

}