package com.master.talent.api.entity;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.User;

@Document(expiry=0)
public class ApplyEntity {
	
	public static final String ENTITY_NAME = "APPLY-";
	
	@Id
	private String key;
	
	@Field
	private String itemId;	
	
	@Field
	private List<User> users;

	public String getKey() {
		return key;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
		this.key = ENTITY_NAME+itemId;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	

}
