package com.master.talent.data.model;

import java.util.List;

import org.joda.time.DateTime;


public class CompanyModel {
	
	 
	private String owner;
	private String ownershipType;
	private List<String> offers;
	private List<String> address;
	private String turnOverCurrency;
	private String foundedOn;
	private DateTime created;
	private String turnOverValue;
	private String processAuditStatus;
	private String type;
	private String title;
	private boolean isDeleted;
	private List<String> websiteUrl;
	private List<PhoneNumber> phone;
	private String pageUrl;
	private String financialAuditStatus;
	private String category;
	private String staffMembers;
	private String businessType;
	private boolean isFollower;
	private List<String> admins;
	private String desc; 
	
	//these are fields which are annotated with @Field in CompanyEntity but not present in the sample json. These are being kept in excel as well.
	private String addressCords; //field
	private String rating;//field
	private String boardingFacility; //field
	private String placementAssistance; //field
	private String financialAssistance;	//field
	private Banner banner; //field
	private String infraFacility;//field
	
	 
	
	
	 
	
	
	 
	
	
	public String getInfraFacility() {
		return infraFacility;
	}
	public void setInfraFacility(String infraFacility) {
		this.infraFacility = infraFacility;
	}
	public Banner getBanner() {
		return banner;
	}
	public void setBanner(Banner banner) {
		this.banner = banner;
	}
	
	public boolean isFollower() {
		return isFollower;
	}
	public void setFollower(boolean isFollower) {
		this.isFollower = isFollower;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBoardingFacility() {
		return boardingFacility;
	}

	public void setBoardingFacility(String boardingFacility) {
		this.boardingFacility = boardingFacility;
	}

	public String getPlacementAssistance() {
		return placementAssistance;
	}

	public void setPlacementAssistance(String placementAssistance) {
		this.placementAssistance = placementAssistance;
	}

	public String getFinancialAssistance() {
		return financialAssistance;
	}

	public void setFinancialAssistance(String financialAssistance) {
		this.financialAssistance = financialAssistance;
	}


	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getFoundedOn() {
		return foundedOn;
	}

	public void setFoundedOn(String foundedOn) {
		this.foundedOn = foundedOn;
	}

	public String getStaffMembers() {
		return staffMembers;
	}

	public void setStaffMembers(String staffMembers) {
		this.staffMembers = staffMembers;
	}


	public String getTurnOverCurrency() {
		return turnOverCurrency;
	}

	public void setTurnOverCurrency(String turnOverCurrency) {
		this.turnOverCurrency = turnOverCurrency;
	}

	public String getTurnOverValue() {
		return turnOverValue;
	}

	public void setTurnOverValue(String turnOverValue) {
		this.turnOverValue = turnOverValue;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getOwnershipType() {
		return ownershipType;
	}

	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}

	public String getFinancialAuditStatus() {
		return financialAuditStatus;
	}

	public void setFinancialAuditStatus(String financialAuditStatus) {
		this.financialAuditStatus = financialAuditStatus;
	}

	public String getProcessAuditStatus() {
		return processAuditStatus;
	}

	public void setProcessAuditStatus(String processAuditStatus) {
		this.processAuditStatus = processAuditStatus;
	}

	public List<String> getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(List<String> websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public List<PhoneNumber> getPhone() {
		return phone;
	}

	public void setPhone(List<PhoneNumber> phone) {
		this.phone = phone;
	}

	public List<String> getAdmins() {
		return admins;
	}

	public void setAdmins(List<String> admins) {
		this.admins = admins;
	}

	public List<String> getOffers() {
		return offers;
	}

	public void setOffers(List<String> offers) {
		this.offers = offers;
	}

	public List<String> getAddress() {
		return address;
	}

	public void setAddress(List<String> address) {
		this.address = address;
	}

	public String getAddressCords() {
		return addressCords;
	}

	public void setAddressCords(String addressCords) {
		this.addressCords = addressCords;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

}
