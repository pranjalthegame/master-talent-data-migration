package com.master.talent.api.entity;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Education;

@Document(expiry=0)
public class UserEducationEntity {
	
	public static final String ENTITY_NAME = "EDUCATION-";
	
	@Id
	private String key;
	
	@Field
	private String username;
	
	@Field
	private List<Education> education;
	
	@Field
	private boolean isDeleted;

	public String getKey() {
		return key;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		this.key = ENTITY_NAME+username;
	}

	public List<Education> getEducation() {
		return education;
	}

	public void setEducation(List<Education> education) {
		this.education = education;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "UserEducationEntity [username=" + username + ", education="
				+ education + "]";
	}
}
