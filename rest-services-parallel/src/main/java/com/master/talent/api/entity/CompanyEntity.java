package com.master.talent.api.entity;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Banner;
import com.master.talent.api.models.PhoneNumber;

@Document(expiry=0)
public class CompanyEntity {
	
	@Id
	@Field
	private String pageId;
	
	@Field
	private String owner;
	
	@Field
	private String type;
	
	@Field
	private DateTime created;
	
	@Field
	private String title;
	
	@Field
	private String desc;
	
	@Field
	private String pageUrl;
	
	@Field
	private String category;
	
	@Field
	private String foundedOn;
	
	@Field
	private String staffMembers;
	
	@Field
	private String infraFacility;
	
	public String getInfraFacility() {
		return infraFacility;
	}
	public void setInfraFacility(String infraFacility) {
		this.infraFacility = infraFacility;
	}

	@Field
	private String turnOverCurrency;

	@Field
	private String turnOverValue;
	
	@Field
	private String businessType;
	
	@Field
	private String ownershipType;
	
	@Field
	private String financialAuditStatus;
	
	@Field
	private String processAuditStatus;

	@Field
	private List<String> websiteUrl;
	
	@Field
	private List<PhoneNumber> phone;
	
	@Field
	private List<String> admins;
	
	@Field
	private List<String> offers;
	
	@Field
	private List<String> address;
	
	@Field
	private String addressCords;
	
	@Field
	private boolean isDeleted;
	
	@Field
	private String rating;
	
	@Field
	private String boardingFacility;
	
	@Field
	private String placementAssistance;
	
	@Field
	private String financialAssistance;	
	
	@Field
	private Banner banner;
	
	private boolean isFollower;
	
	public Banner getBanner() {
		return banner;
	}
	public void setBanner(Banner banner) {
		this.banner = banner;
	}
	
	public boolean isFollower() {
		return isFollower;
	}
	public void setFollower(boolean isFollower) {
		this.isFollower = isFollower;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBoardingFacility() {
		return boardingFacility;
	}

	public void setBoardingFacility(String boardingFacility) {
		this.boardingFacility = boardingFacility;
	}

	public String getPlacementAssistance() {
		return placementAssistance;
	}

	public void setPlacementAssistance(String placementAssistance) {
		this.placementAssistance = placementAssistance;
	}

	public String getFinancialAssistance() {
		return financialAssistance;
	}

	public void setFinancialAssistance(String financialAssistance) {
		this.financialAssistance = financialAssistance;
	}


	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getFoundedOn() {
		return foundedOn;
	}

	public void setFoundedOn(String foundedOn) {
		this.foundedOn = foundedOn;
	}

	public String getStaffMembers() {
		return staffMembers;
	}

	public void setStaffMembers(String staffMembers) {
		this.staffMembers = staffMembers;
	}


	public String getTurnOverCurrency() {
		return turnOverCurrency;
	}

	public void setTurnOverCurrency(String turnOverCurrency) {
		this.turnOverCurrency = turnOverCurrency;
	}

	public String getTurnOverValue() {
		return turnOverValue;
	}

	public void setTurnOverValue(String turnOverValue) {
		this.turnOverValue = turnOverValue;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getOwnershipType() {
		return ownershipType;
	}

	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}

	public String getFinancialAuditStatus() {
		return financialAuditStatus;
	}

	public void setFinancialAuditStatus(String financialAuditStatus) {
		this.financialAuditStatus = financialAuditStatus;
	}

	public String getProcessAuditStatus() {
		return processAuditStatus;
	}

	public void setProcessAuditStatus(String processAuditStatus) {
		this.processAuditStatus = processAuditStatus;
	}

	public List<String> getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(List<String> websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public List<PhoneNumber> getPhone() {
		return phone;
	}

	public void setPhone(List<PhoneNumber> phone) {
		this.phone = phone;
	}

	public List<String> getAdmins() {
		return admins;
	}

	public void setAdmins(List<String> admins) {
		this.admins = admins;
	}

	public List<String> getOffers() {
		return offers;
	}

	public void setOffers(List<String> offers) {
		this.offers = offers;
	}

	public List<String> getAddress() {
		return address;
	}

	public void setAddress(List<String> address) {
		this.address = address;
	}

	public String getAddressCords() {
		return addressCords;
	}

	public void setAddressCords(String addressCords) {
		this.addressCords = addressCords;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "CompanyEntity [pageId=" + pageId + ", owner=" + owner + ", type=" + type + ", created=" + created
				+ ", title=" + title + ", desc=" + desc + ", pageUrl=" + pageUrl + ", category=" + category
				+ ", foundedOn=" + foundedOn + ", staffMembers=" + staffMembers + ", infraFacility="
				+ infraFacility + ", turnOverCurrency=" + turnOverCurrency + ", turnOverValue=" + turnOverValue
				+ ", businessType=" + businessType + ", ownershipType=" + ownershipType + ", financialAuditStatus="
				+ financialAuditStatus + ", processAuditStatus=" + processAuditStatus + ", websiteUrl=" + websiteUrl
				+ ", phone=" + phone + ", admins=" + admins + ", offers=" + offers + ", address=" + address
				+ ", addressCords=" + addressCords + ", isDeleted=" + isDeleted + ", rating=" + rating
				+ ", boardingFacility=" + boardingFacility + ", placementAssistance=" + placementAssistance
				+ ", financialAssistance=" + financialAssistance + "]";
	}

}
