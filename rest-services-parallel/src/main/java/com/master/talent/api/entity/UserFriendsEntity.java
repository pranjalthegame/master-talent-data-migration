package com.master.talent.api.entity;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Friends;

@Document(expiry=0)
public class UserFriendsEntity {
	
	public static final String ENTITY_NAME = "FRIENDS-";
	
	@Id
	private String key;
	
	@Field
	private String username;
	
	@Field
	private List<Friends> friends;
	
	@Field
	private boolean isDeleted;

	public String getKey() {
		return key;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		this.key = ENTITY_NAME+username;
	}


	public List<Friends> getFriends() {
		return friends;
	}

	public void setFriends(List<Friends> friends) {
		this.friends = friends;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "UserOccupationEntity [username=" + username + ", friends="
				+ friends + ", isDeleted=" + isDeleted + "]";
	}

}
