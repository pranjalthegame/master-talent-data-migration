package com.master.talent.api.entity.repo;


import java.util.List;

import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.CommentEntity;
import com.master.talent.api.entity.ProfileViewEntity;


@Repository
public interface ProfielViewEntityRepo extends CouchbaseRepository<ProfileViewEntity, String>{
	
	@View(designDocument = "viewEntity", viewName = "getByUser")
	List<CommentEntity> findByViewee(String viewee);
	
	@View(designDocument = "viewEntity", viewName = "getByUser")
	Long countByViewee(String viewee);
}

