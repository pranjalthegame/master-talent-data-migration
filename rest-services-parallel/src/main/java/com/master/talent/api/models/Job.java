package com.master.talent.api.models;

import java.util.List;

public class Job {
	
	private String title;
	private String company;
	private String functionalArea;
	private String industry;
	private String jobType;
	private String noOfVacancy;
	private String budget;
	private String experience;
	private String qualification;
	private List<String> skills;
	private String desc;
	private boolean promoteJob;
	private String website;
	private String jobFor;
	private String level;
	private boolean locationNotapplicable;
	private String location;
	private String paymentMethod;
	private String negotiable;
	private String currency;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getFunctionalArea() {
		return functionalArea;
	}
	public void setFunctionalArea(String functionalArea) {
		this.functionalArea = functionalArea;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getNoOfVacancy() {
		return noOfVacancy;
	}
	public void setNoOfVacancy(String noOfVacancy) {
		this.noOfVacancy = noOfVacancy;
	}
	public String getBudget() {
		return budget;
	}
	public void setBudget(String budget) {
		this.budget = budget;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public List<String> getSkills() {
		return skills;
	}
	public void setSkills(List<String> skills) {
		this.skills = skills;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public boolean isPromoteJob() {
		return promoteJob;
	}
	public void setPromoteJob(boolean promoteJob) {
		this.promoteJob = promoteJob;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getJobFor() {
		return jobFor;
	}
	public void setJobFor(String jobFor) {
		this.jobFor = jobFor;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public boolean isLocationNotapplicable() {
		return locationNotapplicable;
	}
	public void setLocationNotapplicable(boolean locationNotapplicable) {
		this.locationNotapplicable = locationNotapplicable;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getNegotiable() {
		return negotiable;
	}
	public void setNegotiable(String negotiable) {
		this.negotiable = negotiable;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Override
	public String toString() {
		return "Job [title=" + title + ", company=" + company
				+ ", functionalArea=" + functionalArea + ", industry="
				+ industry + ", jobType=" + jobType + ", noOfVacancy="
				+ noOfVacancy + ", budget=" + budget + ", experience="
				+ experience + ", qualification=" + qualification + ", skills="
				+ skills + ", desc=" + desc + ", promoteJob=" + promoteJob
				+ ", website=" + website + ", jobFor=" + jobFor + ", level="
				+ level + ", locationNotapplicable=" + locationNotapplicable
				+ ", location=" + location + ", paymentMethod=" + paymentMethod
				+ ", negotiable=" + negotiable + ", currency=" + currency + "]";
	}
}
