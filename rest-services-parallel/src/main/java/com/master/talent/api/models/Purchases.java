package com.master.talent.api.models;

import org.joda.time.DateTime;


public class Purchases {
	
	private String purchaseId;
	private String serviceId;
	private String purchasedPrice;
    private DateTime created;
    private DateTime expireBy;
    private int count;
    private int percentage;
    private String paymetnId;
    
    
    public String getPaymetnId() {
		return paymetnId;
	}
	public void setPaymetnId(String paymetnId) {
		this.paymetnId = paymetnId;
	}
	public int getPercentage() {
		return percentage;
	}
	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getPurchaseId() {
		return purchaseId;
	}
	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}
	public String getPurchasedPrice() {
		return purchasedPrice;
	}
	public void setPurchasedPrice(String purchasedPrice) {
		this.purchasedPrice = purchasedPrice;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public DateTime getCreated() {
		return created;
	}
	public void setCreated(DateTime created) {
		this.created = created;
	}
	public DateTime getExpireBy() {
		return expireBy;
	}
	public void setExpireBy(DateTime expireBy) {
		this.expireBy = expireBy;
	}
	
	@Override
	public String toString() {
		return "Purchases [created=" + created + ", expireBy=" + expireBy
				+ ", purchasedPrice=" + purchasedPrice + ", serviceId="
				+ serviceId + "]";
	}
}
