/**
 * 
 */
package com.master.talent.data.callabletasks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.master.talent.data.model.ResponseObject;
import com.master.talent.data.model.UserModel;
import com.master.talent.data.model.excel.UserExcelModel;
import com.master.talent.data.service.RestService;
import com.master.talent.data.util.JsonUtils;
import com.master.talent.data.util.UserConverterUtils;

/**
 * @author pranjal.sinha
 *
 */
public class CallableUserTask implements Callable<Map<String,String>>{

	private RestService restService;
	private List<UserExcelModel> userBatch;
	
	public CallableUserTask(RestService restService,List<UserExcelModel> userBatch){
		this.restService = restService;
		this.userBatch = userBatch;
	}
	
	@Override
	public Map<String,String> call() throws Exception {
		Map<String,String> response = new HashMap<>();
		Thread.currentThread().setName(getThreadName());
		for(UserExcelModel inputData: userBatch){
			UserModel userModel = UserConverterUtils.convertUser(inputData);
			ResponseObject res = restService.processUserOverHttp(userModel);
			response.put(userModel.getUsername(), JsonUtils.toJson(res));
		}return response;
	}
	
	private String getThreadName(){
		String threadName = "master-talent-user-data-migration-thread";
		Long threadId = Thread.currentThread().getId();
		threadName = threadName + "-" + threadId;
		return threadName;
	}

}
