package com.master.talent.api.entity;


import java.util.List;

import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;
import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Chat;

@Document(expiry=0)
public class ChatGroupEntity
{

	@Id
	@Field
	private String Id;
	
	@Field
    @NotNull
    private DateTime created;
	
	@Field
    @NotNull
    private DateTime updated;
	
	@Field
	private String owner;
	
	@Field
	private List<String> userList;
	
	@Field
	private List<Chat> chat;
	
		public DateTime getCreated() {
		return created;
	}

	public void setCreated(DateTime created) {
		this.created = created;
	}

	public DateTime getUpdated() {
		return updated;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public List<String> getUserList() {
		return userList;
	}

	public void setUserList(List<String> userList) {
		this.userList = userList;
	}

	public List<Chat> getMessageList() {
		return chat;
	}

	public void setMessageList(List<Chat> chat) {
		this.chat = chat;
	}

	@Override
	public String toString() {
		return "ChatGroupEntity [Id=" + Id + ", owner=" + owner + ", userList="
				+ userList + ",chat=" +chat + "]";
	}
	
}
