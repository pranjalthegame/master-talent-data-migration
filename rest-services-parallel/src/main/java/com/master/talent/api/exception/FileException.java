package com.master.talent.api.exception;

import com.master.talent.api.exception.base.MasterTalentException;

public class FileException extends MasterTalentException {
   
	private static final long serialVersionUID = 1L;
	
	public FileException() {
        super();
    }
    public FileException(String message, Throwable cause) {
        super(message, cause);
    }
    public FileException(String message) {
        super(message);
    }
    public FileException(Throwable cause) {
        super(cause);
    }
	public FileException(String exceptionCode, String message, Throwable cause) {
		super(exceptionCode, message, cause);
	}
	public FileException(String exceptionCode, String message) {
		super(exceptionCode, message);
	}
    
    
}