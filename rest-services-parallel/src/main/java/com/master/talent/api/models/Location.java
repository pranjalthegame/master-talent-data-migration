package com.master.talent.api.models;


public class Location {

	private String country;
	private String location;
	private boolean current;
	
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public boolean isCurrent() {
		return current;
	}
	public void setCurrent(boolean current) {
		this.current = current;
	}
	@Override
	public String toString() {
		return "Location [location=" + location + ", current=" + current
				+ "]";
	}
}
