package com.master.talent.api.entity;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Location;

@Document(expiry=0)
public class UserLocationEntity {
	
	public static final String ENTITY_NAME = "LOCATION-";
	
	@Id
	private String key;
	
	@Field
	private String username;
	
	@Field
	List<Location> location;
	
	public String getKey() {
		return key;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		this.key = ENTITY_NAME+username;
	}

	public List<Location> getLocation() {
		return location;
	}

	public void setLocation(List<Location> location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "UserLocationEntity [key=" + key + ", username=" + username
				+ ", location=" + location + "]";
	}
}
