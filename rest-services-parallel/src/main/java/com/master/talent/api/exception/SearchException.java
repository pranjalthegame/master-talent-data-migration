package com.master.talent.api.exception;

import com.master.talent.api.exception.base.MasterTalentException;

public class SearchException extends MasterTalentException {
   
	private static final long serialVersionUID = 1L;
	
	public SearchException() {
        super();
    }
    public SearchException(String message, Throwable cause) {
        super(message, cause);
    }
    public SearchException(String message) {
        super(message);
    }
    public SearchException(Throwable cause) {
        super(cause);
    }
	public SearchException(String exceptionCode, String message, Throwable cause) {
		super(exceptionCode, message, cause);
	}
	public SearchException(String exceptionCode, String message) {
		super(exceptionCode, message);
	}
    
}