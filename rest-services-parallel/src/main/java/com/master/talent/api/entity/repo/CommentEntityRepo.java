package com.master.talent.api.entity.repo;


import java.util.List;

import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.master.talent.api.entity.CommentEntity;


@Repository
public interface CommentEntityRepo extends CouchbaseRepository<CommentEntity, String>
{

	//@View(designDocument = "itemEntity", viewName = "byItemId")
	List<CommentEntity> findByItemIdOrderByUpdatedAsc(String itemId);
	
	//@View(designDocument = "itemEntity", viewName = "byItemId")
	Long countByItemId(String itemId);
}

