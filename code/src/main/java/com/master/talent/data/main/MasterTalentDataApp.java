package com.master.talent.data.main;
 
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.master.talent.data.config.AppConfig;
import com.master.talent.data.service.MigrationService;
 
public class MasterTalentDataApp {
	public static void main(String[] args) {
	    
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
	    MigrationService migrationService = (MigrationService) context.getBean("migrationService");
	    System.out.println("<==================MENU==================>");
	    System.out.println("1) User Migration");
	    System.out.println("2) Discussion Migration");
	    System.out.println("3) Company Migration");
	    System.out.println("4) Comments Migration");
	    
	    System.out.println("Enter the option (Please enter the number from 1-4):");
	    Scanner sc = new Scanner(System.in);
	    try{
	    	int choice = sc.nextInt();
	    	 switch (choice) {
	 		case 1:
	 			migrationService.initiate("user");
	 			break;
	 		case 2:
	 			migrationService.initiate("discussion");
	 			break;
	 		case 3:
	 			migrationService.initiate("company");
	 			break;
	 		case 4:
	 			migrationService.initiate("comment");
	 			break;
	 		default:
	 			System.out.println("Incorrect Choice...Exiting");
	 			System.exit(0);
	 			break;
	 		}
	    }catch(Exception e){
	    	e.printStackTrace();
	    }finally{
	    	sc.close();
	    }
	}
}