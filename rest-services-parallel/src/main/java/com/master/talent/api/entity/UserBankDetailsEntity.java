package com.master.talent.api.entity;


import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document(expiry=0)
public class UserBankDetailsEntity
{
	
	public static final String ENTITY_NAME = "BANK-";
	
	@Id
    private String key;
	
	@Field
	@NotEmpty
	private String username;
	
	@Field
	private String bankName;
	
	@Field
	private String fullName;
	
	@Field
	private String ifscCode;
	
	@Field
	private String accountNumber;
	
	@Field
	private String panCard;
	
	@Field
	private String aadharCard;
	
	public String getKey() {
		return key;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		this.key = ENTITY_NAME+username;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPanCard() {
		return panCard;
	}

	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}

	public String getAadharCard() {
		return aadharCard;
	}

	public void setAadharCard(String aadharCard) {
		this.aadharCard = aadharCard;
	}

	@Override
	public String toString() {
		return "UserBankDetailsEntity [aadharCard=" + aadharCard
				+ ", accountNumber=" + accountNumber + ", bankName=" + bankName
				+ ", fullName=" + fullName + ", ifscCode=" + ifscCode
				+ ", key=" + key + ", panCard=" + panCard + ", username="
				+ username + "]";
	}
	
}
