package com.master.talent.api.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.UUID;

import javax.activation.DataHandler;

import net.coobird.thumbnailator.Thumbnails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.master.talent.api.exception.ImageUploadException;
import com.master.talent.api.exception.base.ExceptionCodes;

@Component
public class ProfileImageProcessor {
	
	private final Logger logger = LoggerFactory.getLogger(ProfileImageProcessor.class);
	
	public void generateProfileImage(DataHandler dataHandler, String username) {
		String uploadImagePath = initialRendition(dataHandler, username, 200, 200, "profile");
		generateRendition(uploadImagePath, username, 150, 150, "profile");
		generateRendition(uploadImagePath, username, 100, 100, "profile");
		generateRendition(uploadImagePath, username, 50, 50, "profile");
		generateRendition(uploadImagePath, username, 40, 40, "profile");
		generateRendition(uploadImagePath, username, 30, 30, "profile");
	}
	
	public String generateItem(DataHandler dataHandler) {
		String itemValue = UUID.randomUUID().toString();
		String uploadImagePath = initialItemRendition(dataHandler, itemValue, 850, 850);
		generateItemRendition(uploadImagePath, itemValue, 768, 260);
		generateItemRendition(uploadImagePath, itemValue, 500, 500);
		generateItemRendition(uploadImagePath, itemValue, 200, 200);
		generateItemRendition(uploadImagePath, itemValue, 100, 100);
		String[] temp = uploadImagePath.split("//image");
		String temp1 = temp[0].substring(SpringPropertiesUtil.getProperty("image.path").length(), temp[0].length());
		return temp1;
	}
	
	public void generateBannerImage(DataHandler dataHandler, String username) {
		String uploadImagePath = initialRendition(dataHandler, username, 1400, 380, "banner");
		generateRendition(uploadImagePath, username, 920, 315, "banner");
		generateRendition(uploadImagePath, username, 768, 260, "banner");
		generateRendition(uploadImagePath, username, 560, 190, "banner");
	}
	
	private String initialItemRendition(DataHandler dataHandler, String itemValue, int width, int height) throws ImageUploadException{
		File targetFoler = new File(SpringPropertiesUtil.getProperty("image.path")+"image//item//"+itemValue);
		if(!targetFoler.exists()){
			targetFoler.mkdirs();
		}
		FileOutputStream outputStream = null;
		try{
			outputStream=new FileOutputStream(targetFoler+"//image"+"-"+width+"x"+height+".jpg");
			Thumbnails.of(dataHandler.getInputStream())
			.outputFormat("jpg")
	        .size(width, height)
	        .toOutputStream(outputStream);
		} catch (Exception e){
			logger.debug("file write error", e);
			throw new ImageUploadException(ExceptionCodes.IUECodes.IUE_DEFAULT,e.getMessage(),e);
		} finally {
			try{
				dataHandler.getInputStream().close();
				outputStream.close();
			} catch (Exception e){
				logger.debug("file write error in catch", e);
				throw new ImageUploadException(ExceptionCodes.IUECodes.IUE_DEFAULT,e.getMessage(),e);
			} 
		}
		return targetFoler+"//image"+"-"+width+"x"+height+".jpg";
	}
	
	private String initialRendition(DataHandler dataHandler, String username, int width, int height, String type) throws ImageUploadException{
		File targetFoler = new File(SpringPropertiesUtil.getProperty("image.path")+"image//"+username+"//"+type);
		if(!targetFoler.exists()){
			targetFoler.mkdirs();
		}
		FileOutputStream outputStream = null;
		try{
			outputStream=new FileOutputStream(targetFoler+"//"+type+".jpg");
			Thumbnails.of(dataHandler.getInputStream())
			.outputFormat("jpg")
	        .size(width, height)
	        .toOutputStream(outputStream);
		} catch (Exception e){
			logger.debug("file write error", e);
			throw new ImageUploadException(getCodeForFailureType(type),e.getMessage(),e);
		} finally {
			try{
				dataHandler.getInputStream().close();
				outputStream.close();
			} catch (Exception e){
				logger.debug("file write error in catch", e);
				throw new ImageUploadException(ExceptionCodes.IUECodes.IUE_DEFAULT,e.getMessage(),e);
			} 
		}
		return targetFoler+"//"+type+".jpg";
	}
	
	private void generateRendition(String imageProvidedPath, String username, int width, int height,  String type) throws ImageUploadException{
		File targetFoler = new File(SpringPropertiesUtil.getProperty("image.path")+"image//"+username+"//"+type);
		if(!targetFoler.exists()){
			targetFoler.mkdirs();
		}
		FileOutputStream outputStream = null;
		InputStream inputStream = null;
		try{
			inputStream = new FileInputStream(imageProvidedPath);
			outputStream=new FileOutputStream(targetFoler+"//"+type+"-"+width+"x"+height+".jpg");
			Thumbnails.of(inputStream)
			.outputFormat("jpg")
	        .size(width, height)
	        .toOutputStream(outputStream);
		} catch (Exception e){
			logger.debug("file write error");
			throw new ImageUploadException(getCodeForFailureType(type),e.getMessage(),e);
			
		} finally {
			try{
				inputStream.close();
				outputStream.close();
			} catch (Exception e){
				logger.debug("file write error: ",e);
				throw new ImageUploadException(ExceptionCodes.IUECodes.IUE_DEFAULT,e.getMessage(),e);
				
			} 
		}
	}
	
	private void generateItemRendition(String imageProvidedPath, String itemValue, int width, int height) throws ImageUploadException{
		File targetFoler = new File(SpringPropertiesUtil.getProperty("image.path")+"image//item//"+itemValue);
		if(!targetFoler.exists()){
			targetFoler.mkdirs();
		}
		FileOutputStream outputStream = null;
		InputStream inputStream = null;
		try{
			inputStream = new FileInputStream(imageProvidedPath);
			outputStream=new FileOutputStream(targetFoler+"//image"+"-"+width+"x"+height+".jpg");
			Thumbnails.of(inputStream)
			.outputFormat("jpg")
	        .size(width, height)
	        .toOutputStream(outputStream);
		} catch (Exception e){
			logger.debug("file write error:",e);
			throw new ImageUploadException(ExceptionCodes.IUECodes.IUE_DEFAULT,e.getMessage(),e);
			
		} finally {
			try{
				inputStream.close();
				outputStream.close();
			} catch (Exception e){
				logger.debug("file write error");
				throw new ImageUploadException(ExceptionCodes.IUECodes.IUE_DEFAULT,e.getMessage(),e);
				
			} 
		}
	}
	private String getCodeForFailureType(String type){
		switch(type){
		case "user":
			return ExceptionCodes.IUECodes.IUE_USER_IMAGE_RENDER_FAIL;
		case "item":
			return ExceptionCodes.IUECodes.IUE_ITEM_IMAGE_RENDER_FAIL;
		case "banner":
			return ExceptionCodes.IUECodes.IUE_BANNER_IMAGE_RENDER_FAIL;
		case "profile":
			return ExceptionCodes.IUECodes.IUE_PROFILE_IMAGE_RENDER_FAIL;
		default:
			return ExceptionCodes.IUECodes.IUE_DEFAULT;
		}
			
		
	}
}
