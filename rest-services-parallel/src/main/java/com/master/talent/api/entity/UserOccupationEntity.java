package com.master.talent.api.entity;

import java.util.List;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.master.talent.api.models.Occupation;

@Document(expiry=0)
public class UserOccupationEntity {
	
	public static final String ENTITY_NAME = "OCCUPATION-";
	
	@Id
	private String key;
	
	@Field
	private String username;
	
	@Field
	private List<Occupation> occupation;
	
	@Field
	private boolean isDeleted;

	public String getKey() {
		return key;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		this.key = ENTITY_NAME+username;
	}

	public List<Occupation> getOccupation() {
		return occupation;
	}

	public void setOccupation(List<Occupation> occupation) {
		this.occupation = occupation;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "UserOccupationEntity [username=" + username + ", occupation="
				+ occupation + ", isDeleted=" + isDeleted + "]";
	}

}
