package com.master.talent.api.models;

public class Service
{
  private String name;

  public String getName() { return this.name; }

  public void setName(String name) { this.name = name; }

  private String title;

  public String getTitle() { return this.title; }

  public void setTitle(String title) { this.title = title; }

  private String desc;

  public String getDesc() { return this.desc; }

  public void setDesc(String desc) { this.desc = desc; }
  
  private int referral;
  

public int getReferral() {
	return referral;
}

public void setReferral(int referral) {
	this.referral = referral;
}

private int cost;

  public int getCost() { return this.cost; }

  public void setCost(int cost) { this.cost = cost; }

  private int offerCost;

  public int getOfferCost() { return this.offerCost; }

  public void setOfferCost(int offerCost) { this.offerCost = offerCost; }

  private String offferDesc;

  public String getOffferDesc() { return this.offferDesc; }

  public void setOffferDesc(String offferDesc) { this.offferDesc = offferDesc; }

  private String currency;

  public String getCurrency() { return this.currency; }

  public void setCurrency(String currency) { this.currency = currency; }

  private int validDays;

  public int getValidDays() { return this.validDays; }

  public void setValidDays(int validDays) { this.validDays = validDays; }

  private int validCount;

  public int getValidCount() { return this.validCount; }

  public void setValidCount(int validCount) { this.validCount = validCount; }
}