/**
 * 
 */
package com.master.talent.data.service;

/**
 * @author pranjal.sinha
 *
 */
public interface CommentFeedService {

	void processComments();
	
	void processCommentsConcurrently();
}
