/**
 * 
 */
package com.master.talent.data.service;

/**
 * @author pranjal.sinha
 *
 */
public interface DiscussionFeedService {

	void processDiscussions();
	
	void processDiscussionsConcurrently();
}
