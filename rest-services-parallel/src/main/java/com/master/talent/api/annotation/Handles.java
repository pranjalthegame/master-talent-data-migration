/**
 * 
 */
package com.master.talent.api.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author pranjal.sinha
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target(value=ElementType.TYPE)
public @interface Handles {

	String value();
}
