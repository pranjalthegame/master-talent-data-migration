package com.master.talent.api.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.master.talent.api.entity.ReviewEntity;
import com.master.talent.api.models.Purchases;
import com.master.talent.api.models.RequestInvitation;

@Path("/")
public interface CommonService {

	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/countryCodes")
    public Response getCountryCodes(@Context HttpHeaders headers);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/location/{country}")
    public Response getLocation(@Context HttpHeaders headers, @PathParam("country") String country);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/currency")
    public Response getCurrency(@Context HttpHeaders headers);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/cart")
    public Response getCart(@Context HttpHeaders headers);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/cart")
    public Response addToCart(@Context HttpHeaders headers, Purchases purchases);
	
	@DELETE
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/cart/{purchaseId}")
    public Response removeFromCart(@Context HttpHeaders headers, @PathParam("purchaseId") String purchaseId);
	
	/*@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/purchase")
    public Response purchase(@Context HttpHeaders headers);*/
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/purchase/list/{type}")
    public Response listPurchase(@Context HttpHeaders headers, @PathParam("type") String type);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/showContact/{targetUser}")
    public Response showContact(@Context HttpHeaders headers, @PathParam("targetUser") String targetUser);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/paidServices")
    public Response getPaidServices(@Context HttpHeaders headers);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/category")
    public Response getCategory(@Context HttpHeaders headers);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/email/{username}/{type}")
    public Response sendEmail(@Context HttpHeaders headers, @PathParam("username") String username, @PathParam("type") String type);
	
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/email/sendInvitation")
    public Response sendInvitation(@Context HttpHeaders headers, RequestInvitation requestInvitation);
	
    @GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/session")
    Response getSession(@Context HttpHeaders headers);

	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/relation/friend/{username}")
	//note this should be a soft delete change attribute delete=true
	Response isAFriend(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/relation/follow/{username}")
	//note this should be a soft delete change attribute delete=true
	Response isAFollower(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/profile/view/{username}")
	Response profileView(@Context HttpHeaders headers, @PathParam("username") String username);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/profile/getView/{from}/{to}")
	Response profileViews(@Context HttpHeaders headers,  @PathParam("from") int from,  @PathParam("to") int to);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/profile/chartView/{type}")
	Response chartViews(@Context HttpHeaders headers,  @PathParam("type") String type);
	
	@POST
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/create/review")
	Response createReview(@Context HttpHeaders headers, ReviewEntity reviewEntity);
	
	@PUT
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/update/review")
	Response updateReview(@Context HttpHeaders headers, ReviewEntity reviewEntity);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/get/reviews/{type}/{username}")
	Response getReviews(@Context HttpHeaders headers, @PathParam("type") String type, @PathParam("username") String username);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/get/itemCount/{type}")
	Response getItemCounts(@Context HttpHeaders headers, @PathParam("type") String type);
	
	@GET
    @Consumes("application/json")
    @Produces("application/json")
	@Path("/getPaymentOrder")
	Response getPaymentOrder(@Context HttpHeaders headers);
	
}